/**
 * Función para exportar a excel el reporte.
 */
function exportarReporteExcel() {
	var url = document.getElementById('visorRep').getAttribute('src');
	var urlXls = url.replace('&__format=pdf', '&__format=xls');
	this.location.href = urlXls;
}

/**
 * Función para exportar a word el reporte.
 */
function exportarReporteWord() {
	var url = document.getElementById('visorRep').getAttribute('src');
	var urlDoc = url.replace('&__format=pdf', '&__format=doc');
	this.location.href = urlDoc;
}

/**
 * Se encarga de mostrar una ventana emergente con el reporte.
 */
function mostrarReporte(url, nombre, reporte) {
	if (reporte === 1) {
		contenido = '<iframe id="visorRep" src="'
				+ url
				+ '" width=100% height=100% frameborder=1 scrolling=auto></iframe>';
		console.log(1)
		document.getElementById('divReporte').innerHTML = contenido;
		console.log(2)
		window.location.href = "#visorReporte";
		console.log(3)
		console.log(contenido);
	} else {
		if (ventanaReporte) {
			ventanaReporte.close();
		}
		ventanaReporte = window
				.open(
						url,
						'_blank',
						nombre,
						'height=550,width=800,location=0,toolbar=0,menubar=0,directories=0,resizable=yes,scrollbars=yes');
		if (window.focus) {
			ventanaReporte.focus();
		}
	}
	return false;
}