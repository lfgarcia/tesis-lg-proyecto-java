/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			PersonaConverter.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ec.edu.puce.ltic.sca.entidad.Persona;

/**
 * Convertidor para las personas.
 * 
 * @author Luis García Castro
 */
@FacesConverter("personaConverter")
public class PersonaConverter implements Converter {

	/**
	 * Permite obtener la información como un objeto.
	 */
	@Override
	public Object getAsObject(FacesContext contexto, UIComponent elemento, String valor) {
		Persona persona = new Persona();
		String[] split = valor.split("#");
		persona.setId(Long.valueOf(split[0]));
		persona.setNombre(split[1]);
		persona.setApellido(split[2]);
		return persona;
	}

	/**
	 * Permite obtener la información como string.
	 */
	@Override
	public String getAsString(FacesContext contexto, UIComponent elemento, Object valor) {
		Persona persona = (Persona) valor;
		return persona.getId() + "#" + persona.getNombre() + "#" + persona.getApellido();
	}

}
