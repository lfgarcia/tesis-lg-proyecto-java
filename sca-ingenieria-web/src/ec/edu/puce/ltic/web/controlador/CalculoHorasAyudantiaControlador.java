/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			CalculoHorasAyudantiaControlador.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.reporte.dao.ReporteCalculoHorasAyudantiaDao;
import ec.edu.puce.ltic.web.bb.CalculoHorasAyudantiaBB;
import ec.edu.puce.ltic.web.controlador.base.ControladorReporteBase;

/**
 * Controlador para la visualización del reporte específico.
 * 
 * @author Luis García Castro
 */
@ViewScoped
@ManagedBean(name = "calculoHorasAyudantiaControlador")
public class CalculoHorasAyudantiaControlador extends ControladorReporteBase implements Serializable {

	private static final long serialVersionUID = 9168378970241838035L;

	@ManagedProperty("#{calculoHorasAyudantiaBB}")
	private CalculoHorasAyudantiaBB calculoHorasAyudantiaBB;

	@EJB
	private ReporteCalculoHorasAyudantiaDao calculoHorasAyudantiaDao;

	@PostConstruct
	public void init() {
		try {
			this.consultaInicial(this.calculoHorasAyudantiaBB.getPersonas(), this.calculoHorasAyudantiaBB.getPeriodos());
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	@Override
	public void configurarReporte() throws ScaExcepcion {
		this.calculoHorasAyudantiaBB.setReporte(this.calculoHorasAyudantiaDao.buscarTodos());
	}

	public CalculoHorasAyudantiaBB getCalculoHorasAyudantiaBB() {
		return calculoHorasAyudantiaBB;
	}

	public void setCalculoHorasAyudantiaBB(CalculoHorasAyudantiaBB calculoHorasAyudantiaBB) {
		this.calculoHorasAyudantiaBB = calculoHorasAyudantiaBB;
	}

}
