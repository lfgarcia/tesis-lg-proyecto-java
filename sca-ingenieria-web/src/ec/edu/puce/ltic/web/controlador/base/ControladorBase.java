/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ControladorBase.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador.base;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;

import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;

/**
 * Controlador base para el front-end.
 * 
 * @author Luis García Castro
 */
public abstract class ControladorBase<DTO, ENTIDAD> {

	private Class<DTO> claseDTO;

	private Class<ENTIDAD> claseENTIDAD;

	protected DTO dto;

	protected ENTIDAD objeto;

	public ControladorBase(Class<DTO> claseDTO, Class<ENTIDAD> claseENTIDAD) {
		this.claseDTO = claseDTO;
		this.claseENTIDAD = claseENTIDAD;
	}

	/**
	 * Permite ejecutar comandos de javascript por lógica. Utilizado para
	 * mostrar los modales del sistema.
	 * 
	 * @param modal
	 */
	public void ejecutarComando(final String modal) {
		RequestContext rq = RequestContext.getCurrentInstance();
		rq.execute(modal);
	}

	/**
	 * Permite iniciar el combo a mostrar en los formularios con el texto
	 * inicial: "Seleccione una opción..."
	 * 
	 * @param combo
	 */
	public void iniciarCombo(List<SelectItem> combo) {
		combo.clear();
		SelectItem item = new SelectItem();
		item.setNoSelectionOption(true);
		item.setValue(null);
		item.setLabel("Seleccione una opción...");
		combo.add(item);
	}

	/**
	 * Permite mostrar el mensaje de información como notificación.
	 * 
	 * @param message
	 */
	public void showInfoMessage(String message) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
	}

	/**
	 * Permite mostrar el mensaje de error como notificación.
	 * 
	 * @param message
	 */
	public void showErrorMessage(String message) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
	}

	/**
	 * Permite cerrar los modales tipo crud.
	 */
	public void cerrarModal() {
		ejecutarComando("PF('dialogoCrud').hide();");
	}

	/**
	 * Configuración de información previo a guardar.
	 * 
	 * @throws ScaExcepcion
	 */
	public void configurarGuardar() throws ScaExcepcion {
	}

	/**
	 * Ejecuta la acción de guardar del formulario.
	 */
	public void guardar() {
		try {
			configurarGuardar();
			this.cerrarModal();
			this.showInfoMessage("Información guardada exitosamente.");
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	/**
	 * Configuración de información a mostrar en el modal.
	 */
	public void configurarModal() {
	}

	/**
	 * Permite abrir el modal en la pantalla activa.
	 */
	public void abrirModal() {
		configurarModal();
		this.ejecutarComando("PF('dialogoCrud').show();");
	}

	/**
	 * Configuración de información para realizar el cambio de estado.
	 */
	public void configurarEstado() throws ScaExcepcion {
	}

	/**
	 * Ejecuta la acción de activar o desactivar el registro.
	 */
	public void activarDesactivarRegistro() {
		try {
			configurarEstado();
			Method activarDesactivar = claseDTO.getMethod("activarDesactivar", claseENTIDAD);
			activarDesactivar.invoke(this.dto, this.objeto);
			this.showInfoMessage("Información guardada exitosamente.");
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
			ex.printStackTrace();
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

}
