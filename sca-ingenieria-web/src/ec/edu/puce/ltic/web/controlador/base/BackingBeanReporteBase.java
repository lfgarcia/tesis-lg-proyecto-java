/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			BackingBeanReporteBase.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador.base;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

/**
 * BackingBean base para los reportes el front-end.
 * 
 * @author Luis García Castro
 */
public abstract class BackingBeanReporteBase {

	private List<SelectItem> personas;

	private String identificacionPuce;

	private List<SelectItem> periodos;

	private String periodoActual;

	public BackingBeanReporteBase() {
		this.personas = new ArrayList<>();
		this.periodos = new ArrayList<>();
	}

	public List<SelectItem> getPersonas() {
		return personas;
	}

	public void setPersonas(List<SelectItem> personas) {
		this.personas = personas;
	}

	public String getIdentificacionPuce() {
		return identificacionPuce;
	}

	public void setIdentificacionPuce(String identificacionPuce) {
		this.identificacionPuce = identificacionPuce;
	}

	public List<SelectItem> getPeriodos() {
		return periodos;
	}

	public void setPeriodos(List<SelectItem> periodos) {
		this.periodos = periodos;
	}

	public String getPeriodoActual() {
		return periodoActual;
	}

	public void setPeriodoActual(String periodoActual) {
		this.periodoActual = periodoActual;
	}

}
