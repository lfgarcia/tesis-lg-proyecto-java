/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			PersonaControlador.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador;

import java.io.Serializable;
import java.util.GregorianCalendar;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import ec.edu.puce.ltic.sca.dto.CatalogoDto;
import ec.edu.puce.ltic.sca.dto.PerfilDto;
import ec.edu.puce.ltic.sca.dto.PersonaDto;
import ec.edu.puce.ltic.sca.entidad.Catalogo;
import ec.edu.puce.ltic.sca.entidad.Perfil;
import ec.edu.puce.ltic.sca.entidad.Persona;
import ec.edu.puce.ltic.sca.enums.TiposCatalogosEnum;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.web.bb.PersonaBB;
import ec.edu.puce.ltic.web.bb.SesionBB;
import ec.edu.puce.ltic.web.controlador.base.ControladorBase;

/**
 * Controlador para el manejo de personas.
 * 
 * @author Luis García Castro
 */
@ViewScoped
@ManagedBean(name = "personaControlador")
public class PersonaControlador extends ControladorBase<PersonaDto, Persona> implements Serializable {

	private static final long serialVersionUID = 2494166960577093271L;

	@ManagedProperty("#{personaBB}")
	private PersonaBB personaBB;

	@ManagedProperty("#{sesionBB}")
	private SesionBB sesionBB;

	@EJB
	private PersonaDto personaDto;

	@EJB
	private CatalogoDto catalogoDto;

	@EJB
	private PerfilDto perfilDto;

	public PersonaControlador() {
		super(PersonaDto.class, Persona.class);
	}

	@PostConstruct
	public void init() {
		try {
			this.consultarListaPersonas();
			this.iniciarCombo(this.personaBB.getListaTiposIdentificacion());
			for (Catalogo catalogo : this.catalogoDto.buscarTodosPorTipoCatalogoCodigo(TiposCatalogosEnum.TIPO_IDENTIFICACION.getCodigo())) {
				SelectItem item = new SelectItem();
				item.setLabel(catalogo.getNombre());
				item.setValue(catalogo.getId());
				this.personaBB.getListaTiposIdentificacion().add(item);
			}
			this.iniciarCombo(this.personaBB.getListaGeneros());
			for (Catalogo catalogo : this.catalogoDto.buscarTodosPorTipoCatalogoCodigo(TiposCatalogosEnum.GENERO.getCodigo())) {
				SelectItem item = new SelectItem();
				item.setLabel(catalogo.getNombre());
				item.setValue(catalogo.getId());
				this.personaBB.getListaGeneros().add(item);
			}
			this.iniciarCombo(this.personaBB.getListaPerfiles());
			for (Perfil perfil : this.perfilDto.buscarPorTipoRol(this.sesionBB.getCodigoPerfil())) {
				SelectItem item = new SelectItem();
				item.setLabel(perfil.getTipoRol().getNombre());
				item.setValue(perfil.getId());
				this.personaBB.getListaPerfiles().add(item);
			}
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	/**
	 * Ejecuta las consultas iniciales al entrar a la pantalla.
	 * 
	 * @throws ScaExcepcion
	 */
	private void consultarListaPersonas() throws ScaExcepcion {
		if (this.sesionBB.getUsuario() != null) {
			this.personaBB.setListaPersonas(this.personaDto.buscarTodos(this.sesionBB.getUsuario().getPersona().getPerfil().getId()));
		}
	}

	/**
	 * Permite colocar el correo electrónico institucional según la
	 * identificación PUCE que coloca en el campo de texto.
	 */
	public void colocarMail() {
		this.personaBB.getPersona().setCorreoInstitucional(this.personaBB.getPersona().getIdentificacionPuce() + "@puce.edu.ec");
	}

	/**
	 * Ejecuta la acción de generar el usuario de la persona registrada.
	 */
	public void generarUsuario() {
		try {
			personaDto.generarUsuario(this.personaBB.getPersona());
			showInfoMessage("Se generó el usuario correctamente.");
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	/**
	 * Permite realizar la configuración previa para guardar una persona.
	 */
	@Override
	public void configurarGuardar() throws ScaExcepcion {
		this.personaDto.guardar(this.personaBB.getIsNuevo(), this.personaBB.getPersona(), this.personaBB.getIdTipoIdentificacion(), this.personaBB.getIdGenero(), this.personaBB.getIdPerfil());
		this.consultarListaPersonas();
	}

	/**
	 * Permite preparar el modal para agregar o editar una persona.
	 */
	@Override
	public void configurarModal() {
		if (this.personaBB.getIsNuevo()) {
			this.personaBB.setPersona(new Persona());
			this.personaBB.setIdTipoIdentificacion(null);
			this.personaBB.setIdGenero(null);
			this.personaBB.getPersona().setFechaNacimiento(new GregorianCalendar(1970, 0, 1).getTime());
			this.personaBB.setIdPerfil(null);
		} else {
			this.personaBB.setIdTipoIdentificacion(this.personaBB.getPersona().getTipoIdentificacion().getId());
			if (this.personaBB.getPersona().getGenero() != null) {
				this.personaBB.setIdGenero(this.personaBB.getPersona().getGenero().getId());
			} else {
				this.personaBB.setIdGenero(null);
			}
			this.personaBB.setIdPerfil(this.personaBB.getPersona().getPerfil().getId());
		}
	}

	/**
	 * Permite configurar el estado de la persona.
	 */
	@Override
	public void configurarEstado() {
		super.dto = this.personaDto;
		super.objeto = this.personaBB.getPersona();
	}

	public PersonaBB getPersonaBB() {
		return personaBB;
	}

	public void setPersonaBB(PersonaBB personaBB) {
		this.personaBB = personaBB;
	}

	public SesionBB getSesionBB() {
		return sesionBB;
	}

	public void setSesionBB(SesionBB sesionBB) {
		this.sesionBB = sesionBB;
	}

}
