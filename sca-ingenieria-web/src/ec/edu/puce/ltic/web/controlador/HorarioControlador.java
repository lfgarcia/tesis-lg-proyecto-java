/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			HorarioControlador.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.model.DualListModel;

import ec.edu.puce.ltic.sca.dto.HorarioDto;
import ec.edu.puce.ltic.sca.dto.PermisoDto;
import ec.edu.puce.ltic.sca.entidad.Horario;
import ec.edu.puce.ltic.sca.entidad.HorarioPersona;
import ec.edu.puce.ltic.sca.entidad.Parametro;
import ec.edu.puce.ltic.sca.entidad.Persona;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.utils.Constantes;
import ec.edu.puce.ltic.web.bb.HorarioBB;
import ec.edu.puce.ltic.web.bb.SesionBB;
import ec.edu.puce.ltic.web.controlador.base.ControladorBase;

/**
 * Controlador para el manejo de horario.
 * 
 * @author Luis García Castro
 */
@ViewScoped
@ManagedBean(name = "horarioControlador")
public class HorarioControlador extends ControladorBase<HorarioDto, Horario> implements Serializable {

	private static final long serialVersionUID = -2427654017109242382L;

	@ManagedProperty("#{horarioBB}")
	private HorarioBB horarioBB;

	@ManagedProperty("#{sesionBB}")
	private SesionBB sesionBB;

	@EJB
	private HorarioDto horarioDto;

	@EJB
	private PermisoDto permisoDto;

	public HorarioControlador() {
		super(HorarioDto.class, Horario.class);
	}

	@PostConstruct
	public void init() {
		try {
			this.consultaInicial();
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	/**
	 * Permite realizar las consultas iniciales para la pantalla.
	 * 
	 * @throws ScaExcepcion
	 */
	private void consultaInicial() throws ScaExcepcion {
		this.horarioBB.setHorario(this.horarioDto.buscarTodosHorarios());
		Parametro p = this.horarioDto.getParametroDao().buscarPorCodigo(Constantes.PERIODO_ACTUAL);
		if (p.getValorTexto() == null) {
			this.horarioBB.setPeriodoActivo(Boolean.FALSE);
		} else {
			this.horarioBB.setPeriodoActivo(Boolean.TRUE);
		}

		super.iniciarCombo(this.horarioBB.getListaCompletaPersona());
		List<Persona> completa = this.horarioDto.getPersonaDao().buscarTodosPorEstadoActivo();
		completa.forEach(persona -> {
			SelectItem item = new SelectItem();
			item.setLabel(persona.getNombreCompleto());
			item.setValue(persona.getId());
			this.horarioBB.getListaCompletaPersona().add(item);
		});
	}

	/**
	 * Permite indicar que estilo de clase se visualizará en la pantalla según
	 * la cantidad de alumnos que se encuentran en el actual horario.
	 * 
	 * @param cantidad
	 * @return Estilo a visualizar.
	 */
	public String rangoPersonas(int cantidad) {
		try {
			if (cantidad >= 0 && cantidad < (this.sesionBB.getAlumnosPorHora() / 2))
				return "onButton";
			else if (cantidad >= (this.sesionBB.getAlumnosPorHora() / 2) && cantidad < this.sesionBB.getAlumnosPorHora())
				return "editButton";
		} catch (NullPointerException ex) {
		}
		return "offButton";
	}

	/**
	 * Permite mostrar el ícono para visualizar según la cantidad de alumnos que
	 * se encuentran en el actual horario.
	 * 
	 * @param cantidad
	 * @return Ícono para mostrar en la pantalla.
	 */
	public String iconoPorRangoPersonas(int cantidad) {
		try {
			if (cantidad >= 0 && cantidad < (this.sesionBB.getAlumnosPorHora() / 2))
				return "fa-plus";
			else if (cantidad >= (this.sesionBB.getAlumnosPorHora() / 2) && cantidad < this.sesionBB.getAlumnosPorHora())
				return "fa-exchange";
		} catch (NullPointerException ex) {
		}
		return "fa-times";
	}

	/**
	 * Ejecuta la acción de guardar del formulario.
	 */
	public void guardarHorario() {
		try {
			this.horarioDto.guardar(this.horarioBB.getHora().getHoraEntrada(), this.horarioBB.getCodigoDia(), this.horarioBB.getAsociacion().getTarget());
			this.horarioBB.setHorario(this.horarioDto.buscarTodosHorarios());
			cerrarModal();
			showInfoMessage("Asociación guardada exitosamente");
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	/**
	 * Permite cargar la información y abrir el modal en la pantalla activa.
	 * 
	 * @throws ScaExcepcion
	 */
	public void cargarDatosModelo() throws ScaExcepcion {
		List<Persona> personasSource = this.horarioDto.getPersonaDao().buscarTodos(0L);
		List<Persona> personasTarget = new ArrayList<>();
		for (HorarioPersona persona : this.horarioBB.getPersonas()) {
			personasTarget.add(persona.getPersona());
			for (Persona p : personasSource) {
				if (p.getId().intValue() == persona.getPersona().getId()) {
					personasSource.remove(p);
					break;
				}
			}
		}
		this.horarioBB.setAsociacion(new DualListModel<Persona>(personasSource, personasTarget));
		iniciarCombo(this.horarioBB.getListaPersonas());
		for (Persona persona : personasTarget) {
			SelectItem item = new SelectItem();
			item.setLabel(persona.getNombreCompleto());
			item.setValue(persona.getId());
			this.horarioBB.getListaPersonas().add(item);
		}
		ejecutarComando("PF('dialogoCrud').show()");
	}

	/**
	 * Permite registrar un permiso para la persona.
	 * 
	 * @throws ScaExcepcion
	 */
	public void registrarPermiso() throws ScaExcepcion {
		try {
			this.permisoDto.registrarPermiso(this.horarioBB.getCodigoDia(), this.horarioBB.getHora().getHoraEntrada(), this.horarioBB.getHora().getHoraSalida(), this.horarioBB.getIdPersona(), this.horarioBB.getEsJustificada());
			ejecutarComando("PF('dialogoPermisos').hide();");
			showInfoMessage("Permiso guardado exitosamente.");
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	/**
	 * Permite cerrar el periodo académico actual para poder dar el inicio a uno
	 * nuevo.
	 */
	public void cerrarPeriodo() {
		try {
			this.horarioDto.cerrarPeriodoAcademico();
			this.consultaInicial();
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	/**
	 * Permite iniciar un nuevo periodo académico.
	 */
	public void iniciarPeriodo() {
		try {
			this.horarioDto.iniciarPeriodoAcademico(this.horarioBB.getCodigoPeriodo(), this.horarioBB.getDescripcionPeriodo());
			this.consultaInicial();
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	/**
	 * Permite registrar la reasignación de horas para alumnos ayudantes.
	 */
	public void registrarReasignacion() {
		try {
			this.horarioDto.registrarReasignacion(this.horarioBB.getIdPersona(), this.horarioBB.getDiaReasignado(), this.horarioBB.getHoraEntrada(), this.horarioBB.getHoraSalida());
			this.horarioBB.setIdPersona(null);
			this.horarioBB.setDiaReasignado(null);
			this.horarioBB.setHoraEntrada(null);
			this.horarioBB.setHoraSalida(null);
			super.ejecutarComando("PF('dialogoReasignacion').hide()");
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	public HorarioBB getHorarioBB() {
		return horarioBB;
	}

	public void setHorarioBB(HorarioBB horarioBB) {
		this.horarioBB = horarioBB;
	}

	public SesionBB getSesionBB() {
		return sesionBB;
	}

	public void setSesionBB(SesionBB sesionBB) {
		this.sesionBB = sesionBB;
	}

}
