/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			HorarioPorPersonaControlador.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import ec.edu.puce.ltic.sca.dto.HorarioDto;
import ec.edu.puce.ltic.sca.entidad.Horario;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.web.bb.HorarioBB;
import ec.edu.puce.ltic.web.bb.SesionBB;
import ec.edu.puce.ltic.web.controlador.base.ControladorBase;

/**
 * Controlador para el manejo de horario por persona.
 * 
 * @author Luis García Castro
 */
@ViewScoped
@ManagedBean(name = "horarioPorPersonaControlador")
public class HorarioPorPersonaControlador extends ControladorBase<HorarioDto, Horario> implements Serializable {

	private static final long serialVersionUID = 5020738614646464463L;

	@ManagedProperty("#{horarioBB}")
	private HorarioBB horarioBB;

	@ManagedProperty("#{sesionBB}")
	private SesionBB sesionBB;

	@EJB
	private HorarioDto horarioDto;

	public HorarioPorPersonaControlador() {
		super(HorarioDto.class, Horario.class);
	}

	@PostConstruct
	public void init() {
		try {
			this.consultaInicial();
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	/**
	 * Permite realizar las consultas iniciales para la pantalla.
	 * 
	 * @throws ScaExcepcion
	 */
	private void consultaInicial() throws ScaExcepcion {
		this.horarioBB.setHorario(this.horarioDto.buscarTodosHorarios());
	}

	public HorarioBB getHorarioBB() {
		return horarioBB;
	}

	public void setHorarioBB(HorarioBB horarioBB) {
		this.horarioBB = horarioBB;
	}

	public SesionBB getSesionBB() {
		return sesionBB;
	}

	public void setSesionBB(SesionBB sesionBB) {
		this.sesionBB = sesionBB;
	}

}
