/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			LoginControlador.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import ec.edu.puce.ltic.sca.dao.ParametroDao;
import ec.edu.puce.ltic.sca.dto.ControlAsistenciaDto;
import ec.edu.puce.ltic.sca.dto.LoginDto;
import ec.edu.puce.ltic.sca.dto.UsuarioDto;
import ec.edu.puce.ltic.sca.entidad.ControlAsistencia;
import ec.edu.puce.ltic.sca.entidad.Parametro;
import ec.edu.puce.ltic.sca.entidad.Persona;
import ec.edu.puce.ltic.sca.entidad.Usuario;
import ec.edu.puce.ltic.sca.enums.PerfilesEnum;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.utils.Constantes;
import ec.edu.puce.ltic.web.bb.SesionBB;
import ec.edu.puce.ltic.web.controlador.base.ControladorBase;

/**
 * Controlador para el manejo del login.
 * 
 * @author Luis García Castro
 */
@ViewScoped
@ManagedBean(name = "loginControlador")
public class LoginControlador extends ControladorBase<UsuarioDto, Usuario> implements Serializable {

	private static final long serialVersionUID = -9202519009729890994L;

	@ManagedProperty("#{sesionBB}")
	private SesionBB sesionBB;

	@EJB
	private UsuarioDto usuarioDto;

	@EJB
	private ParametroDao parametroDao;

	@EJB
	private ControlAsistenciaDto controlAsistenciaDto;

	@EJB
	private LoginDto loginDto;

	public LoginControlador() {
		super(UsuarioDto.class, Usuario.class);
	}

	@PostConstruct
	public void init() {
		try {
			if (this.sesionBB.getUsuario() != null) {
				FacesContext.getCurrentInstance().getExternalContext().redirect("/ScaIngenieria/pages/welcome.xhtml");
			}
		} catch (IOException ex) {
			showErrorMessage(ex.getMessage());
		}
	}

	/**
	 * Ejecuta la acción de ingresar al sistema.
	 */
	public void doLogin() {
		try {
			sesionBB.setUsuario(usuarioDto.doLogin(this.sesionBB.getUsername(), this.sesionBB.getPassword()));
			sesionBB.setMenu(sesionBB.getUsuario().getMenu());
			sesionBB.setCodigoPerfil(sesionBB.getUsuario().getPersona().getPerfil().getTipoRol().getCodigo());
			Parametro parametro = parametroDao.buscarPorCodigo(Constantes.TAMANO_ALUMNOS_POR_HORA);
			sesionBB.setAlumnosPorHora(parametro.getValorEntero().intValue());
			if (sesionBB.getUsuario().getPersona().getPerfil().getTipoRol().getCodigo().equals(PerfilesEnum.DOCENTE.getCodigo()) || sesionBB.getUsuario().getPersona().getPerfil().getTipoRol().getCodigo().equals(PerfilesEnum.BECARIO.getCodigo())) {
				FacesContext.getCurrentInstance().getExternalContext().redirect("/ScaIngenieria/pages/asistencia/horarios/lista.xhtml");
			} else {
				FacesContext.getCurrentInstance().getExternalContext().redirect("/ScaIngenieria/pages/welcome.xhtml");
			}
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
		} catch (IOException ex) {
			showErrorMessage(ex.getMessage());
		}
	}

	/**
	 * Ejecuta la acción de refrescar la página.
	 * 
	 * @throws IOException
	 */
	public void refresh() throws IOException {
		this.sesionBB.setUsuario(null);
		FacesContext.getCurrentInstance().getExternalContext().redirect("/ScaIngenieria");
	}

	/**
	 * Ejecuta la acción de generar el código de validación de dos pasos que
	 * será enviado al correo electrónico.
	 */
	public void crearCodigoVerificacion() {
		try {
			Persona persona = this.controlAsistenciaDto.getPersonaDao().buscarPorIdentificacionPuce(this.sesionBB.getIdentificacionPuce());
			if (persona == null) {
				showErrorMessage("La identificación PUCE ingresada no existe.");
				return;
			}
			this.sesionBB.setFechaVerificacion(new Date());
			this.sesionBB.setCodigoVerificacion(this.loginDto.generarClave(this.sesionBB.getIdentificacionPuce()));
			super.ejecutarComando("PF('dialogoCodigo').show()");
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
		}
	}

	/**
	 * Permite registrar la asistencia en el sistema.
	 */
	public void registrarAsistencia() {
		Calendar codigo = Calendar.getInstance();
		codigo.setTime(this.sesionBB.getFechaVerificacion());
		Calendar verificacion = Calendar.getInstance();
		float horaCodigo = codigo.get(Calendar.MINUTE) + ((float) codigo.get(Calendar.SECOND) / 100);
		float horaVerificacion = verificacion.get(Calendar.MINUTE) + ((float) verificacion.get(Calendar.SECOND) / 100);
		if (horaVerificacion - horaCodigo <= 2) {
			if (this.sesionBB.getCodigoVerificacion().equalsIgnoreCase(this.sesionBB.getConfirmarCodigo())) {
				try {
					ControlAsistencia ca = this.controlAsistenciaDto.registrarAsistencia(this.sesionBB.getIdentificacionPuce());
					StringBuilder sb = new StringBuilder(ca.getPersona().getNombreCompleto().concat(". Hora de "));
					if (ca.getHoraSalida() == null) {
						sb.append("Entrada: ".concat(ca.getHoraEntrada().toString()));
					} else {
						sb.append("Salida: ".concat(ca.getHoraSalida().toString()));
					}
					showInfoMessage(sb.toString());
					this.sesionBB.setIdentificacionPuce(null);
				} catch (ScaExcepcion ex) {
					showErrorMessage(ex.getMensaje());
				}
			} else {
				showErrorMessage("El código ingresado no es correcto");
			}
		} else {
			showErrorMessage("Su código de verificación caducó.");
		}
		super.ejecutarComando("PF('dialogoCodigo').hide()");
	}

	/**
	 * Ejecuta la acción de recuperar la contraseña.
	 * 
	 * @throws IOException
	 */
	public void recuperarClave() throws IOException {
		try {
			this.usuarioDto.recuperarClave(this.sesionBB.getUsernameRecovery());
		} catch (ScaExcepcion ex) {
			this.showErrorMessage(ex.getMensaje());
		}
	}

	/**
	 * Permite preconfigurar la información para restablecer la clave.
	 */
	public void preconfigurarCambioClave() {
		this.sesionBB.setUsernameRecovery("");
		this.ejecutarComando("PF('dialogoRecuperarClave').show()");
	}

	public SesionBB getSesionBB() {
		return sesionBB;
	}

	public void setSesionBB(SesionBB sesionBB) {
		this.sesionBB = sesionBB;
	}

}
