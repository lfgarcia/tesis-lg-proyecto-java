/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			BackingBeanBase.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador.base;

/**
 * BackingBean base para el front-end.
 * 
 * @author Luis García Castro
 */
public abstract class BackingBeanBase {

	protected String textoDialogo;

	protected Boolean isNuevo;

	protected String textoActivarDesactivar;

	/**
	 * Permite indicar la clase del botón a mostrar en la lista según el estado
	 * en el que se encuentra el registro.
	 * 
	 * @param estado
	 * @return
	 */
	public String activarDesactivar(Boolean estado) {
		if (estado) {
			return "offButton";
		}
		return "onButton";
	}

	public String getTextoDialogo() {
		return textoDialogo;
	}

	public void setTextoDialogo(String textoDialogo) {
		this.textoDialogo = textoDialogo;
	}

	public Boolean getIsNuevo() {
		return isNuevo;
	}

	public void setIsNuevo(Boolean isNuevo) {
		this.isNuevo = isNuevo;
	}

	public String getTextoActivarDesactivar() {
		return textoActivarDesactivar;
	}

	public void setTextoActivarDesactivar(String textoActivarDesactivar) {
		this.textoActivarDesactivar = textoActivarDesactivar;
	}

}
