/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			TipoCatalogoControlador.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import ec.edu.puce.ltic.sca.dto.TipoCatalogoDto;
import ec.edu.puce.ltic.sca.entidad.TipoCatalogo;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.web.bb.TipoCatalogoBB;
import ec.edu.puce.ltic.web.controlador.base.ControladorBase;

/**
 * Controlador para el manejo de los tipos de catálogos.
 * 
 * @author Luis García Castro
 */
@ViewScoped
@ManagedBean(name = "tipoCatalogoControlador")
public class TipoCatalogoControlador extends ControladorBase<TipoCatalogoDto, TipoCatalogo> implements Serializable {

	private static final long serialVersionUID = 7414543352861451264L;

	@ManagedProperty("#{tipoCatalogoBB}")
	private TipoCatalogoBB tipoCatalogoBB;

	@EJB
	private TipoCatalogoDto tipoCatalogoDto;

	public TipoCatalogoControlador() {
		super(TipoCatalogoDto.class, TipoCatalogo.class);
	}

	@PostConstruct
	public void init() {
		this.consultarListaTiposCatalogos();
	}

	/**
	 * Ejecuta las consultas iniciales al entrar a la pantalla.
	 */
	private void consultarListaTiposCatalogos() {
		try {
			this.tipoCatalogoBB.setListaTiposCatalogos(this.tipoCatalogoDto.buscarTodos());
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	/**
	 * Permite realizar la configuración previa para guardar un tipo de catálogo.
	 */
	@Override
	public void configurarGuardar() throws ScaExcepcion {
		this.tipoCatalogoDto.guardar(this.tipoCatalogoBB.getIsNuevo(), this.tipoCatalogoBB.getTipoCatalogo());
		this.consultarListaTiposCatalogos();
	}

	/**
	 * Permite preparar el modal para agregar o editar un tipo de catálogo.
	 */
	@Override
	public void configurarModal() {
		if (this.tipoCatalogoBB.getIsNuevo()) {
			this.tipoCatalogoBB.setTipoCatalogo(new TipoCatalogo());
		}
	}

	/**
	 * Permite configurar el estado del tipo de catálogo.
	 */
	@Override
	public void configurarEstado() {
		super.dto = this.tipoCatalogoDto;
		super.objeto = this.tipoCatalogoBB.getTipoCatalogo();
	}

	public TipoCatalogoBB getTipoCatalogoBB() {
		return tipoCatalogoBB;
	}

	public void setTipoCatalogoBB(TipoCatalogoBB tipoCatalogoBB) {
		this.tipoCatalogoBB = tipoCatalogoBB;
	}

}
