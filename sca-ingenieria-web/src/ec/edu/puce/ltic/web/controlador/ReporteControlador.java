/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ReporteControlador.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import ec.edu.puce.ltic.sca.utils.UtilSca;
import ec.edu.puce.ltic.web.bb.ReporteBB;
import ec.edu.puce.ltic.web.controlador.base.ControladorBase;

/**
 * Controlador para el manejo de parámetros.
 * 
 * @author Luis García Castro
 */
@ViewScoped
@ManagedBean(name = "reporteControlador")
public class ReporteControlador extends ControladorBase<Object, Object> implements Serializable {

	public ReporteControlador() {
		super(null, null);
	}

	@ManagedProperty("#{reporteBB}")
	private ReporteBB reporteBB;

	private static final long serialVersionUID = -4085549086722261190L;

	/**
	 * Permite regresar a la página previa en donde se abrió el reporte.
	 */
	public void regresoPagina() {
		try {
			StringBuilder pagina = new StringBuilder("/ScaIngenieria/pages/");
			pagina.append(this.reporteBB.getPaginaPrevia());
			FacesContext.getCurrentInstance().getExternalContext().redirect(pagina.toString());
		} catch (IOException ex) {
			showErrorMessage(ex.getMessage());
		}
	}

	/**
	 * Permite generar el reporte solicitado y abrirlo en la misma pantalla.
	 */
	public void generarReporte() {
		StringBuilder url = new StringBuilder("http://sca-ltic.von-development-studio.com/ScaReportes/frameset?__report=");
		url.append(this.reporteBB.getReporte());
		url.append("&TituloReporte=");
		url.append(UtilSca.codificarTextoUrl(this.reporteBB.getTitulo()));
		url.append("&PeriodoActual=");
		url.append(UtilSca.codificarTextoUrl(this.reporteBB.getPeriodo()));
		url.append("&__format=pdf");
		RequestContext.getCurrentInstance().execute("mostrarReporte('" + url.toString() + "','',1);");
		this.reporteBB.setPeriodo(null);
	}

	public ReporteBB getReporteBB() {
		return reporteBB;
	}

	public void setReporteBB(ReporteBB reporteBB) {
		this.reporteBB = reporteBB;
	}

}
