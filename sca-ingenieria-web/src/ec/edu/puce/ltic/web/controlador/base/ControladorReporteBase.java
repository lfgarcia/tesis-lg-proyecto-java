/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ControladorReporteBase.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador.base;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;

import ec.edu.puce.ltic.sca.dao.CatalogoDao;
import ec.edu.puce.ltic.sca.dao.ParametroDao;
import ec.edu.puce.ltic.sca.dao.PersonaDao;
import ec.edu.puce.ltic.sca.entidad.Catalogo;
import ec.edu.puce.ltic.sca.entidad.Parametro;
import ec.edu.puce.ltic.sca.entidad.Persona;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.utils.Constantes;

/**
 * Controlador base para los reportes en el front-end.
 * 
 * @author Luis García Castro
 */
public abstract class ControladorReporteBase {

	@EJB
	private PersonaDao personaDao;

	@EJB
	private CatalogoDao catalogoDao;

	@EJB
	private ParametroDao parametroDao;

	/**
	 * Permite ejecutar comandos de javascript por lógica. Utilizado para
	 * mostrar los modales del sistema.
	 * 
	 * @param modal
	 */
	public void ejecutarComando(final String modal) {
		RequestContext rq = RequestContext.getCurrentInstance();
		rq.execute(modal);
	}

	/**
	 * Permite iniciar el combo a mostrar en los formularios con el texto
	 * inicial: "Seleccione una opción..."
	 * 
	 * @param combo
	 */
	public void iniciarCombo(List<SelectItem> combo) {
		combo.clear();
		SelectItem item = new SelectItem();
		item.setNoSelectionOption(true);
		item.setValue(null);
		item.setLabel("Seleccione una opción...");
		combo.add(item);
	}

	/**
	 * Permite mostrar el mensaje de error como notificación.
	 * 
	 * @param message
	 */
	public void showErrorMessage(String message) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
	}

	/**
	 * Permite realizar la consulta para llenar el reporte.
	 * 
	 * @throws ScaExcepcion
	 */
	public void configurarReporte() throws ScaExcepcion {
	}

	/**
	 * Permite realizar las consultas iniciales para la pantalla.
	 * 
	 * @throws ScaExcepcion
	 */
	public void consultaInicial(List<SelectItem> comboPersonas, List<SelectItem> comboPeriodos) throws ScaExcepcion {
		configurarReporte();

		List<Persona> personas = this.personaDao.buscarTodos(0L);
		iniciarCombo(comboPersonas);
		for (Persona persona : personas) {
			SelectItem item = new SelectItem();
			item.setLabel(persona.getNombreCompleto());
			item.setValue(persona.getIdentificacionPuce());
			comboPersonas.add(item);
		}

		List<Catalogo> catalogos = this.catalogoDao.buscarTodosPorTipoCatalogoCodigo(Constantes.PERIODO_ACADEMICO);
		iniciarCombo(comboPeriodos);
		for (Catalogo catalogo : catalogos) {
			SelectItem item = new SelectItem();
			item.setLabel(catalogo.getNombre());
			item.setValue(catalogo.getCodigo());
			comboPeriodos.add(item);
		}
		Parametro parametro = this.parametroDao.buscarPorCodigo(Constantes.PERIODO_ACTUAL);
		comboPeriodos.add(new SelectItem(parametro.getValorTexto(), parametro.getDescripcion()));
	}

}
