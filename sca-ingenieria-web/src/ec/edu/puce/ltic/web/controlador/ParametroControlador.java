/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ParametroControlador.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import ec.edu.puce.ltic.sca.dto.ParametroDto;
import ec.edu.puce.ltic.sca.entidad.Catalogo;
import ec.edu.puce.ltic.sca.entidad.Parametro;
import ec.edu.puce.ltic.sca.enums.TiposCatalogosEnum;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.web.bb.ParametroBB;
import ec.edu.puce.ltic.web.controlador.base.ControladorBase;

/**
 * Controlador para el manejo de parámetros.
 * 
 * @author Luis García Castro
 */
@ViewScoped
@ManagedBean(name = "parametroControlador")
public class ParametroControlador extends ControladorBase<ParametroDto, Parametro> implements Serializable {

	private static final long serialVersionUID = 8860766452808121302L;

	@ManagedProperty("#{parametroBB}")
	private ParametroBB parametroBB;

	@EJB
	private ParametroDto parametroDto;

	public ParametroControlador() {
		super(ParametroDto.class, Parametro.class);
	}

	@PostConstruct
	public void init() {
		try {
			this.consultarListaParametros();
			this.iniciarCombo(this.parametroBB.getListaTiposParametros());
			for (Catalogo catalogo : this.parametroDto.getCatalogoDao().buscarTodosPorTipoCatalogoCodigo(TiposCatalogosEnum.TIPO_PARAMETRO.getCodigo())) {
				SelectItem item = new SelectItem();
				item.setLabel(catalogo.getNombre());
				item.setValue(catalogo.getCodigo());
				this.parametroBB.getListaTiposParametros().add(item);
			}
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	/**
	 * Ejecuta las consultas iniciales al entrar a la página.
	 * 
	 * @throws ScaExcepcion
	 */
	private void consultarListaParametros() throws ScaExcepcion {
		this.parametroBB.setListaParametros(this.parametroDto.buscarTodos());
	}

	/**
	 * Permite realizar la configuración previa para guardar un parámetro.
	 */
	@Override
	public void configurarGuardar() throws ScaExcepcion {
		this.parametroDto.guardar(this.parametroBB.getIsNuevo(), this.getParametroBB().getParametro(), this.parametroBB.getCodigoTipoParametro());
		this.consultarListaParametros();
	}

	/**
	 * Permite preparar el modal para agregar o editar un parámetro.
	 */
	@Override
	public void configurarModal() {
		if (this.parametroBB.getIsNuevo()) {
			this.parametroBB.setParametro(new Parametro());
			this.parametroBB.setCodigoTipoParametro(null);
		} else {
			this.parametroBB.setCodigoTipoParametro(this.parametroBB.getParametro().getTipoParametro().getCodigo());
		}
		this.comprobarRenderizacionCampo(this.parametroBB.getCodigoTipoParametro());
	}

	/**
	 * Permite configurar el estado del parámetro.
	 */
	@Override
	public void configurarEstado() {
		super.dto = this.parametroDto;
		super.objeto = this.parametroBB.getParametro();
	}

	/**
	 * Comprueba la renderización del campo a mostrar en el modal del
	 * formulario.
	 * 
	 * @param codigo
	 */
	public void comprobarRenderizacionCampo(String codigo) {
		this.parametroBB.setIsTipoFecha(this.parametroBB.comprobarTipoFecha(codigo));
		this.parametroBB.setIsTipoHora(this.parametroBB.comprobarTipoHora(codigo));
		this.parametroBB.setIsTipoEntero(this.parametroBB.comprobarTipoEntero(codigo));
		this.parametroBB.setIsTipoDecimal(this.parametroBB.comprobarTipoDecimal(codigo));
		this.parametroBB.setIsTipoTexto(this.parametroBB.comprobarTipoTexto(codigo));
	}

	public ParametroBB getParametroBB() {
		return parametroBB;
	}

	public void setParametroBB(ParametroBB parametroBB) {
		this.parametroBB = parametroBB;
	}

}
