/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			CalculoFaltasControlador.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.reporte.dao.ReporteCalculoFaltasDao;
import ec.edu.puce.ltic.web.bb.CalculoFaltasBB;
import ec.edu.puce.ltic.web.controlador.base.ControladorReporteBase;

/**
 * Controlador para la visualización del reporte específico.
 * 
 * @author Luis García Castro
 */
@ViewScoped
@ManagedBean(name = "calculoFaltasControlador")
public class CalculoFaltasControlador extends ControladorReporteBase implements Serializable {

	private static final long serialVersionUID = 4506698145927638224L;

	@ManagedProperty("#{calculoFaltasBB}")
	private CalculoFaltasBB calculoFaltasBB;

	@EJB
	private ReporteCalculoFaltasDao calculoFaltasDao;

	@PostConstruct
	public void init() {
		try {
			this.consultaInicial(this.calculoFaltasBB.getPersonas(), this.calculoFaltasBB.getPeriodos());
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	@Override
	public void configurarReporte() throws ScaExcepcion {
		this.calculoFaltasBB.setReporte(this.calculoFaltasDao.buscarTodos());
	}

	public CalculoFaltasBB getCalculoFaltasBB() {
		return calculoFaltasBB;
	}

	public void setCalculoFaltasBB(CalculoFaltasBB calculoFaltasBB) {
		this.calculoFaltasBB = calculoFaltasBB;
	}

}
