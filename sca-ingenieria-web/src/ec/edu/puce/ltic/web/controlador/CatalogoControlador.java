/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			CatalogoControlador.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import ec.edu.puce.ltic.sca.dto.CatalogoDto;
import ec.edu.puce.ltic.sca.dto.TipoCatalogoDto;
import ec.edu.puce.ltic.sca.entidad.Catalogo;
import ec.edu.puce.ltic.sca.entidad.TipoCatalogo;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.web.bb.CatalogoBB;
import ec.edu.puce.ltic.web.controlador.base.ControladorBase;

/**
 * Controlador para el manejo de catálogos.
 * 
 * @author Luis García Castro
 */
@ViewScoped
@ManagedBean(name = "catalogoControlador")
public class CatalogoControlador extends ControladorBase<CatalogoDto, Catalogo> implements Serializable {

	private static final long serialVersionUID = -2554874661584843586L;

	@ManagedProperty("#{catalogoBB}")
	private CatalogoBB catalogoBB;

	@EJB
	private CatalogoDto catalogoDto;

	@EJB
	private TipoCatalogoDto tipoCatalogoDto;

	public CatalogoControlador() {
		super(CatalogoDto.class, Catalogo.class);
	}

	@PostConstruct
	public void init() {
		try {
			this.consultaInicial();
			this.iniciarCombo(this.catalogoBB.getListaTiposCatalogos());
			for (TipoCatalogo tipoCatalogo : this.tipoCatalogoDto.buscarTodosPorEstadoActivo()) {
				SelectItem item = new SelectItem();
				item.setLabel(tipoCatalogo.getNombre());
				item.setValue(tipoCatalogo.getId());
				this.catalogoBB.getListaTiposCatalogos().add(item);
			}
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	/**
	 * Permite realizar las consultas iniciales para la pantalla.
	 * 
	 * @throws ScaExcepcion
	 */
	private void consultaInicial() throws ScaExcepcion {
		this.catalogoBB.setListaCatalogos(this.catalogoDto.buscarTodos());
	}

	/**
	 * Permite realizar la configuración previa para guardar un catálogo.
	 */
	@Override
	public void configurarGuardar() throws ScaExcepcion {
		this.catalogoDto.guardar(this.catalogoBB.getIsNuevo(), this.catalogoBB.getCatalogo(), this.catalogoBB.getIdTipoCatalogo());
		this.consultaInicial();
	}

	/**
	 * Permite preparar el modal para agregar o editar un catálogo.
	 */
	@Override
	public void configurarModal() {
		if (this.catalogoBB.getIsNuevo()) {
			this.catalogoBB.setCatalogo(new Catalogo());
			this.catalogoBB.setIdTipoCatalogo(null);
		} else {
			this.catalogoBB.setIdTipoCatalogo(this.catalogoBB.getCatalogo().getTipoCatalogo().getId());
		}
	}

	/**
	 * Permite configurar el estado del catálogo.
	 */
	@Override
	public void configurarEstado() {
		super.dto = this.catalogoDto;
		super.objeto = this.catalogoBB.getCatalogo();
	}

	public CatalogoBB getCatalogoBB() {
		return catalogoBB;
	}

	public void setCatalogoBB(CatalogoBB catalogoBB) {
		this.catalogoBB = catalogoBB;
	}

}
