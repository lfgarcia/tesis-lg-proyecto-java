/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			CalculoFaltasJustificadasControlador.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.reporte.dao.ReporteCalculoFaltasJustificadasDao;
import ec.edu.puce.ltic.web.bb.CalculoFaltasJustificadasBB;
import ec.edu.puce.ltic.web.controlador.base.ControladorReporteBase;

/**
 * Controlador para la visualización del reporte específico.
 * 
 * @author Luis García Castro
 */
@ViewScoped
@ManagedBean(name = "calculoFaltasJustificadasControlador")
public class CalculoFaltasJustificadasControlador extends ControladorReporteBase implements Serializable {

	private static final long serialVersionUID = -6211065680834971834L;

	@ManagedProperty("#{calculoFaltasJustificadasBB}")
	private CalculoFaltasJustificadasBB calculoFaltasJustificadasBB;

	@EJB
	private ReporteCalculoFaltasJustificadasDao calculoFaltasJustificadasDao;

	@PostConstruct
	public void init() {
		try {
			this.consultaInicial(this.calculoFaltasJustificadasBB.getPersonas(), this.calculoFaltasJustificadasBB.getPeriodos());
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	@Override
	public void configurarReporte() throws ScaExcepcion {
		this.calculoFaltasJustificadasBB.setReporte(this.calculoFaltasJustificadasDao.buscarTodos());
	}

	public CalculoFaltasJustificadasBB getCalculoFaltasJustificadasBB() {
		return calculoFaltasJustificadasBB;
	}

	public void setCalculoFaltasJustificadasBB(CalculoFaltasJustificadasBB calculoFaltasJustificadasBB) {
		this.calculoFaltasJustificadasBB = calculoFaltasJustificadasBB;
	}

}
