/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			MenuControlador.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import ec.edu.puce.ltic.sca.dto.UsuarioDto;
import ec.edu.puce.ltic.sca.entidad.Usuario;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.web.bb.SesionBB;
import ec.edu.puce.ltic.web.controlador.base.ControladorBase;

/**
 * Controlador para el manejo del menú.
 * 
 * @author Luis García Castro
 */
@ViewScoped
@ManagedBean(name = "menuControlador")
public class MenuControlador extends ControladorBase<UsuarioDto, Usuario> implements Serializable {

	private static final long serialVersionUID = -9016460690095306483L;

	@ManagedProperty("#{sesionBB}")
	private SesionBB sesionBB;

	@EJB
	private UsuarioDto usuarioDto;

	public MenuControlador() {
		super(UsuarioDto.class, Usuario.class);
	}

	@PostConstruct
	public void init() {
		try {
			if (this.sesionBB.getUsuario() == null) {
				FacesContext.getCurrentInstance().getExternalContext().redirect("/ScaIngenieria");
			}
		} catch (IOException ex) {
			// ex.printStackTrace();
		}
	}

	/**
	 * Direcciona a la pantalla que indica el menú.
	 * 
	 * @param path
	 * @throws IOException
	 */
	public void goPage(String path) throws IOException {
		FacesContext.getCurrentInstance().getExternalContext().redirect(path);
	}

	/**
	 * Ejecuta la acción de cerrar la sesión.
	 * 
	 * @throws IOException
	 */
	public void logout() throws IOException {
		this.sesionBB.setUsuario(null);
		FacesContext.getCurrentInstance().getExternalContext().redirect("/ScaIngenieria");
	}

	/**
	 * Permite preconfigurar la información para el cambio de clave.
	 */
	public void preconfigurarCambioClave() {
		this.sesionBB.setOldPassword("");
		this.sesionBB.setNewPassword("");
		this.ejecutarComando("PF('dialogoCambioClave').show()");
	}

	/**
	 * Ejecuta la acción del cambio de clave.
	 * 
	 * @throws IOException
	 */
	public void cambiarClave() throws IOException {
		try {
			this.usuarioDto.cambiarClave(this.sesionBB.getUsuario(), this.sesionBB.getOldPassword(), this.sesionBB.getNewPassword());
			this.logout();
		} catch (ScaExcepcion ex) {
			this.showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	public SesionBB getSesionBB() {
		return sesionBB;
	}

	public void setSesionBB(SesionBB sesionBB) {
		this.sesionBB = sesionBB;
	}

}
