/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			CalculoAtrasosControlador.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.reporte.dao.ReporteCalculoAtrasosDao;
import ec.edu.puce.ltic.web.bb.CalculoAtrasosBB;
import ec.edu.puce.ltic.web.controlador.base.ControladorReporteBase;

/**
 * Controlador para la visualización del reporte específico.
 * 
 * @author Luis García Castro
 */
@ViewScoped
@ManagedBean(name = "calculoAtrasosControlador")
public class CalculoAtrasosControlador extends ControladorReporteBase implements Serializable {

	private static final long serialVersionUID = -6643332667241659942L;

	@ManagedProperty("#{calculoAtrasosBB}")
	private CalculoAtrasosBB calculoAtrasosBB;

	@EJB
	private ReporteCalculoAtrasosDao calculoAtrasosDao;

	@PostConstruct
	public void init() {
		try {
			this.consultaInicial(this.calculoAtrasosBB.getPersonas(), this.calculoAtrasosBB.getPeriodos());
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	@Override
	public void configurarReporte() throws ScaExcepcion {
		this.calculoAtrasosBB.setReporte(this.calculoAtrasosDao.buscarTodos());
	}

	public CalculoAtrasosBB getCalculoAtrasosBB() {
		return calculoAtrasosBB;
	}

	public void setCalculoAtrasosBB(CalculoAtrasosBB calculoAtrasosBB) {
		this.calculoAtrasosBB = calculoAtrasosBB;
	}

}
