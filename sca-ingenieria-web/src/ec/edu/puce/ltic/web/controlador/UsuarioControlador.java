/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			UsuarioControlador.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import ec.edu.puce.ltic.sca.dto.PersonaDto;
import ec.edu.puce.ltic.sca.dto.UsuarioDto;
import ec.edu.puce.ltic.sca.entidad.Usuario;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.web.bb.UsuarioBB;
import ec.edu.puce.ltic.web.controlador.base.ControladorBase;

/**
 * Controlador para el manejo de usuarios.
 * 
 * @author Luis García Castro
 */
@ViewScoped
@ManagedBean(name = "usuarioControlador")
public class UsuarioControlador extends ControladorBase<UsuarioDto, Usuario> implements Serializable {

	private static final long serialVersionUID = 3250873488549496836L;

	@ManagedProperty("#{usuarioBB}")
	private UsuarioBB usuarioBB;

	@EJB
	private UsuarioDto usuarioDto;

	@EJB
	private PersonaDto personaDto;

	public UsuarioControlador() {
		super(UsuarioDto.class, Usuario.class);
	}

	@PostConstruct
	public void init() {
		try {
			this.usuarioBB.setListaUsuarios(this.usuarioDto.buscarTodos());
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	/**
	 * Ejecuta la acción de generar el usuario de la persona registrada.
	 */
	public void generarUsuario() {
		try {
			personaDto.generarUsuario(this.usuarioBB.getUsuario().getPersona());
			showInfoMessage("Se reinició la contraseña.");
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	/**
	 * Permite configurar el estado del usuario.
	 */
	@Override
	public void configurarEstado() {
		super.dto = this.usuarioDto;
		super.objeto = this.usuarioBB.getUsuario();
	}

	public UsuarioBB getUsuarioBB() {
		return usuarioBB;
	}

	public void setUsuarioBB(UsuarioBB usuarioBB) {
		this.usuarioBB = usuarioBB;
	}

}
