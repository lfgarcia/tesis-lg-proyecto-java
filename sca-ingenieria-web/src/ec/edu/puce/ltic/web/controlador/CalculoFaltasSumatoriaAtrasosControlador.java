/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			CalculoFaltasSumatoriaAtrasosControlador.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.controlador;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.reporte.dao.ReporteCalculoFaltasSumatoriaAtrasosDao;
import ec.edu.puce.ltic.web.bb.CalculoFaltasSumatoriaAtrasosBB;
import ec.edu.puce.ltic.web.controlador.base.ControladorReporteBase;

/**
 * Controlador para la visualización del reporte específico.
 * 
 * @author Luis García Castro
 */
@ViewScoped
@ManagedBean(name = "calculoFaltasSumatoriaAtrasosControlador")
public class CalculoFaltasSumatoriaAtrasosControlador extends ControladorReporteBase implements Serializable {

	private static final long serialVersionUID = -1940714461024849589L;

	@ManagedProperty("#{calculoFaltasSumatoriaAtrasosBB}")
	private CalculoFaltasSumatoriaAtrasosBB calculoFaltasSumatoriaAtrasosBB;

	@EJB
	private ReporteCalculoFaltasSumatoriaAtrasosDao calculoFaltasSumatoriaAtrasosDao;

	@PostConstruct
	public void init() {
		try {
			this.consultaInicial(this.calculoFaltasSumatoriaAtrasosBB.getPersonas(), this.calculoFaltasSumatoriaAtrasosBB.getPeriodos());
		} catch (ScaExcepcion ex) {
			showErrorMessage(ex.getMensaje());
			// ex.printStackTrace();
		}
	}

	@Override
	public void configurarReporte() throws ScaExcepcion {
		this.calculoFaltasSumatoriaAtrasosBB.setReporte(this.calculoFaltasSumatoriaAtrasosDao.buscarTodos());
	}

	public CalculoFaltasSumatoriaAtrasosBB getCalculoFaltasSumatoriaAtrasosBB() {
		return calculoFaltasSumatoriaAtrasosBB;
	}

	public void setCalculoFaltasSumatoriaAtrasosBB(CalculoFaltasSumatoriaAtrasosBB calculoFaltasSumatoriaAtrasosBB) {
		this.calculoFaltasSumatoriaAtrasosBB = calculoFaltasSumatoriaAtrasosBB;
	}

}
