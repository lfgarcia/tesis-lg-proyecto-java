/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ParametroBB.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.bb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import ec.edu.puce.ltic.sca.entidad.Parametro;
import ec.edu.puce.ltic.sca.enums.TiposParametrosEnum;
import ec.edu.puce.ltic.web.controlador.base.BackingBeanBase;

/**
 * BackingBean para los parámetros.
 * 
 * @author Luis García Castro
 */
@SessionScoped
@ManagedBean(name = "parametroBB")
public class ParametroBB extends BackingBeanBase implements Serializable {

	private static final long serialVersionUID = 4240451518052886933L;

	private List<Parametro> listaParametros;

	private Parametro parametro = new Parametro();

	private List<SelectItem> listaTiposParametros;

	private Long idTipoParametro;

	private String codigoTipoParametro;

	private Boolean isTipoFecha;

	private Boolean isTipoHora;

	private Boolean isTipoEntero;

	private Boolean isTipoDecimal;

	private Boolean isTipoTexto;

	public ParametroBB() {
		this.listaParametros = new ArrayList<>();
		this.listaTiposParametros = new ArrayList<>();
	}

	public Boolean comprobarTipoFecha(String tipoParametro) {
		if (tipoParametro != null && tipoParametro.equalsIgnoreCase(TiposParametrosEnum.VALOR_FECHA.getCodigo())) {
			return true;
		}
		return false;
	}

	public Boolean comprobarTipoHora(String tipoParametro) {
		if (tipoParametro != null && tipoParametro.equalsIgnoreCase(TiposParametrosEnum.VALOR_HORA.getCodigo())) {
			return true;
		}
		return false;
	}

	public Boolean comprobarTipoEntero(String tipoParametro) {
		if (tipoParametro != null && tipoParametro.equalsIgnoreCase(TiposParametrosEnum.VALOR_ENTERO.getCodigo())) {
			return true;
		}
		return false;
	}

	public Boolean comprobarTipoDecimal(String tipoParametro) {
		if (tipoParametro != null && tipoParametro.equalsIgnoreCase(TiposParametrosEnum.VALOR_DECIMAL.getCodigo())) {
			return true;
		}
		return false;
	}

	public Boolean comprobarTipoTexto(String tipoParametro) {
		if (tipoParametro != null && tipoParametro.equalsIgnoreCase(TiposParametrosEnum.VALOR_TEXTO.getCodigo())) {
			return true;
		}
		return false;
	}

	public List<Parametro> getListaParametros() {
		return listaParametros;
	}

	public void setListaParametros(List<Parametro> listaParametros) {
		this.listaParametros = listaParametros;
	}

	public List<SelectItem> getListaTiposParametros() {
		return listaTiposParametros;
	}

	public void setListaTiposParametros(List<SelectItem> listaTiposParametros) {
		this.listaTiposParametros = listaTiposParametros;
	}

	public Long getIdTipoParametro() {
		return idTipoParametro;
	}

	public void setIdTipoParametro(Long idTipoParametro) {
		this.idTipoParametro = idTipoParametro;
	}

	public Parametro getParametro() {
		return parametro;
	}

	public void setParametro(Parametro parametro) {
		this.parametro = parametro;
	}

	public String getCodigoTipoParametro() {
		return codigoTipoParametro;
	}

	public void setCodigoTipoParametro(String codigoTipoParametro) {
		this.codigoTipoParametro = codigoTipoParametro;
	}

	public Boolean getIsTipoFecha() {
		return isTipoFecha;
	}

	public void setIsTipoFecha(Boolean isTipoFecha) {
		this.isTipoFecha = isTipoFecha;
	}

	public Boolean getIsTipoHora() {
		return isTipoHora;
	}

	public void setIsTipoHora(Boolean isTipoHora) {
		this.isTipoHora = isTipoHora;
	}

	public Boolean getIsTipoEntero() {
		return isTipoEntero;
	}

	public void setIsTipoEntero(Boolean isTipoEntero) {
		this.isTipoEntero = isTipoEntero;
	}

	public Boolean getIsTipoDecimal() {
		return isTipoDecimal;
	}

	public void setIsTipoDecimal(Boolean isTipoDecimal) {
		this.isTipoDecimal = isTipoDecimal;
	}

	public Boolean getIsTipoTexto() {
		return isTipoTexto;
	}

	public void setIsTipoTexto(Boolean isTipoTexto) {
		this.isTipoTexto = isTipoTexto;
	}

}
