/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			CalculoHorasAyudantiaBB.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.bb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import ec.edu.puce.ltic.sca.reporte.entidad.ReporteCalculoHorasAyudantia;
import ec.edu.puce.ltic.web.controlador.base.BackingBeanReporteBase;

/**
 * BackingBean para el reporte específico.
 * 
 * @author Luis García Castro
 */
@SessionScoped
@ManagedBean(name = "calculoHorasAyudantiaBB")
public class CalculoHorasAyudantiaBB extends BackingBeanReporteBase implements Serializable {

	private static final long serialVersionUID = -434409937389804054L;

	private List<ReporteCalculoHorasAyudantia> reporte;

	public CalculoHorasAyudantiaBB() {
		super();
		this.reporte = new ArrayList<>();
	}

	public List<ReporteCalculoHorasAyudantia> getReporte() {
		return reporte;
	}

	public void setReporte(List<ReporteCalculoHorasAyudantia> reporte) {
		this.reporte = reporte;
	}

}
