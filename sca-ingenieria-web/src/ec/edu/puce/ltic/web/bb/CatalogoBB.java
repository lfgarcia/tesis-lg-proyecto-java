/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			CatalogoBB.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.bb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import ec.edu.puce.ltic.sca.entidad.Catalogo;
import ec.edu.puce.ltic.web.controlador.base.BackingBeanBase;

/**
 * BackingBean para los catálogos.
 * 
 * @author Luis García Castro
 */
@SessionScoped
@ManagedBean(name = "catalogoBB")
public class CatalogoBB extends BackingBeanBase implements Serializable {

	private static final long serialVersionUID = -3011118638261969378L;

	private List<Catalogo> listaCatalogos;

	private Catalogo catalogo = new Catalogo();

	private List<SelectItem> listaTiposCatalogos;

	private Long idTipoCatalogo;

	public CatalogoBB() {
		this.listaCatalogos = new ArrayList<>();
		this.listaTiposCatalogos = new ArrayList<>();
	}

	public List<Catalogo> getListaCatalogos() {
		return listaCatalogos;
	}

	public void setListaCatalogos(List<Catalogo> listaCatalogos) {
		this.listaCatalogos = listaCatalogos;
	}

	public Catalogo getCatalogo() {
		return catalogo;
	}

	public void setCatalogo(Catalogo catalogo) {
		this.catalogo = catalogo;
	}

	public List<SelectItem> getListaTiposCatalogos() {
		return listaTiposCatalogos;
	}

	public void setListaTiposCatalogos(List<SelectItem> listaTiposCatalogos) {
		this.listaTiposCatalogos = listaTiposCatalogos;
	}

	public Long getIdTipoCatalogo() {
		return idTipoCatalogo;
	}

	public void setIdTipoCatalogo(Long idTipoCatalogo) {
		this.idTipoCatalogo = idTipoCatalogo;
	}

}
