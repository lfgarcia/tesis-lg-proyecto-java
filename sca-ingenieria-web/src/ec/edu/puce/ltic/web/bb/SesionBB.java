/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			SesionBB.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.bb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import ec.edu.puce.ltic.sca.entidad.OpcionMenu;
import ec.edu.puce.ltic.sca.entidad.Usuario;
import ec.edu.puce.ltic.web.controlador.base.BackingBeanBase;

/**
 * BackingBean para la sesión.
 * 
 * @author Luis García Castro
 */
@SessionScoped
@ManagedBean(name = "sesionBB")
public class SesionBB extends BackingBeanBase implements Serializable {

	private static final long serialVersionUID = 4605899052200690681L;

	private Usuario usuario;

	private List<OpcionMenu> menu;

	private String username;

	private String password;

	private String codigoPerfil;

	private String oldPassword;

	private String newPassword;

	private Integer alumnosPorHora;

	private String identificacionPuce;

	private String usernameRecovery;

	private Date fechaVerificacion;

	private String codigoVerificacion;

	private String confirmarCodigo;

	public SesionBB() {
		this.menu = new ArrayList<>();
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<OpcionMenu> getMenu() {
		return menu;
	}

	public void setMenu(List<OpcionMenu> menu) {
		this.menu = menu;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCodigoPerfil() {
		return codigoPerfil;
	}

	public void setCodigoPerfil(String codigoPerfil) {
		this.codigoPerfil = codigoPerfil;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public Integer getAlumnosPorHora() {
		return alumnosPorHora;
	}

	public void setAlumnosPorHora(Integer alumnosPorHora) {
		this.alumnosPorHora = alumnosPorHora;
	}

	public String getIdentificacionPuce() {
		return identificacionPuce;
	}

	public void setIdentificacionPuce(String identificacionPuce) {
		this.identificacionPuce = identificacionPuce;
	}

	public String getUsernameRecovery() {
		return usernameRecovery;
	}

	public void setUsernameRecovery(String usernameRecovery) {
		this.usernameRecovery = usernameRecovery;
	}

	public Date getFechaVerificacion() {
		return fechaVerificacion;
	}

	public void setFechaVerificacion(Date fechaVerificacion) {
		this.fechaVerificacion = fechaVerificacion;
	}

	public String getCodigoVerificacion() {
		return codigoVerificacion;
	}

	public void setCodigoVerificacion(String codigoVerificacion) {
		this.codigoVerificacion = codigoVerificacion;
	}

	public String getConfirmarCodigo() {
		return confirmarCodigo;
	}

	public void setConfirmarCodigo(String confirmarCodigo) {
		this.confirmarCodigo = confirmarCodigo;
	}

}
