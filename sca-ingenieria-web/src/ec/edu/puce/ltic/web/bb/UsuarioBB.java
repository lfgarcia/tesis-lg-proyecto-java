/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			UsuarioBB.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.bb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import ec.edu.puce.ltic.sca.entidad.Usuario;
import ec.edu.puce.ltic.web.controlador.base.BackingBeanBase;

/**
 * BackingBean para los usuarios.
 * 
 * @author Luis García Castro
 */
@SessionScoped
@ManagedBean(name = "usuarioBB")
public class UsuarioBB extends BackingBeanBase implements Serializable {

	private static final long serialVersionUID = 6207826012381303779L;

	private List<Usuario> listaUsuarios;

	private Usuario usuario;

	public UsuarioBB() {
		this.listaUsuarios = new ArrayList<>();
	}

	public List<Usuario> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<Usuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
