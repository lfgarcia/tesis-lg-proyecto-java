/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			PersonaBB.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.bb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import ec.edu.puce.ltic.sca.entidad.Persona;
import ec.edu.puce.ltic.web.controlador.base.BackingBeanBase;

/**
 * BackingBean para las personas.
 * 
 * @author Luis García Castro
 */
@SessionScoped
@ManagedBean(name = "personaBB")
public class PersonaBB extends BackingBeanBase implements Serializable {

	private static final long serialVersionUID = 8554457242330576434L;

	private List<Persona> listaPersonas;

	private Persona persona = new Persona();

	private List<SelectItem> listaTiposIdentificacion;

	private List<SelectItem> listaGeneros;

	private Long idTipoIdentificacion;

	private Long idGenero;

	private List<SelectItem> listaPerfiles;

	private Long idPerfil;

	public PersonaBB() {
		this.listaPersonas = new ArrayList<>();
		this.listaTiposIdentificacion = new ArrayList<>();
		this.listaGeneros = new ArrayList<>();
		this.listaPerfiles = new ArrayList<>();
	}

	public List<Persona> getListaPersonas() {
		return listaPersonas;
	}

	public void setListaPersonas(List<Persona> listaPersonas) {
		this.listaPersonas = listaPersonas;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public List<SelectItem> getListaTiposIdentificacion() {
		return listaTiposIdentificacion;
	}

	public void setListaTiposIdentificacion(List<SelectItem> listaTiposIdentificacion) {
		this.listaTiposIdentificacion = listaTiposIdentificacion;
	}

	public List<SelectItem> getListaGeneros() {
		return listaGeneros;
	}

	public void setListaGeneros(List<SelectItem> listaGeneros) {
		this.listaGeneros = listaGeneros;
	}

	public Long getIdTipoIdentificacion() {
		return idTipoIdentificacion;
	}

	public void setIdTipoIdentificacion(Long idTipoIdentificacion) {
		this.idTipoIdentificacion = idTipoIdentificacion;
	}

	public Long getIdGenero() {
		return idGenero;
	}

	public void setIdGenero(Long idGenero) {
		this.idGenero = idGenero;
	}

	public List<SelectItem> getListaPerfiles() {
		return listaPerfiles;
	}

	public void setListaPerfiles(List<SelectItem> listaPerfiles) {
		this.listaPerfiles = listaPerfiles;
	}

	public Long getIdPerfil() {
		return idPerfil;
	}

	public void setIdPerfil(Long idPerfil) {
		this.idPerfil = idPerfil;
	}

}
