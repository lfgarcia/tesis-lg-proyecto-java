/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			CalculoAtrasosBB.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.bb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import ec.edu.puce.ltic.sca.reporte.entidad.ReporteCalculoAtrasos;
import ec.edu.puce.ltic.web.controlador.base.BackingBeanReporteBase;

/**
 * BackingBean para el reporte específico.
 * 
 * @author Luis García Castro
 */
@SessionScoped
@ManagedBean(name = "calculoAtrasosBB")
public class CalculoAtrasosBB extends BackingBeanReporteBase implements Serializable {

	private static final long serialVersionUID = -434409937389804054L;

	private List<ReporteCalculoAtrasos> reporte;

	public CalculoAtrasosBB() {
		super();
		this.reporte = new ArrayList<>();
	}

	public List<ReporteCalculoAtrasos> getReporte() {
		return reporte;
	}

	public void setReporte(List<ReporteCalculoAtrasos> reporte) {
		this.reporte = reporte;
	}

}
