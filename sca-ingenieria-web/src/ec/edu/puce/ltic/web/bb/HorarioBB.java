/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			HorarioBB.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.bb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import org.primefaces.model.DualListModel;

import ec.edu.puce.ltic.sca.entidad.HorarioPersona;
import ec.edu.puce.ltic.sca.entidad.Persona;
import ec.edu.puce.ltic.sca.utils.Constantes;
import ec.edu.puce.ltic.sca.vo.HorarioVO;
import ec.edu.puce.ltic.web.controlador.base.BackingBeanBase;

/**
 * BackingBean para los horarios.
 * 
 * @author Luis García Castro
 */
@SessionScoped
@ManagedBean(name = "horarioBB")
public class HorarioBB extends BackingBeanBase implements Serializable {

	private static final long serialVersionUID = -7430222332638394862L;

	private List<HorarioVO> horario;

	private HorarioVO hora;

	private String lunes = Constantes.CODIGO_LUNES;

	private String martes = Constantes.CODIGO_MARTES;

	private String miercoles = Constantes.CODIGO_MIERCOLES;

	private String jueves = Constantes.CODIGO_JUEVES;

	private String viernes = Constantes.CODIGO_VIERNES;

	private String codigoDia;

	private DualListModel<Persona> asociacion;

	private List<HorarioPersona> personas;

	private List<SelectItem> listaPersonas;

	private Long idPersona;

	private Boolean esJustificada;

	private String codigoPeriodo;

	private String descripcionPeriodo;

	private Boolean periodoActivo;

	private List<SelectItem> listaCompletaPersona;

	private Date horaEntrada;

	private Date horaSalida;

	private Date diaReasignado;

	public HorarioBB() {
		this.horario = new ArrayList<>();
		this.asociacion = new DualListModel<>();
		this.personas = new ArrayList<>();
		this.listaPersonas = new ArrayList<>();
		this.listaCompletaPersona = new ArrayList<>();
	}

	public List<HorarioVO> getHorario() {
		return horario;
	}

	public void setHorario(List<HorarioVO> horario) {
		this.horario = horario;
	}

	public HorarioVO getHora() {
		return hora;
	}

	public void setHora(HorarioVO hora) {
		this.hora = hora;
	}

	public String getLunes() {
		return lunes;
	}

	public void setLunes(String lunes) {
		this.lunes = lunes;
	}

	public String getMartes() {
		return martes;
	}

	public void setMartes(String martes) {
		this.martes = martes;
	}

	public String getMiercoles() {
		return miercoles;
	}

	public void setMiercoles(String miercoles) {
		this.miercoles = miercoles;
	}

	public String getJueves() {
		return jueves;
	}

	public void setJueves(String jueves) {
		this.jueves = jueves;
	}

	public String getViernes() {
		return viernes;
	}

	public void setViernes(String viernes) {
		this.viernes = viernes;
	}

	public String getCodigoDia() {
		return codigoDia;
	}

	public void setCodigoDia(String codigoDia) {
		this.codigoDia = codigoDia;
	}

	public DualListModel<Persona> getAsociacion() {
		return asociacion;
	}

	public void setAsociacion(DualListModel<Persona> asociacion) {
		this.asociacion = asociacion;
	}

	public List<HorarioPersona> getPersonas() {
		return personas;
	}

	public void setPersonas(List<HorarioPersona> personas) {
		this.personas = personas;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public List<SelectItem> getListaPersonas() {
		return listaPersonas;
	}

	public void setListaPersonas(List<SelectItem> listaPersonas) {
		this.listaPersonas = listaPersonas;
	}

	public Boolean getEsJustificada() {
		return esJustificada;
	}

	public void setEsJustificada(Boolean esJustificada) {
		this.esJustificada = esJustificada;
	}

	public String getCodigoPeriodo() {
		return codigoPeriodo;
	}

	public void setCodigoPeriodo(String codigoPeriodo) {
		this.codigoPeriodo = codigoPeriodo;
	}

	public String getDescripcionPeriodo() {
		return descripcionPeriodo;
	}

	public void setDescripcionPeriodo(String descripcionPeriodo) {
		this.descripcionPeriodo = descripcionPeriodo;
	}

	public Boolean getPeriodoActivo() {
		return periodoActivo;
	}

	public void setPeriodoActivo(Boolean periodoActivo) {
		this.periodoActivo = periodoActivo;
	}

	public List<SelectItem> getListaCompletaPersona() {
		return listaCompletaPersona;
	}

	public void setListaCompletaPersona(List<SelectItem> listaCompletaPersona) {
		this.listaCompletaPersona = listaCompletaPersona;
	}

	public Date getHoraEntrada() {
		return horaEntrada;
	}

	public void setHoraEntrada(Date horaEntrada) {
		this.horaEntrada = horaEntrada;
	}

	public Date getHoraSalida() {
		return horaSalida;
	}

	public void setHoraSalida(Date horaSalida) {
		this.horaSalida = horaSalida;
	}

	public Date getDiaReasignado() {
		return diaReasignado;
	}

	public void setDiaReasignado(Date diaReasignado) {
		this.diaReasignado = diaReasignado;
	}

}
