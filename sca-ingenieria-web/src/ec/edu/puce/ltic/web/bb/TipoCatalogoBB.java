/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			TipoCatalogoBB.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.bb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import ec.edu.puce.ltic.sca.entidad.TipoCatalogo;
import ec.edu.puce.ltic.web.controlador.base.BackingBeanBase;

/**
 * BackingBean para los tipos de catálogos.
 * 
 * @author Luis García Castro
 */
@SessionScoped
@ManagedBean(name = "tipoCatalogoBB")
public class TipoCatalogoBB extends BackingBeanBase implements Serializable {

	private static final long serialVersionUID = -3011118638261969378L;

	private List<TipoCatalogo> listaTiposCatalogos;

	private TipoCatalogo tipoCatalogo = new TipoCatalogo();

	public TipoCatalogoBB() {
		this.listaTiposCatalogos = new ArrayList<>();
	}

	public List<TipoCatalogo> getListaTiposCatalogos() {
		return listaTiposCatalogos;
	}

	public void setListaTiposCatalogos(List<TipoCatalogo> listaTiposCatalogos) {
		this.listaTiposCatalogos = listaTiposCatalogos;
	}

	public TipoCatalogo getTipoCatalogo() {
		return tipoCatalogo;
	}

	public void setTipoCatalogo(TipoCatalogo tipoCatalogo) {
		this.tipoCatalogo = tipoCatalogo;
	}

}
