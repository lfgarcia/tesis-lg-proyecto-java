/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ReporteBB.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.web.bb;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 * BackingBean para los reportes.
 * 
 * @author Luis García Castro
 */
@SessionScoped
@ManagedBean(name = "reporteBB")
public class ReporteBB implements Serializable {

	private static final long serialVersionUID = -270299359840530271L;

	private String reporte;

	private String titulo;

	private String periodo;

	private String paginaPrevia;

	public ReporteBB() {
	}

	public String getReporte() {
		return reporte;
	}

	public void setReporte(String reporte) {
		this.reporte = reporte;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getPaginaPrevia() {
		return paginaPrevia;
	}

	public void setPaginaPrevia(String paginaPrevia) {
		this.paginaPrevia = paginaPrevia;
	}

}
