/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			HorarioVO.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.vo;

import java.io.Serializable;
import java.util.List;

import ec.edu.puce.ltic.sca.entidad.HorarioPersona;

/**
 * Clase para guardar información referente al Horario.
 * 
 * @author Luis García Castro
 */
public class HorarioVO implements Serializable {

	private static final long serialVersionUID = 196364532280038435L;

	private String horaEntrada;

	private String horaSalida;

	private List<HorarioPersona> lunes;

	private List<HorarioPersona> martes;

	private List<HorarioPersona> miercoles;

	private List<HorarioPersona> jueves;

	private List<HorarioPersona> viernes;

	private List<HorarioPersona> sabado;

	private List<HorarioPersona> domingo;

	@Override
	public String toString() {
		return "HorarioVO : { horaEntrada : " + horaEntrada + ", horaSalida : " + horaSalida + ", lunes : " + lunes + ", martes : " + martes + ", miercoles : " + miercoles + ", jueves : " + jueves + ", viernes : " + viernes + ", sabado : " + sabado + ", domingo : " + domingo + " }";
	}

	public String getHoraEntrada() {
		return horaEntrada;
	}

	public void setHoraEntrada(String horaEntrada) {
		this.horaEntrada = horaEntrada;
	}

	public String getHoraSalida() {
		return horaSalida;
	}

	public void setHoraSalida(String horaSalida) {
		this.horaSalida = horaSalida;
	}

	public List<HorarioPersona> getLunes() {
		return lunes;
	}

	public void setLunes(List<HorarioPersona> lunes) {
		this.lunes = lunes;
	}

	public List<HorarioPersona> getMartes() {
		return martes;
	}

	public void setMartes(List<HorarioPersona> martes) {
		this.martes = martes;
	}

	public List<HorarioPersona> getMiercoles() {
		return miercoles;
	}

	public void setMiercoles(List<HorarioPersona> miercoles) {
		this.miercoles = miercoles;
	}

	public List<HorarioPersona> getJueves() {
		return jueves;
	}

	public void setJueves(List<HorarioPersona> jueves) {
		this.jueves = jueves;
	}

	public List<HorarioPersona> getViernes() {
		return viernes;
	}

	public void setViernes(List<HorarioPersona> viernes) {
		this.viernes = viernes;
	}

	public List<HorarioPersona> getSabado() {
		return sabado;
	}

	public void setSabado(List<HorarioPersona> sabado) {
		this.sabado = sabado;
	}

	public List<HorarioPersona> getDomingo() {
		return domingo;
	}

	public void setDomingo(List<HorarioPersona> domingo) {
		this.domingo = domingo;
	}
}
