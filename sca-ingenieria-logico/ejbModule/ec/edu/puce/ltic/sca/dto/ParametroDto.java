/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ParametroDto.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dto;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

import ec.edu.puce.ltic.sca.dao.CatalogoDao;
import ec.edu.puce.ltic.sca.dao.ParametroDao;
import ec.edu.puce.ltic.sca.entidad.Catalogo;
import ec.edu.puce.ltic.sca.entidad.Parametro;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.utils.Constantes;

/**
 * Transacciones con la base de datos para los parámetros.
 * 
 * @author Luis García Castro
 */
@Stateless
@LocalBean
public class ParametroDto implements Serializable {

	private static final long serialVersionUID = 2247006332474660682L;

	@EJB
	private ParametroDao parametroDao;

	@EJB
	private CatalogoDao catalogoDao;

	/**
	 * Búqueda de todos los parámetros.
	 * 
	 * @return Lista de parámetros.
	 * @throws ScaExcepcion
	 */
	public List<Parametro> buscarTodos() throws ScaExcepcion {
		return this.parametroDao.buscarTodos(Constantes.fetchTipoParametro);
	}

	/**
	 * Permite realizar la creación o actualización en la base de datos.
	 * 
	 * @param isNuevo
	 * @param parametro
	 * @param codigoTipoParametro
	 * @return Parametro en base de datos.
	 * @throws ScaExcepcion
	 */
	@Transactional
	public Parametro guardar(final Boolean isNuevo, Parametro parametro, final String codigoTipoParametro) throws ScaExcepcion {
		Parametro found = this.parametroDao.buscarPorCodigo(parametro.getCodigo());
		if (isNuevo && found != null) {
			throw new ScaExcepcion("UK-CODIGO", "El parámetro con código: " + parametro.getCodigo() + " ya existe.");
		} else if (!isNuevo && found != null && found.getId().intValue() != parametro.getId().intValue()) {
			throw new ScaExcepcion("UK-CODIGO", "El parámetro con código: " + parametro.getCodigo() + " ya existe.");
		}
		Catalogo tipoParametro = this.catalogoDao.buscarPorCodigo(codigoTipoParametro);
		parametro.setTipoParametro(tipoParametro);
		if (isNuevo) {
			this.parametroDao.create(parametro);
		} else {
			this.parametroDao.update(parametro);
		}
		return parametro;
	}

	/**
	 * Permite activar o desactivar un registro en la base de datos.
	 * 
	 * @param parametro
	 * @return Parámetro en base de datos.
	 * @throws ScaExcepcion
	 */
	@Transactional
	public Parametro activarDesactivar(Parametro parametro) throws ScaExcepcion {
		parametro.setEstado(!parametro.getEstado());
		parametro = this.parametroDao.update(parametro);
		return parametro;
	}

	public CatalogoDao getCatalogoDao() {
		return catalogoDao;
	}

}
