/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			PermisoDto.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dto;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

import ec.edu.puce.ltic.sca.dao.HorarioPersonaDao;
import ec.edu.puce.ltic.sca.dao.PermisoDao;
import ec.edu.puce.ltic.sca.entidad.HorarioPersona;
import ec.edu.puce.ltic.sca.entidad.Permiso;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;

/**
 * Transacciones con la base de datos para los permisos.
 * 
 * @author Luis García Castro
 */
@Stateless
@LocalBean
public class PermisoDto implements Serializable {

	private static final long serialVersionUID = -1076661423691244359L;

	@EJB
	private HorarioPersonaDao horarioPersonaDao;

	@EJB
	private PermisoDao permisoDao;

	/**
	 * Permite realizar el registro de un permiso en el sistema.
	 * 
	 * @param codigoDia
	 * @param horaEntrada
	 * @param horaSalida
	 * @param idPersona
	 * @param esJustificada
	 * @throws ScaExcepcion
	 */
	@Transactional
	public void registrarPermiso(String codigoDia, String horaEntrada, String horaSalida, Long idPersona, Boolean esJustificada) throws ScaExcepcion {
		HorarioPersona horarioPersona = this.horarioPersonaDao.buscarPorHorarioPersona(codigoDia, horaEntrada, horaSalida, idPersona);
		if (horarioPersona == null) {
			throw new ScaExcepcion("NULL", "No se encontró al alumno registrado en ese horario.");
		}
		Permiso permiso = new Permiso();
		permiso.setDia(horarioPersona.getHorario().getDia());
		permiso.setHoraEntrada(horarioPersona.getHorario().getHoraEntrada());
		permiso.setHoraSalida(horarioPersona.getHorario().getHoraSalida());
		permiso.setPersona(horarioPersona.getPersona());
		permiso.setEsJustificada(esJustificada);
		this.permisoDao.create(permiso);
	}

}
