/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			TipoCatalogoDto.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dto;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

import ec.edu.puce.ltic.sca.dao.TipoCatalogoDao;
import ec.edu.puce.ltic.sca.entidad.TipoCatalogo;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;

/**
 * Transacciones con la base de datos para los tipos de catálogos.
 * 
 * @author Luis García Castro
 */
@Stateless
@LocalBean
public class TipoCatalogoDto implements Serializable {

	private static final long serialVersionUID = -2688795364172621980L;

	@EJB
	private TipoCatalogoDao tipoCatalogoDao;

	/**
	 * Búsqueda de todos los tipos de catálogos en base de datos.
	 * 
	 * @return Lista de tipos de catálogos en base de datos.
	 * @throws ScaExcepcion
	 */
	public List<TipoCatalogo> buscarTodos() throws ScaExcepcion {
		return this.tipoCatalogoDao.buscarTodos();
	}

	/**
	 * Búsqueda de todos los tipos de catálogos en base de datos que se
	 * encuentran con estado activo.
	 * 
	 * @return Lista de tipos de catálogos en base de datos.
	 * @throws ScaExcepcion
	 */
	public List<TipoCatalogo> buscarTodosPorEstadoActivo() throws ScaExcepcion {
		return this.tipoCatalogoDao.buscarTodosPorEstadoActivo();
	}

	/**
	 * Permite realizar la creación o actualización en la base de datos.
	 * 
	 * @param isNuevo
	 * @param tipoCatalogo
	 * @return Tipo de catálogo en base de datos.
	 * @throws ScaExcepcion
	 */
	@Transactional
	public TipoCatalogo guardar(final Boolean isNuevo, TipoCatalogo tipoCatalogo) throws ScaExcepcion {
		TipoCatalogo found = this.tipoCatalogoDao.buscarPorCodigo(tipoCatalogo.getCodigo());
		if (isNuevo && found != null) {
			throw new ScaExcepcion("UK-CODIGO", "El tipo de catálogo con código: " + tipoCatalogo.getCodigo() + " ya existe.");
		} else if (!isNuevo && found != null && found.getId().intValue() != tipoCatalogo.getId().intValue()) {
			throw new ScaExcepcion("UK-CODIGO", "El tipo de catálogo con código: " + tipoCatalogo.getCodigo() + " ya existe.");
		}
		if (isNuevo) {
			this.tipoCatalogoDao.create(tipoCatalogo);
		} else {
			this.tipoCatalogoDao.update(tipoCatalogo);
		}
		return tipoCatalogo;
	}

	/**
	 * Permite activar o desactivar un registro en la base de datos.
	 * 
	 * @param tipoCatalogo
	 * @return Tipo de catálogo en base de datos.
	 * @throws ScaExcepcion
	 */
	@Transactional
	public TipoCatalogo activarDesactivar(TipoCatalogo tipoCatalogo) throws ScaExcepcion {
		tipoCatalogo.setEstado(!tipoCatalogo.getEstado());
		tipoCatalogo = this.tipoCatalogoDao.update(tipoCatalogo);
		return tipoCatalogo;
	}

}
