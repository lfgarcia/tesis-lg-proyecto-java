/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			HorarioDto.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

import ec.edu.puce.ltic.sca.dao.CatalogoDao;
import ec.edu.puce.ltic.sca.dao.HorarioDao;
import ec.edu.puce.ltic.sca.dao.HorarioPersonaDao;
import ec.edu.puce.ltic.sca.dao.HorarioReasignacionDao;
import ec.edu.puce.ltic.sca.dao.ParametroDao;
import ec.edu.puce.ltic.sca.dao.PersonaDao;
import ec.edu.puce.ltic.sca.dao.TipoCatalogoDao;
import ec.edu.puce.ltic.sca.entidad.Catalogo;
import ec.edu.puce.ltic.sca.entidad.Horario;
import ec.edu.puce.ltic.sca.entidad.HorarioPersona;
import ec.edu.puce.ltic.sca.entidad.HorarioReasignacion;
import ec.edu.puce.ltic.sca.entidad.Parametro;
import ec.edu.puce.ltic.sca.entidad.Persona;
import ec.edu.puce.ltic.sca.entidad.TipoCatalogo;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.utils.Constantes;
import ec.edu.puce.ltic.sca.vo.HorarioVO;

/**
 * Transacciones con la base de datos para el horario.
 * 
 * @author Luis García Castro
 */
@Stateless
@LocalBean
public class HorarioDto implements Serializable {

	private static final long serialVersionUID = -8673306818842483531L;

	@EJB
	private HorarioDao horarioDao;

	@EJB
	private PersonaDao personaDao;

	@EJB
	private HorarioPersonaDao horarioPersonaDao;

	@EJB
	private TipoCatalogoDao tipoCatalogoDao;

	@EJB
	private CatalogoDao catalogoDao;

	@EJB
	private ParametroDao parametroDao;

	@EJB
	private HorarioReasignacionDao horarioReasignacionDao;

	/**
	 * Búsqueda de todos los horarios armados en una variable completa.
	 * 
	 * @return
	 * @throws ScaExcepcion
	 */
	public List<HorarioVO> buscarTodosHorarios() throws ScaExcepcion {
		List<Horario> horarios = this.horarioDao.buscarTodos();
		List<HorarioVO> resultado = new ArrayList<>();
		while (!horarios.isEmpty()) {
			HorarioVO vo = new HorarioVO();
			vo.setHoraEntrada(horarios.get(0).getHoraEntrada().toString());
			vo.setHoraSalida(horarios.get(0).getHoraSalida().toString());
			for (int j = 0; j < 5; j++) {
				switch (horarios.get(0).getDia().getCodigo()) {
				case Constantes.CODIGO_LUNES:
					vo.setLunes(horarios.get(j).getPersonas());
					break;
				case Constantes.CODIGO_MARTES:
					vo.setMartes(horarios.get(j - 1).getPersonas());
					break;
				case Constantes.CODIGO_MIERCOLES:
					vo.setMiercoles(horarios.get(j - 2).getPersonas());
					break;
				case Constantes.CODIGO_JUEVES:
					vo.setJueves(horarios.get(j - 3).getPersonas());
					break;
				case Constantes.CODIGO_VIERNES:
					vo.setViernes(horarios.get(j - 4).getPersonas());
					break;
				}
				horarios.remove(0);
			}
			resultado.add(vo);
		}
		return resultado;
	}

	/**
	 * Permite guardar la información actualizada en el horario indicado.
	 * 
	 * @param horaEntrada
	 * @param personas
	 * @return Horario guardado en base.
	 * @throws ScaExcepcion
	 */
	@Transactional
	public Horario guardar(String horaEntrada, String codigoDia, List<Persona> personas) throws ScaExcepcion {
		Horario horario = this.horarioDao.buscarPorHoraEntradaCodigoDia(horaEntrada, codigoDia);
		for (HorarioPersona hp : horario.getPersonas()) {
			this.horarioPersonaDao.delete(hp);
		}
		horario.setPersonas(new ArrayList<>());
		for (Persona persona : personas) {
			HorarioPersona hp = new HorarioPersona();
			hp.setHorario(horario);
			hp.setPersona(persona);
			hp = this.horarioPersonaDao.create(hp);
		}
		return horario;
	}

	/**
	 * Permite cerrar el período académico, lo cual seteará el parámetro de
	 * período actual en nulo.
	 * 
	 * @throws ScaExcepcion
	 */
	@Transactional
	public void cerrarPeriodoAcademico() throws ScaExcepcion {
		Parametro parametro = this.parametroDao.buscarPorCodigo(Constantes.PERIODO_ACTUAL);
		TipoCatalogo tc = this.tipoCatalogoDao.buscarPorCodigo(Constantes.PERIODO_ACADEMICO);
		Catalogo catalogo = new Catalogo();
		catalogo.setTipoCatalogo(tc);
		catalogo.setCodigo(parametro.getValorTexto());
		catalogo.setNombre(parametro.getDescripcion());
		this.catalogoDao.create(catalogo);
		parametro.setDescripcion(null);
		parametro.setValorTexto(null);
		this.parametroDao.update(parametro);
		List<HorarioPersona> horariosPersona = this.horarioPersonaDao.buscarTodos();
		for (HorarioPersona hp : horariosPersona) {
			this.horarioPersonaDao.delete(hp);
		}
	}

	/**
	 * Permite iniciar el período académico, lo cual permitira configurar los
	 * valores para el parámetro de período actual.
	 * 
	 * @param codigo
	 * @param descripcion
	 * @throws ScaExcepcion
	 */
	@Transactional
	public void iniciarPeriodoAcademico(final String codigo, final String descripcion) throws ScaExcepcion {
		Parametro parametro = this.parametroDao.buscarPorCodigo(Constantes.PERIODO_ACTUAL);
		parametro.setDescripcion(descripcion);
		parametro.setValorTexto(codigo);
		this.parametroDao.update(parametro);
	}

	/**
	 * Permite registrar la reasignación de horas a los alumnos.
	 * 
	 * @param codigo
	 * @param descripcion
	 * @throws ScaExcepcion
	 */
	@Transactional
	public void registrarReasignacion(final Long personaId, final Date diaReasignacion, final Date horaEntrada, final Date horaSalida) throws ScaExcepcion {
		Persona persona = this.personaDao.buscarPorId(personaId);
		HorarioReasignacion horarioReasignacion = new HorarioReasignacion();
		horarioReasignacion.setPersona(persona);
		horarioReasignacion.setDiaReasignacion(diaReasignacion);
		horarioReasignacion.setHoraEntradaReasignacion(horaEntrada);
		horarioReasignacion.setHoraSalidaReasignacion(horaSalida);
		this.horarioReasignacionDao.create(horarioReasignacion);
	}

	public List<HorarioPersona> buscarHorarioPorPersona() throws ScaExcepcion {
		return this.horarioPersonaDao.buscarTodos();
	}

	public HorarioDao getHorarioDao() {
		return horarioDao;
	}

	public PersonaDao getPersonaDao() {
		return personaDao;
	}

	public ParametroDao getParametroDao() {
		return parametroDao;
	}

	public HorarioReasignacionDao getHorarioReasignacionDao() {
		return horarioReasignacionDao;
	}

}
