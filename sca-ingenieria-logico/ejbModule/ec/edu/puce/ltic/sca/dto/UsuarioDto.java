/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			UsuarioDto.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.transaction.Transactional;

import ec.edu.puce.ltic.sca.dao.AsociacionOpcionMenuDao;
import ec.edu.puce.ltic.sca.dao.ParametroDao;
import ec.edu.puce.ltic.sca.dao.UsuarioDao;
import ec.edu.puce.ltic.sca.entidad.AsociacionOpcionMenu;
import ec.edu.puce.ltic.sca.entidad.OpcionMenu;
import ec.edu.puce.ltic.sca.entidad.Parametro;
import ec.edu.puce.ltic.sca.entidad.Usuario;
import ec.edu.puce.ltic.sca.excepcion.EncodeExcepcion;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.utils.Constantes;
import ec.edu.puce.ltic.sca.utils.EncriptarSha;
import ec.edu.puce.ltic.sca.utils.Mail;
import ec.edu.puce.ltic.sca.utils.MailParametros;
import ec.edu.puce.ltic.sca.utils.UtilSca;

/**
 * Transacciones con la base de datos para los usuarios.
 * 
 * @author Luis García Castro
 */
@Stateless
@LocalBean
public class UsuarioDto implements Serializable {

	private static final long serialVersionUID = 5930199979407733026L;

	@EJB
	private UsuarioDao usuarioDao;

	@EJB
	private AsociacionOpcionMenuDao asociacionOpcionMenuDao;

	@EJB
	private ParametroDao parametroDao;

	@EJB
	private Mail mail;

	/**
	 * Búsqueda de todos los usuarios en base de datos.
	 * 
	 * @return Lista de usuarios en base de datos.
	 * @throws ScaExcepcion
	 */
	public List<Usuario> buscarTodos() throws ScaExcepcion {
		return this.usuarioDao.buscarTodos();
	}

	/**
	 * Permite activar o desactivar un registro en la base de datos.
	 * 
	 * @param usuario
	 * @return Usuario en base de datos.
	 * @throws ScaExcepcion
	 */
	@Transactional
	public Usuario activarDesactivar(Usuario usuario) throws ScaExcepcion {
		usuario.setEstado(!usuario.getEstado());
		usuario = this.usuarioDao.update(usuario);
		return usuario;
	}

	/**
	 * Método que permite hacer el ingreso al sistema.
	 * 
	 * @param username
	 * @param password
	 * @return Acceso de usuario exitoso.
	 * @throws ScaExcepcion
	 */
	public Usuario doLogin(String username, String password) throws ScaExcepcion {
		Usuario usuario = this.usuarioDao.doLogin(username, password);
		List<AsociacionOpcionMenu> asociacionOpcionMenus = this.asociacionOpcionMenuDao.buscarPorPerfilId(usuario.getPersona().getPerfil().getId());
		for (AsociacionOpcionMenu menu : asociacionOpcionMenus) {
			if (menu.getOpcionMenu().getNivel().intValue() == 0) {
				menu.getOpcionMenu().setOpcionesMenu(new ArrayList<>());
				usuario.getMenu().add(asociarMenuRecursivo(menu.getOpcionMenu(), asociacionOpcionMenus));
			}
		}
		return usuario;
	}

	/**
	 * Permite realizar la asociación de las opciones de menú según el perfil
	 * del usuario de forma recursiva.
	 * 
	 * @param menuPadre
	 * @param menus
	 * @return Opciones de menú
	 */
	private OpcionMenu asociarMenuRecursivo(OpcionMenu menuPadre, List<AsociacionOpcionMenu> menus) {
		for (AsociacionOpcionMenu menu : menus) {
			if (menu.getOpcionMenu().getPadre() != null && menu.getOpcionMenu().getPadre().getId().intValue() == menuPadre.getId().intValue()) {
				menuPadre.getOpcionesMenu().add(menu.getOpcionMenu());
			}
		}
		return menuPadre;
	}

	/**
	 * Permite realizar el cambio de contraseña al usuario que ingresa al
	 * sistema.
	 * 
	 * @param usuario
	 * @param oldClave
	 * @param newClave
	 * @return Usuario en base de datos.
	 * @throws ScaExcepcion
	 */
	@Transactional
	public Usuario cambiarClave(Usuario usuario, String oldClave, String newClave) throws ScaExcepcion {
		try {
			if (usuario.getClave().equals(EncriptarSha.encriptar(oldClave))) {
				usuario.setClave(EncriptarSha.encriptar(newClave));
				usuario = this.usuarioDao.update(usuario);
				return usuario;
			} else {
				throw new ScaExcepcion("PASS-01", "La clave ingresada no coincide con la clave actual.");
			}
		} catch (EncodeExcepcion e) {
			throw new ScaExcepcion("ENCODE", "No se pudo codificar la contraseña.");
		}
	}

	/**
	 * Permite recuperar un usuario para el acceso en el sistema.
	 * 
	 * @param persona
	 * @return Persona en base de datos.
	 * @throws ScaExcepcion
	 */
	@Transactional
	public void recuperarClave(String username) throws ScaExcepcion {
		try {
			String claveGenerada = UtilSca.generarClaveAleatoria(8);
			Usuario found = this.usuarioDao.comprobarUsuario(username);
			if (found != null) {
				found.setClave(EncriptarSha.encriptar(claveGenerada));
				found = this.usuarioDao.update(found);

				MailParametros mailParametros;
				if (found.getUsuario().equalsIgnoreCase(Constantes.USUARIO_ADMINISTRADOR)) {
					Parametro parametro = this.parametroDao.buscarPorCodigo(Constantes.EMAIL_DOCENTE_ACTUAL);
					mailParametros = this.configurarParametrosAdministrador(parametro.getValorTexto(), claveGenerada);
				} else {
					mailParametros = this.configurarParametrosNormales(found, claveGenerada);
				}
				this.mail.enviarCorreo(mailParametros);
			} else {
				throw new ScaExcepcion("USER", "No se encuentra un usuario registrado.");
			}
		} catch (MessagingException ex) {
			ex.printStackTrace();
			throw new ScaExcepcion("MAIL", "No se pudo enviar correctamente el mail.");
		} catch (EncodeExcepcion ex) {
			ex.printStackTrace();
			throw new ScaExcepcion("ENCODE", "No se pudo codificar la contraseña.");
		}
	}

	/**
	 * Configuración de los parametros para el envío de los correos.
	 * 
	 * @param persona
	 * @param usuario
	 * @param clave
	 * @return Parametros del mail.
	 */
	private MailParametros configurarParametrosNormales(Usuario usuario, String clave) {
		MailParametros mailParametros = new MailParametros();
		mailParametros.setAsunto(Constantes.ASUNTO_RECUPERAR_USUARIO);
		String contenido = Constantes.RECUPERAR_USUARIO_CABECERA.concat(Constantes.RECUPERAR_USUARIO_CUERPO);
		Map<String, String> parametros = new HashMap<>();
		parametros.put("nombres", usuario.getPersona().getNombre());
		parametros.put("apellidos", usuario.getPersona().getApellido());
		parametros.put("clave", clave);
		mailParametros.setContenido(this.mail.remplazarParametros(parametros, contenido));
		mailParametros.getCorreosTO().add(usuario.getPersona().getCorreoInstitucional());
		if (usuario.getPersona().getCorreoPersonal() != null) {
			mailParametros.getCorreosCC().add(usuario.getPersona().getCorreoPersonal());
		}
		return mailParametros;
	}

	/**
	 * Configuración de los parametros para el envío de los correos.
	 * 
	 * @param persona
	 * @param usuario
	 * @param clave
	 * @return Parametros del mail.
	 */
	private MailParametros configurarParametrosAdministrador(String email, String clave) {
		MailParametros mailParametros = new MailParametros();
		mailParametros.setAsunto(Constantes.ASUNTO_RECUPERAR_USUARIO);
		String contenido = Constantes.RECUPERAR_USUARIO_CUERPO;
		Map<String, String> parametros = new HashMap<>();
		parametros.put("clave", clave);
		mailParametros.setContenido(this.mail.remplazarParametros(parametros, contenido));
		mailParametros.getCorreosTO().add(email);
		return mailParametros;
	}

	public UsuarioDao getUsuarioDao() {
		return usuarioDao;
	}

}
