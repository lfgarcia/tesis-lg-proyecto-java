/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			LoginDto.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.transaction.Transactional;

import ec.edu.puce.ltic.sca.dao.PersonaDao;
import ec.edu.puce.ltic.sca.entidad.Persona;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.utils.Constantes;
import ec.edu.puce.ltic.sca.utils.Mail;
import ec.edu.puce.ltic.sca.utils.MailParametros;
import ec.edu.puce.ltic.sca.utils.UtilSca;

/**
 * Transacciones del login.
 * 
 * @author Luis García Castro
 */
@Stateless
@LocalBean
public class LoginDto implements Serializable {

	private static final long serialVersionUID = -2846269328612413418L;

	@EJB
	private PersonaDao personaDao;

	@EJB
	private Mail mail;

	/**
	 * Permite generar un usuario para accesos en el sistema.
	 * 
	 * @param persona
	 * @return Persona en base de datos.
	 * @throws ScaExcepcion
	 */
	@Transactional
	public String generarClave(String identificacionPuce) throws ScaExcepcion {
		try {
			String codigoGenerado = UtilSca.generarCodigoAleatorio(4);
			Persona found = this.personaDao.buscarPorIdentificacionPuce(identificacionPuce);
			this.mail.enviarCorreo(this.configurarParametros(found, codigoGenerado));
			return codigoGenerado;
		} catch (MessagingException ex) {
			ex.printStackTrace();
			throw new ScaExcepcion("MAIL", "No se pudo enviar correctamente el mail.");
		}
	}

	/**
	 * Configuración de los parametros para el envío de los correos.
	 * 
	 * @param persona
	 * @param usuario
	 * @param clave
	 * @return Parametros del mail.
	 */
	private MailParametros configurarParametros(Persona persona, String codigo) {
		MailParametros mailParametros = new MailParametros();
		mailParametros.setAsunto(Constantes.ASUNTO_GENERACION_CODIGO);
		String contenido = Constantes.GENERACION_CODIGO_CUERPO;
		Map<String, String> parametros = new HashMap<>();
		parametros.put("codigo", codigo);
		mailParametros.setContenido(this.mail.remplazarParametros(parametros, contenido));
		mailParametros.getCorreosTO().add(persona.getCorreoInstitucional());
		if (persona.getCorreoPersonal() != null) {
			mailParametros.getCorreosCC().add(persona.getCorreoPersonal());
		}
		return mailParametros;
	}

}
