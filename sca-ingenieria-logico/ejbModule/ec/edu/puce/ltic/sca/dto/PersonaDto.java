/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			PersonaDto.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.transaction.Transactional;

import ec.edu.puce.ltic.sca.dao.PersonaDao;
import ec.edu.puce.ltic.sca.dao.UsuarioDao;
import ec.edu.puce.ltic.sca.entidad.Catalogo;
import ec.edu.puce.ltic.sca.entidad.Perfil;
import ec.edu.puce.ltic.sca.entidad.Persona;
import ec.edu.puce.ltic.sca.entidad.Usuario;
import ec.edu.puce.ltic.sca.excepcion.EncodeExcepcion;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.utils.Constantes;
import ec.edu.puce.ltic.sca.utils.EncriptarSha;
import ec.edu.puce.ltic.sca.utils.Mail;
import ec.edu.puce.ltic.sca.utils.MailParametros;
import ec.edu.puce.ltic.sca.utils.UtilSca;

/**
 * Transacciones con la base de datos para las personas.
 * 
 * @author Luis García Castro
 */
@Stateless
@LocalBean
public class PersonaDto implements Serializable {

	private static final long serialVersionUID = 4936056032496326980L;

	@EJB
	private PersonaDao personaDao;

	@EJB
	private UsuarioDao usuarioDao;

	@EJB
	private Mail mail;

	/**
	 * Búsqueda de todas las personas en base de datos por el id del perfil.
	 * 
	 * @param idPerfil
	 * @return Lista de personas en base de datos.
	 * @throws ScaExcepcion
	 */
	public List<Persona> buscarTodos(Long idPerfil) throws ScaExcepcion {
		return this.personaDao.buscarTodos(idPerfil);
	}

	/**
	 * Permite realizar la creación o actualización en la base de datos.
	 * 
	 * @param isNuevo
	 * @param persona
	 * @param idTipoIdentificacion
	 * @param idGenero
	 * @param idPerfil
	 * @return Persona en base de datos.
	 * @throws ScaExcepcion
	 */
	@Transactional
	public Persona guardar(final Boolean isNuevo, Persona persona, final Long idTipoIdentificacion, final Long idGenero, final Long idPerfil) throws ScaExcepcion {
		Persona foundIdentificacionPuce = this.personaDao.buscarPorIdentificacionPuce(persona.getIdentificacionPuce());
		if (isNuevo && foundIdentificacionPuce != null) {
			throw new ScaExcepcion("UK-CODIGO", "La persona con identificación (PUCE): " + persona.getIdentificacionPuce() + " ya existe.");
		} else if (!isNuevo && foundIdentificacionPuce != null && foundIdentificacionPuce.getId().intValue() != persona.getId().intValue()) {
			throw new ScaExcepcion("UK-CODIGO", "La persona con identificación (PUCE): " + persona.getIdentificacionPuce() + " ya existe.");
		} else {
			Persona foundIdentificacion = this.personaDao.buscarPorIdentificacion(idTipoIdentificacion, persona.getNumeroIdentificacion());
			if (isNuevo && foundIdentificacion != null) {
				throw new ScaExcepcion("UK-CODIGO", "La persona con identificación: " + persona.getNumeroIdentificacion() + " y tipo de documento: " + foundIdentificacion.getTipoIdentificacion().getNombre() + " ya existe.");
			} else if (!isNuevo && foundIdentificacion != null && foundIdentificacion.getId().intValue() != persona.getId().intValue()) {
				throw new ScaExcepcion("UK-CODIGO", "La persona con identificación: " + persona.getNumeroIdentificacion() + " y tipo de documento: " + foundIdentificacion.getTipoIdentificacion().getNombre() + " ya existe.");
			}
		}
		persona.setTipoIdentificacion(new Catalogo(idTipoIdentificacion));
		persona.setPerfil(new Perfil(idPerfil));
		if (idGenero != null) {
			persona.setGenero(new Catalogo(idGenero));
		} else {
			persona.setGenero(null);
		}
		if (isNuevo) {
			this.personaDao.create(persona);
		} else {
			this.personaDao.update(persona);
		}
		return persona;
	}

	/**
	 * Permite activar o desactivar un registro en la base de datos.
	 * 
	 * @param persona
	 * @return Persona en base de datos.
	 * @throws ScaExcepcion
	 */
	@Transactional
	public Persona activarDesactivar(Persona persona) throws ScaExcepcion {
		persona.setEstado(!persona.getEstado());
		persona = this.personaDao.update(persona);
		return persona;
	}

	/**
	 * Permite generar un usuario para accesos en el sistema.
	 * 
	 * @param persona
	 * @return Persona en base de datos.
	 * @throws ScaExcepcion
	 */
	@Transactional
	public Persona generarUsuario(Persona persona) throws ScaExcepcion {
		try {
			persona.setTieneUsuario(Boolean.TRUE);
			persona = this.personaDao.update(persona);
			String claveGenerada = UtilSca.generarClaveAleatoria(8);
			Usuario found = this.usuarioDao.buscarPorPersonaId(persona.getId());
			if (found != null) {
				found.setClave(EncriptarSha.encriptar(claveGenerada));
				found = this.usuarioDao.update(found);
			} else {
				found = new Usuario();
				found.setUsuario(persona.getIdentificacionPuce());
				found.setClave(EncriptarSha.encriptar(claveGenerada));
				found.setPersona(persona);
				found = this.usuarioDao.create(found);

			}
			this.mail.enviarCorreo(this.configurarParametros(persona, found.getUsuario(), claveGenerada));
		} catch (MessagingException ex) {
			ex.printStackTrace();
			throw new ScaExcepcion("MAIL", "No se pudo enviar correctamente el mail.");
		} catch (EncodeExcepcion ex) {
			ex.printStackTrace();
			throw new ScaExcepcion("ENCODE", "No se pudo codificar la contraseña.");
		}
		return persona;
	}

	/**
	 * Configuración de los parametros para el envío de los correos.
	 * 
	 * @param persona
	 * @param usuario
	 * @param clave
	 * @return Parametros del mail.
	 */
	private MailParametros configurarParametros(Persona persona, String usuario, String clave) {
		MailParametros mailParametros = new MailParametros();
		mailParametros.setAsunto(Constantes.ASUNTO_GENERACION_USUARIO);
		String contenido = Constantes.GENERACION_USUARIO_CABECERA.concat(Constantes.GENERACION_USUARIO_CUERPO);
		Map<String, String> parametros = new HashMap<>();
		parametros.put("nombres", persona.getNombre());
		parametros.put("apellidos", persona.getApellido());
		parametros.put("nombreUsuario", usuario);
		parametros.put("clave", clave);
		mailParametros.setContenido(this.mail.remplazarParametros(parametros, contenido));
		mailParametros.getCorreosTO().add(persona.getCorreoInstitucional());
		if (persona.getCorreoPersonal() != null) {
			mailParametros.getCorreosCC().add(persona.getCorreoPersonal());
		}
		return mailParametros;
	}

}
