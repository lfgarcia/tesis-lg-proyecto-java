/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			AsociacionOpcionMenuDto.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dto;

import java.io.Serializable;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Transacciones con la base de datos para la asociación de las opciones de
 * menú.
 * 
 * @author Luis García Castro
 */
@Stateless
@LocalBean
public class AsociacionOpcionMenuDto implements Serializable {

	private static final long serialVersionUID = -3864007839416411332L;

}
