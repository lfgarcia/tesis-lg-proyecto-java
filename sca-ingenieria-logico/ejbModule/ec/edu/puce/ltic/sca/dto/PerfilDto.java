/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			PerfilDto.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import ec.edu.puce.ltic.sca.dao.PerfilDao;
import ec.edu.puce.ltic.sca.entidad.Perfil;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.utils.Constantes;

/**
 * Transacciones con la base de datos para los perfiles.
 * 
 * @author Luis García Castro
 */
@Stateless
@LocalBean
public class PerfilDto implements Serializable {

	private static final long serialVersionUID = -1628353071980319701L;

	@EJB
	private PerfilDao perfilDao;

	/**
	 * Permite buscar todos los perfiles por el tipo de rol.
	 * 
	 * @param perfil
	 * @return Lista de perfiles en base de datos.
	 * @throws ScaExcepcion
	 */
	public List<Perfil> buscarPorTipoRol(String perfil) throws ScaExcepcion {
		List<String> perfiles = new ArrayList<>();
		for (int i = 0; i < Constantes.PERFILES.length; i++) {
			if (!Constantes.PERFILES[i].equalsIgnoreCase(perfil)) {
				perfiles.add(Constantes.PERFILES[i]);
			}
		}
		return this.perfilDao.buscarPorTipoRol(perfiles);
	}

}
