/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			CatalogoDto.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dto;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

import ec.edu.puce.ltic.sca.dao.CatalogoDao;
import ec.edu.puce.ltic.sca.entidad.Catalogo;
import ec.edu.puce.ltic.sca.entidad.TipoCatalogo;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.utils.Constantes;

/**
 * Transacciones con la base de datos para los catálogos.
 * 
 * @author Luis García Castro
 */
@Stateless
@LocalBean
public class CatalogoDto implements Serializable {

	private static final long serialVersionUID = -464148659697322467L;

	@EJB
	private CatalogoDao catalogoDao;

	/**
	 * Búsqueda de todos los catálogos.
	 * 
	 * @return Lista de catálogos.
	 * @throws ScaExcepcion
	 */
	public List<Catalogo> buscarTodos() throws ScaExcepcion {
		return this.catalogoDao.buscarTodos(Constantes.fetchTipoCatalogo);
	}

	/**
	 * Búsqueda de todos los catálogos por el código del tipo de catálogo.
	 * 
	 * @param codigo
	 * @return Lista de catálogos.
	 * @throws ScaExcepcion
	 */
	public List<Catalogo> buscarTodosPorTipoCatalogoCodigo(final String codigo) throws ScaExcepcion {
		return this.catalogoDao.buscarTodosPorTipoCatalogoCodigo(codigo);
	}

	/**
	 * Permite realizar la creación o actualización en la base de datos.
	 * 
	 * @param isNuevo
	 * @param catalogo
	 * @param idTipoCatalogo
	 * @return Catálogo en base de datos.
	 * @throws ScaExcepcion
	 */
	@Transactional
	public Catalogo guardar(final Boolean isNuevo, Catalogo catalogo, final Long idTipoCatalogo) throws ScaExcepcion {
		Catalogo found = this.catalogoDao.buscarPorCodigo(catalogo.getCodigo());
		if (isNuevo && found != null) {
			throw new ScaExcepcion("UK-CODIGO", "El catálogo con código: " + catalogo.getCodigo() + " ya existe.");
		} else if (!isNuevo && found != null && found.getId().intValue() != catalogo.getId().intValue()) {
			throw new ScaExcepcion("UK-CODIGO", "El catálogo con código: " + catalogo.getCodigo() + " ya existe.");
		}
		catalogo.setTipoCatalogo(new TipoCatalogo(idTipoCatalogo));
		if (isNuevo) {
			this.catalogoDao.create(catalogo);
		} else {
			this.catalogoDao.update(catalogo);
		}
		return catalogo;
	}

	/**
	 * Permite activar o desactivar un registro en la base de datos.
	 * 
	 * @param catalogo
	 * @return Catálogo en base de datos.
	 * @throws ScaExcepcion
	 */
	@Transactional
	public Catalogo activarDesactivar(Catalogo catalogo) throws ScaExcepcion {
		catalogo.setEstado(!catalogo.getEstado());
		catalogo = this.catalogoDao.update(catalogo);
		return catalogo;
	}

}
