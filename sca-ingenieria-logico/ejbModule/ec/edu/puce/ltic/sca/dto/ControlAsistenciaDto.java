/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ControlAsistenciaDto.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dto;

import java.io.Serializable;
import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

import ec.edu.puce.ltic.sca.dao.ControlAsistenciaDao;
import ec.edu.puce.ltic.sca.dao.ParametroDao;
import ec.edu.puce.ltic.sca.dao.PersonaDao;
import ec.edu.puce.ltic.sca.entidad.ControlAsistencia;
import ec.edu.puce.ltic.sca.entidad.Persona;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;

/**
 * Transacciones con la base de datos para el control de asistencia.
 * 
 * @author Luis García Castro
 */
@Stateless
@LocalBean
public class ControlAsistenciaDto implements Serializable {

	private static final long serialVersionUID = -2186761582423585874L;

	@EJB
	private ControlAsistenciaDao controlAsistenciaDao;

	@EJB
	private PersonaDao personaDao;

	@EJB
	private ParametroDao parametroDao;

	/**
	 * Permite realizar el registro o salida de la asistencia por persona.
	 * 
	 * @param identificacionPuce
	 * @return Control de asistencia.
	 * @throws ScaExcepcion
	 */
	@Transactional
	public ControlAsistencia registrarAsistencia(final String identificacionPuce) throws ScaExcepcion {
		Persona persona = this.personaDao.buscarPorIdentificacionPuce(identificacionPuce);
		if (persona == null) {
			throw new ScaExcepcion("EBD-01", "No se encontró la persona con identificación: " + identificacionPuce);
		}
		ControlAsistencia ca = this.controlAsistenciaDao.buscarPorPersona(identificacionPuce);
		if (ca == null) {
			ca = new ControlAsistencia();
			ca.setHoraEntrada(new Date());
			ca.setPersona(persona);
			ca = this.controlAsistenciaDao.create(ca);
		} else {
			ca.setHoraSalida(new Date());
			ca = this.controlAsistenciaDao.update(ca);
		}
		return ca;
	}

	public PersonaDao getPersonaDao() {
		return personaDao;
	}

}
