Contiene los proyectos en lenguaje JAVA para el desarrollo de la disertación.

Requerimientos:
* Java 8
* PostgreSQL 9.6 o +
* Wildfly 10 o +

Configuración desde 0:
* Instalación y configuración de base de datos en PostgreSQL (Revisar repositorio). Si se instala en servidor separado, configuración de conexión remoto
* Configuración del datasource de base de datos: java:/IngenieriaDS
* Configuración del datasource de SMTP: java:jboss/mail/ScaIngenieria
