/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			Parametro.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.entidad;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.edu.puce.ltic.sca.generico.EntidadBasica;

/**
 * Mapeo de clase a la base de datos.
 * 
 * @author Luis García Castro
 */
@Entity
@Table(	name = "parametro",
		schema = "sca")
@NamedQueries({ @NamedQuery(name = Parametro.buscarTodosPorTipoParametroCodigo,
							query = "SELECT p FROM Parametro p LEFT JOIN FETCH p.tipoParametro tp WHERE tp.codigo = :codigo"),
		@NamedQuery(name = Parametro.buscarPorCodigo,
					query = "SELECT p FROM Parametro p WHERE p.codigo = :codigo"),
		@NamedQuery(name = Parametro.buscarTodosPorNombre,
					query = "SELECT p FROM Parametro p WHERE UPPER(p.nombre) LIKE UPPER(:nombre)"),
		@NamedQuery(name = Parametro.buscarTodosPorDescripcion,
					query = "SELECT p FROM Parametro p WHERE UPPER(p.descripcion) LIKE UPPER(:descripcion)") })
public class Parametro extends EntidadBasica implements Serializable {

	private static final long serialVersionUID = 2405833979224586687L;

	public static final String buscarTodosPorTipoParametroCodigo = "parametro.buscarPorTipoParametroCodigo";

	public static final String buscarPorCodigo = "parametro.buscarPorCodigo";

	public static final String buscarTodosPorNombre = "parametro.buscarTodosPorNombre";

	public static final String buscarTodosPorDescripcion = "parametro.buscarTodosPorDescripcion";

	@Column(name = "codigo",
			nullable = false)
	private String codigo;

	@Column(name = "nombre",
			nullable = false)
	private String nombre;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "valor_fecha")
	@Temporal(TemporalType.DATE)
	private Date valorFecha;

	@Column(name = "valor_hora")
	@Temporal(TemporalType.TIME)
	private Date valorHora;

	@Column(name = "valor_entero")
	private Long valorEntero;

	@Column(name = "valor_decimal")
	private Double valorDecimal;

	@Column(name = "valor_texto")
	private String valorTexto;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_tipo_parametro",
				nullable = false)
	private Catalogo tipoParametro;

	public Parametro() {
		super();
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getValorFecha() {
		return valorFecha;
	}

	public void setValorFecha(Date valorFecha) {
		this.valorFecha = valorFecha;
	}

	public Date getValorHora() {
		return valorHora;
	}

	public void setValorHora(Date valorHora) {
		this.valorHora = valorHora;
	}

	public Long getValorEntero() {
		return valorEntero;
	}

	public void setValorEntero(Long valorEntero) {
		this.valorEntero = valorEntero;
	}

	public Double getValorDecimal() {
		return valorDecimal;
	}

	public void setValorDecimal(Double valorDecimal) {
		this.valorDecimal = valorDecimal;
	}

	public String getValorTexto() {
		return valorTexto;
	}

	public void setValorTexto(String valorTexto) {
		this.valorTexto = valorTexto;
	}

	public Catalogo getTipoParametro() {
		return tipoParametro;
	}

	public void setTipoParametro(Catalogo tipoParametro) {
		this.tipoParametro = tipoParametro;
	}

}
