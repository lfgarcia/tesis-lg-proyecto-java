/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			OpcionMenu.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.entidad;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import ec.edu.puce.ltic.sca.generico.EntidadBasica;

/**
 * Mapeo de clase a la base de datos.
 * 
 * @author Luis García Castro
 */
@Entity
@Table(	name = "opcion_menu",
		schema = "sca")
@NamedQueries({ @NamedQuery(name = OpcionMenu.buscarTodosPadres,
							query = "SELECT om FROM OpcionMenu om WHERE om.padre IS NULL"),
		@NamedQuery(name = OpcionMenu.buscarTodosPorPadre,
					query = "SELECT om FROM OpcionMenu om WHERE om.padre = :idPadre") })
public class OpcionMenu extends EntidadBasica implements Serializable {

	private static final long serialVersionUID = -6650848092431577914L;

	public static final String buscarTodosPadres = "OpcionMenu.buscarTodosPadres";

	public static final String buscarTodosPorPadre = "OpcionMenu.buscarTodosPorPadre";

	@Column(name = "nombre_pagina",
			nullable = false)
	private String nombrePagina;

	@Column(name = "icono",
			nullable = false)
	private String icono;

	@Column(name = "url_pagina",
			nullable = false)
	private String url_pagina;

	@Column(name = "nivel",
			nullable = false)
	private Long nivel;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_padre")
	private OpcionMenu padre;

	@Transient
	private List<OpcionMenu> opcionesMenu;

	public OpcionMenu() {
		super();
	}

	public String getNombrePagina() {
		return nombrePagina;
	}

	public void setNombrePagina(String nombrePagina) {
		this.nombrePagina = nombrePagina;
	}

	public String getIcono() {
		return icono;
	}

	public void setIcono(String icono) {
		this.icono = icono;
	}

	public String getUrl_pagina() {
		return url_pagina;
	}

	public void setUrl_pagina(String url_pagina) {
		this.url_pagina = url_pagina;
	}

	public Long getNivel() {
		return nivel;
	}

	public void setNivel(Long nivel) {
		this.nivel = nivel;
	}

	public OpcionMenu getPadre() {
		return padre;
	}

	public void setPadre(OpcionMenu padre) {
		this.padre = padre;
	}

	public List<OpcionMenu> getOpcionesMenu() {
		return opcionesMenu;
	}

	public void setOpcionesMenu(List<OpcionMenu> opcionesMenu) {
		this.opcionesMenu = opcionesMenu;
	}

}
