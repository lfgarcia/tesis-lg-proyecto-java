/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			HorarioPersona.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.entidad;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Mapeo de clase a la base de datos.
 * 
 * @author Luis García Castro
 */
@Entity
@Table(	name = "horario_persona",
		schema = "sca")
@NamedQueries({ @NamedQuery(name = HorarioPersona.buscarPorHorarioPersona,
							query = "SELECT hp FROM HorarioPersona hp LEFT JOIN hp.horario h LEFT JOIN h.dia d LEFT JOIN hp.persona p WHERE d.codigo = :codigoDia AND h.horaEntrada = CAST(:horaEntrada AS time) AND h.horaSalida = CAST(:horaSalida AS time) AND p.id = :idPersona"),
		@NamedQuery(name = HorarioPersona.buscarTodos,
					query = "SELECT hp FROM HorarioPersona hp") })
public class HorarioPersona implements Serializable {

	private static final long serialVersionUID = -4022781284688235767L;

	public static final String buscarPorHorarioPersona = "HorarioPersona.buscarPorHorarioPersona";

	public static final String buscarTodos = "HorarioPersona.buscarTodos";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id",
			nullable = false)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_horario")
	private Horario horario;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_persona")
	private Persona persona;

	public HorarioPersona() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Horario getHorario() {
		return horario;
	}

	public void setHorario(Horario horario) {
		this.horario = horario;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

}
