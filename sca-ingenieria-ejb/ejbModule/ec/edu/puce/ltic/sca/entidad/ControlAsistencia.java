/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ControlAsistencia.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.entidad;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.edu.puce.ltic.sca.generico.EntidadBasica;

/**
 * Mapeo de clase a la base de datos.
 * 
 * @author Luis García Castro
 */
@Entity
@Table(	name = "control_asistencia",
		schema = "sca")
@NamedQueries({ @NamedQuery(name = ControlAsistencia.buscarPorPersona,
							query = "SELECT ca FROM ControlAsistencia ca WHERE DATE(ca.fechaCreacion) = DATE(NOW()) AND ca.persona.identificacionPuce = :identificacionPuce AND ca.horaSalida = NULL") })
public class ControlAsistencia extends EntidadBasica implements Serializable {

	private static final long serialVersionUID = 8444509170347739934L;

	public static final String buscarPorPersona = "ControlAsistencia.buscarPorPersona";

	@Column(name = "hora_entrada",
			nullable = false)
	@Temporal(TemporalType.TIME)
	private Date horaEntrada;

	@Column(name = "hora_salida",
			nullable = false)
	@Temporal(TemporalType.TIME)
	private Date horaSalida;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_persona",
				nullable = false)
	private Persona persona;

	public ControlAsistencia() {
		super();
	}

	public Date getHoraEntrada() {
		return horaEntrada;
	}

	public void setHoraEntrada(Date horaEntrada) {
		this.horaEntrada = horaEntrada;
	}

	public Date getHoraSalida() {
		return horaSalida;
	}

	public void setHoraSalida(Date horaSalida) {
		this.horaSalida = horaSalida;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

}
