/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			HorarioReasignacion.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.entidad;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Mapeo de clase a la base de datos.
 * 
 * @author Luis García Castro
 */
@Entity
@Table(	name = "horario_reasignacion",
		schema = "sca")
public class HorarioReasignacion implements Serializable {

	private static final long serialVersionUID = -8160588218666313986L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id",
			nullable = false)
	private Long id;

	@Column(name = "dia_reasignacion",
			nullable = false)
	@Temporal(TemporalType.DATE)
	private Date diaReasignacion;

	@Column(name = "hora_entrada_reasignacion",
			nullable = false)
	@Temporal(TemporalType.TIME)
	private Date horaEntradaReasignacion;

	@Column(name = "hora_salida_reasignacion",
			nullable = false)
	@Temporal(TemporalType.TIME)
	private Date horaSalidaReasignacion;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_persona")
	private Persona persona;

	public HorarioReasignacion() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDiaReasignacion() {
		return diaReasignacion;
	}

	public void setDiaReasignacion(Date diaReasignacion) {
		this.diaReasignacion = diaReasignacion;
	}

	public Date getHoraEntradaReasignacion() {
		return horaEntradaReasignacion;
	}

	public void setHoraEntradaReasignacion(Date horaEntradaReasignacion) {
		this.horaEntradaReasignacion = horaEntradaReasignacion;
	}

	public Date getHoraSalidaReasignacion() {
		return horaSalidaReasignacion;
	}

	public void setHoraSalidaReasignacion(Date horaSalidaReasignacion) {
		this.horaSalidaReasignacion = horaSalidaReasignacion;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

}
