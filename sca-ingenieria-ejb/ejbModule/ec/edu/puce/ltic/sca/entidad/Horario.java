/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			Horario.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.edu.puce.ltic.sca.generico.EntidadBasica;

/**
 * Mapeo de clase a la base de datos.
 * 
 * @author Luis García Castro
 */
@Entity
@Table(	name = "horario",
		schema = "sca")
@NamedQueries({ @NamedQuery(name = Horario.buscarTodos,
							query = "SELECT h FROM Horario h ORDER BY h.horaEntrada ASC, h.orden ASC"),
		@NamedQuery(name = Horario.buscarPorHoraEntradaCodigoDia,
					query = "SELECT h FROM Horario h LEFT JOIN h.personas p WHERE h.horaEntrada = CAST(:horaEntrada AS time) AND h.dia.codigo = :codigoDia") })
public class Horario extends EntidadBasica implements Serializable {

	private static final long serialVersionUID = -1637818791577563222L;

	public static final String buscarTodos = "Horario.buscarTodos";

	public static final String buscarPorHoraEntradaCodigoDia = "Horario.buscarPorHoraEntrada";

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_dia",
				nullable = false)
	private Catalogo dia;

	@Column(name = "hora_entrada",
			nullable = false)
	@Temporal(TemporalType.TIME)
	private Date horaEntrada;

	@Column(name = "hora_salida",
			nullable = false)
	@Temporal(TemporalType.TIME)
	private Date horaSalida;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "orden",
			nullable = false)
	private Long orden;

	@OneToMany(	mappedBy = "horario",
				fetch = FetchType.EAGER,
				cascade = CascadeType.ALL)
	private List<HorarioPersona> personas;

	public Horario() {
		super();
	}

	public Catalogo getDia() {
		return dia;
	}

	public void setDia(Catalogo dia) {
		this.dia = dia;
	}

	public Date getHoraEntrada() {
		return horaEntrada;
	}

	public void setHoraEntrada(Date horaEntrada) {
		this.horaEntrada = horaEntrada;
	}

	public Date getHoraSalida() {
		return horaSalida;
	}

	public void setHoraSalida(Date horaSalida) {
		this.horaSalida = horaSalida;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Long getOrden() {
		return orden;
	}

	public void setOrden(Long orden) {
		this.orden = orden;
	}

	public List<HorarioPersona> getPersonas() {
		return personas;
	}

	public void setPersonas(List<HorarioPersona> personas) {
		this.personas = personas;
	}

}
