/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			Catalogo.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import ec.edu.puce.ltic.sca.generico.EntidadBasica;

/**
 * Mapeo de clase a la base de datos.
 * 
 * @author Luis García Castro
 */
@Entity
@Table(	name = "catalogo",
		schema = "sca")
@NamedQueries({ @NamedQuery(name = Catalogo.buscarTodosPorTipoCatalogoCodigo,
							query = "SELECT c FROM Catalogo c LEFT JOIN FETCH c.tipoCatalogo tc WHERE tc.codigo = :codigo ORDER BY c.id ASC"),
		@NamedQuery(name = Catalogo.buscarPorCodigo,
					query = "SELECT c FROM Catalogo c WHERE c.codigo = :codigo"),
		@NamedQuery(name = Catalogo.buscarTodosPorNombre,
					query = "SELECT c FROM Catalogo c WHERE UPPER(c.nombre) LIKE UPPER(:nombre) ORDER BY c.id ASC"),
		@NamedQuery(name = Catalogo.buscarTodosPorDescripcion,
					query = "SELECT c FROM Catalogo c WHERE UPPER(c.descripcion) LIKE UPPER(:descripcion) ORDER BY c.id ASC") })
public class Catalogo extends EntidadBasica implements Serializable {

	private static final long serialVersionUID = 7038948851205255914L;

	public static final String buscarTodosPorTipoCatalogoCodigo = "catalogo.buscarPorTipoCatalogoCodigo";

	public static final String buscarPorCodigo = "catalogo.buscarPorCodigo";

	public static final String buscarTodosPorNombre = "catalogo.buscarTodosPorNombre";

	public static final String buscarTodosPorDescripcion = "catalogo.buscarTodosPorDescripcion";

	@Column(name = "codigo",
			nullable = false)
	private String codigo;

	@Column(name = "nombre",
			nullable = false)
	private String nombre;

	@Column(name = "descripcion")
	private String descripcion;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_tipo_catalogo",
				nullable = false)
	private TipoCatalogo tipoCatalogo;

	public Catalogo() {
		super();
	}

	public Catalogo(final Long id) {
		super(id);
	}

	public Catalogo(final String codigo) {
		super();
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public TipoCatalogo getTipoCatalogo() {
		return tipoCatalogo;
	}

	public void setTipoCatalogo(TipoCatalogo tipoCatalogo) {
		this.tipoCatalogo = tipoCatalogo;
	}

}
