/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			Persona.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.entidad;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import ec.edu.puce.ltic.sca.generico.EntidadBasica;

/**
 * Mapeo de clase a la base de datos.
 * 
 * @author Luis García Castro
 */
@Entity
@Table(	name = "persona",
		schema = "sca")
@NamedQueries({ @NamedQuery(name = Persona.buscarTodos,
							query = "SELECT p FROM Persona p LEFT JOIN FETCH p.tipoIdentificacion ti LEFT JOIN FETCH p.genero g WHERE p.id != 0 AND p.perfil.id > :idPerfil ORDER BY p.numeroIdentificacion ASC"),
		@NamedQuery(name = Persona.buscarPorTipoIdentificacionAndNumeroIdentificacion,
					query = "SELECT p FROM Persona p LEFT JOIN FETCH p.tipoIdentificacion ti LEFT JOIN FETCH p.genero g WHERE ti.id = :idTipoIdentificacion AND p.numeroIdentificacion = :numeroIdentificacion"),
		@NamedQuery(name = Persona.buscarPorIdentificacionPuce,
					query = "SELECT p FROM Persona p LEFT JOIN FETCH p.tipoIdentificacion ti LEFT JOIN FETCH p.genero g WHERE p.identificacionPuce = :identificacionPuce") })
public class Persona extends EntidadBasica implements Serializable {

	private static final long serialVersionUID = 5358000623732130455L;

	public static final String buscarTodos = "Persona.buscarTodos";

	public static final String buscarPorTipoIdentificacionAndNumeroIdentificacion = "Persona.buscarPorTipoIdentificacionAndNumeroIdentificacion";

	public static final String buscarPorIdentificacionPuce = "Persona.buscarPorIdentificacionPuce";

	@Column(name = "numero_identificacion",
			nullable = false)
	private String numeroIdentificacion;

	@Column(name = "nombre",
			nullable = false)
	private String nombre;

	@Column(name = "apellido",
			nullable = false)
	private String apellido;

	@Column(name = "fecha_nacimiento")
	@Temporal(TemporalType.DATE)
	private Date fechaNacimiento;

	@Column(name = "identificacion_puce",
			nullable = false)
	private String identificacionPuce;

	@Column(name = "correo_institucional",
			nullable = false)
	private String correoInstitucional;

	@Column(name = "correo_personal")
	private String correoPersonal;

	@Column(name = "tiene_usuario")
	private Boolean tieneUsuario = Boolean.FALSE;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_tipo_identificacion",
				nullable = false)
	private Catalogo tipoIdentificacion;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_genero")
	private Catalogo genero;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_perfil",
				nullable = false)
	private Perfil perfil;

	@Transient
	private String nombreCompleto;

	public Persona() {
		super();
	}

	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}

	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getIdentificacionPuce() {
		return identificacionPuce;
	}

	public void setIdentificacionPuce(String identificacionPuce) {
		this.identificacionPuce = identificacionPuce;
	}

	public String getCorreoInstitucional() {
		return correoInstitucional;
	}

	public void setCorreoInstitucional(String correoInstitucional) {
		this.correoInstitucional = correoInstitucional;
	}

	public String getCorreoPersonal() {
		return correoPersonal;
	}

	public void setCorreoPersonal(String correoPersonal) {
		this.correoPersonal = correoPersonal;
	}

	public Boolean getTieneUsuario() {
		return tieneUsuario;
	}

	public void setTieneUsuario(Boolean tieneUsuario) {
		this.tieneUsuario = tieneUsuario;
	}

	public Catalogo getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(Catalogo tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public Catalogo getGenero() {
		return genero;
	}

	public void setGenero(Catalogo genero) {
		this.genero = genero;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public String getNombreCompleto() {
		this.nombreCompleto = this.nombre + " " + this.apellido;
		return this.nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

}
