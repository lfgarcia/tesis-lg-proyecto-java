/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			TipoCatalogo.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import ec.edu.puce.ltic.sca.generico.EntidadBasica;

/**
 * Mapeo de clase a la base de datos.
 * 
 * @author Luis García Castro
 */
@Entity
@Table(	name = "tipo_catalogo",
		schema = "sca")
@NamedQueries({ @NamedQuery(name = TipoCatalogo.buscarPorCodigo,
							query = "SELECT tc FROM TipoCatalogo tc WHERE tc.codigo = :codigo"),
		@NamedQuery(name = TipoCatalogo.buscarTodosPorNombre,
					query = "SELECT tc FROM TipoCatalogo tc WHERE UPPER(tc.nombre) LIKE '%' || UPPER(:nombre) || '%' ORDER BY tc.nombre"),
		@NamedQuery(name = TipoCatalogo.buscarTodosPorDescripcion,
					query = "SELECT tc FROM TipoCatalogo tc WHERE UPPER(tc.descripcion) LIKE '%' || UPPER(:descripcion) || '%' ORDER BY tc.nombre") })
public class TipoCatalogo extends EntidadBasica implements Serializable {

	private static final long serialVersionUID = 3027727798142512550L;

	public static final String buscarPorCodigo = "tipoCatalogo.buscarPorCodigo";

	public static final String buscarTodosPorNombre = "tipoCatalogo.buscarTodosPorNombre";

	public static final String buscarTodosPorDescripcion = "tipoCatalogo.buscarTodosPorDescripcion";

	@Column(name = "codigo",
			nullable = false)
	private String codigo;

	@Column(name = "nombre",
			nullable = false)
	private String nombre;

	@Column(name = "descripcion")
	private String descripcion;

	public TipoCatalogo() {
		super();
	}

	public TipoCatalogo(final Long id) {
		super(id);
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
