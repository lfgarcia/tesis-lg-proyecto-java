/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			AsociacionOpcionMenu.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.entidad;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import ec.edu.puce.ltic.sca.generico.EntidadBasica;

/**
 * Mapeo de clase a la base de datos.
 * 
 * @author Luis García Castro
 */
@Entity
@Table(	name = "asociacion_opcion_menu",
		schema = "sca")
@NamedQueries({ @NamedQuery(name = AsociacionOpcionMenu.buscarPorPerfilId,
							query = "SELECT aom FROM AsociacionOpcionMenu aom LEFT JOIN aom.perfil p LEFT JOIN aom.opcionMenu om WHERE p.id = :idPerfil ORDER BY om.nivel ASC, aom.id ASC") })
public class AsociacionOpcionMenu extends EntidadBasica implements Serializable {

	private static final long serialVersionUID = -1520322820544406667L;

	public static final String buscarPorPerfilId = "AsociacionOpcionMenu.buscarPorPerfilId";

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_perfil")
	private Perfil perfil;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_opcion_menu")
	private OpcionMenu opcionMenu;

	public AsociacionOpcionMenu() {
		super();
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public OpcionMenu getOpcionMenu() {
		return opcionMenu;
	}

	public void setOpcionMenu(OpcionMenu opcionMenu) {
		this.opcionMenu = opcionMenu;
	}

}
