/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			Usuario.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.entidad;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import ec.edu.puce.ltic.sca.generico.EntidadBasica;

/**
 * Mapeo de clase a la base de datos.
 * 
 * @author Luis García Castro
 */
@Entity
@Table(	name = "usuario",
		schema = "sca")
@NamedQueries({ @NamedQuery(name = Usuario.buscarTodos,
							query = "SELECT us FROM Usuario us WHERE us.id != 0"),
		@NamedQuery(name = Usuario.doLogin,
					query = "SELECT us FROM Usuario us LEFT JOIN us.persona.genero WHERE usuario = :usuario AND clave = :clave"),
		@NamedQuery(name = Usuario.buscarPorPersonaId,
					query = "SELECT us FROM Usuario us LEFT JOIN us.persona pe WHERE pe.id = :idPersona"),
		@NamedQuery(name = Usuario.comprobarUsuario,
					query = "SELECT us FROM Usuario us WHERE us.usuario = :usuario") })
public class Usuario extends EntidadBasica implements Serializable {

	private static final long serialVersionUID = -6003204016327259363L;

	public static final String buscarTodos = "Usuario.buscarTodos";

	public static final String doLogin = "Usuario.doLogin";

	public static final String buscarPorPersonaId = "Usuario.buscarPorPersona";

	public static final String comprobarUsuario = "Usuario.comprobarUsuario";

	@Column(name = "usuario",
			nullable = false)
	private String usuario;

	@Column(name = "clave",
			nullable = false)
	private String clave;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_persona",
				nullable = false)
	private Persona persona;

	@Transient
	private List<OpcionMenu> menu = new ArrayList<>();

	public Usuario() {
		super();
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public List<OpcionMenu> getMenu() {
		return menu;
	}

	public void setMenu(List<OpcionMenu> menu) {
		this.menu = menu;
	}

}
