/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			Permiso.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.entidad;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Mapeo de clase a la base de datos.
 * 
 * @author Luis García Castro
 */
@Entity
@Table(	name = "permiso",
		schema = "sca")
@NamedQueries({})
public class Permiso implements Serializable {

	private static final long serialVersionUID = 4951487340156430826L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id",
			nullable = false)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_dia",
				nullable = false)
	private Catalogo dia;

	@Column(name = "hora_entrada",
			nullable = false)
	@Temporal(TemporalType.TIME)
	private Date horaEntrada;

	@Column(name = "hora_salida",
			nullable = false)
	@Temporal(TemporalType.TIME)
	private Date horaSalida;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_persona")
	private Persona persona;

	@Column(name = "es_justificada",
			nullable = false,
			columnDefinition = "BOOLEAN DEFAULT FALSE")
	private Boolean esJustificada;

	public Permiso() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Catalogo getDia() {
		return dia;
	}

	public void setDia(Catalogo dia) {
		this.dia = dia;
	}

	public Date getHoraEntrada() {
		return horaEntrada;
	}

	public void setHoraEntrada(Date horaEntrada) {
		this.horaEntrada = horaEntrada;
	}

	public Date getHoraSalida() {
		return horaSalida;
	}

	public void setHoraSalida(Date horaSalida) {
		this.horaSalida = horaSalida;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Boolean getEsJustificada() {
		return esJustificada;
	}

	public void setEsJustificada(Boolean esJustificada) {
		this.esJustificada = esJustificada;
	}

}
