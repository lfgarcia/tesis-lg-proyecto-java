/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			Perfil.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.entidad;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import ec.edu.puce.ltic.sca.generico.EntidadBasica;

/**
 * Mapeo de clase a la base de datos.
 * 
 * @author Luis García Castro
 */
@Entity
@Table(	name = "perfil",
		schema = "sca")
@NamedQueries({ @NamedQuery(name = Perfil.buscarPorTipoRol,
							query = "SELECT p FROM Perfil p WHERE p.tipoRol.codigo IN (:perfiles)") })
public class Perfil extends EntidadBasica implements Serializable {

	private static final long serialVersionUID = -4545898206271116555L;

	public static final String buscarPorTipoRol = "Perfil.buscarPorTipoRol";

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_tipo_rol",
				nullable = false)
	private Catalogo tipoRol;

	public Perfil() {
		super();
	}

	public Perfil(final Long id) {
		super();
		this.setId(id);
	}

	public Catalogo getTipoRol() {
		return tipoRol;
	}

	public void setTipoRol(Catalogo tipoRol) {
		this.tipoRol = tipoRol;
	}

}
