/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ReporteBasica.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.generico;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Mapeo genérico a la base de datos para los reportes.
 * 
 * @author Luis García Castro
 */
@MappedSuperclass
public abstract class ReporteBasica implements Serializable {

	private static final long serialVersionUID = 5645644663831234912L;

	@Column(name = "tipo_identificacion")
	private String tipoIdentificacion;

	@Column(name = "numero_identificacion")
	private String numeroIdentificacion;

	@Column(name = "nombre_completo")
	private String nombreCompleto;

	@Id
	@Column(name = "identificacion_puce")
	private String identificacionPuce;

	@Column(name = "periodo_actual")
	private String periodoActual;

	@Column(name = "periodo_actual_nombre")
	private String periodoActualNombre;

	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}

	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getIdentificacionPuce() {
		return identificacionPuce;
	}

	public void setIdentificacionPuce(String identificacionPuce) {
		this.identificacionPuce = identificacionPuce;
	}

	public String getPeriodoActual() {
		return periodoActual;
	}

	public void setPeriodoActual(String periodoActual) {
		this.periodoActual = periodoActual;
	}

	public String getPeriodoActualNombre() {
		return periodoActualNombre;
	}

	public void setPeriodoActualNombre(String periodoActualNombre) {
		this.periodoActualNombre = periodoActualNombre;
	}

}
