/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			DaoGenerico.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.generico;

import java.io.Serializable;
import java.util.List;

import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;

/**
 * Consultas genéricas para el sistema.
 * 
 * @author Luis García Castro
 */
public abstract interface DaoGenerico<Entidad, ID extends Serializable> extends Serializable {

	/**
	 * Método que permite crear un registro en la base de datos.
	 * 
	 * @param entidad
	 *            Información de la entidad para persistir.
	 * @return
	 * @throws ScaExcepcion
	 */
	public Entidad create(Entidad entidad) throws ScaExcepcion;

	/**
	 * Método que permite actualizar un registro en la base de datos.
	 * 
	 * @param entidad
	 *            Información de la entidad para persistir.
	 * @return
	 * @throws ScaExcepcion
	 */
	public Entidad update(Entidad entidad) throws ScaExcepcion;

	/**
	 * Método que permite eliminar un registro en la base de datos.
	 * 
	 * @param entidad
	 *            Información de la entidad para persistir.
	 * @return
	 * @throws ScaExcepcion
	 */
	public void delete(Entidad entidad) throws ScaExcepcion;

	/**
	 * Método que permite realizar una búsqueda de todos los registros.
	 * 
	 * @param enlace
	 *            Corresponde al JOIN FETCH de las tablas (JOIN FETCH aaaa JOIN
	 *            FETCH bbbb).
	 * @return
	 * @throws ScaExcepcion
	 */
	public List<Entidad> buscarTodos(final String[]... enlace) throws ScaExcepcion;

	/**
	 * Método que permite realizar una búsqueda de todos los registros en
	 * consulta paginada.
	 * 
	 * @param enlace
	 *            Corresponde al JOIN FETCH de las tablas (JOIN FETCH aaaa JOIN
	 *            FETCH bbbb).
	 * @param inicio
	 *            Inicio de los registros.
	 * @param cantidad
	 *            Cantidad de registros que mostraría.
	 * @return
	 * @throws ScaExcepcion
	 */
	public List<Entidad> buscarTodosPaginado(final Integer inicio, final Integer cantidad, final String[]... enlace) throws ScaExcepcion;

	/**
	 * Método que permite realizar una búsqueda de un registro según su id.
	 * 
	 * @param id
	 *            ID del registro.
	 * @param enlace
	 *            Corresponde al JOIN FETCH de las tablas (JOIN FETCH aaaa JOIN
	 *            FETCH bbbb).
	 * @return
	 * @throws ScaExcepcion
	 */
	public Entidad buscarPorId(final Long id, final String[]... enlace) throws ScaExcepcion;

	/**
	 * Método que permite realizar una búsqueda de todos los registros activos.
	 * 
	 * @param enlace
	 *            Corresponde al JOIN FETCH de las tablas (JOIN FETCH aaaa JOIN
	 *            FETCH bbbb).
	 * @return
	 * @throws ScaExcepcion
	 */
	public List<Entidad> buscarTodosPorEstadoActivo(final String[]... enlace) throws ScaExcepcion;

	/**
	 * Método que permite realizar una búsqueda de todos los registros activos
	 * en consulta paginada.
	 * 
	 * @param enlace
	 *            Corresponde al JOIN FETCH de las tablas (JOIN FETCH aaaa JOIN
	 *            FETCH bbbb).
	 * @param inicio
	 *            Inicio de los registros.
	 * @param cantidad
	 *            Cantidad de registros que mostraría.
	 * @return
	 * @throws ScaExcepcion
	 */
	public List<Entidad> buscarTodosPorEstadoActivoPaginado(final Integer inicio, final Integer cantidad, final String[]... enlace) throws ScaExcepcion;

}
