/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			DaoGenericoImpl.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.generico;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;

/**
 * Implementación de las consultas genéricas.
 * 
 * @author Luis García Castro
 */
public abstract class DaoGenericoImpl<Entidad, ID extends Serializable> implements DaoGenerico<Entidad, ID> {

	private static final long serialVersionUID = 9076807614477198302L;

	private Class<Entidad> claseEntidad;

	@PersistenceContext(unitName = "IngenieriaDS",
						type = PersistenceContextType.TRANSACTION)
	private EntityManager em;

	public DaoGenericoImpl(Class<Entidad> clase) {
		this.claseEntidad = clase;
	}

	public final String construirSelect(final String[]... enlace) {
		StringBuilder select = new StringBuilder("SELECT obj FROM ");
		select.append(claseEntidad.getSimpleName());
		select.append(" obj ");
		if (enlace != null) {
			for (int i = 0; i < enlace.length; i++) {
				select.append("LEFT JOIN FETCH obj.");
				select.append(enlace[i][0]);
				select.append(" ");
				select.append(enlace[i][1]);
				select.append(" ");
			}
		}
		return select.toString();
	}

	@Override
	public Entidad create(Entidad entidad) throws ScaExcepcion {
		getEntityManager().persist(entidad);
		return entidad;
	}

	@Override
	public Entidad update(Entidad entidad) throws ScaExcepcion {
		getEntityManager().merge(entidad);
		getEntityManager().flush();
		return entidad;
	}

	@Override
	public void delete(Entidad entidad) throws ScaExcepcion {
		getEntityManager().remove(entidad);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Entidad> buscarTodos(final String[]... enlace) throws ScaExcepcion {
		StringBuilder consulta = new StringBuilder(construirSelect(enlace));
		consulta.append(" ORDER BY obj.id");
		Query query = getEntityManager().createQuery(consulta.toString());
		return query.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Entidad> buscarTodosPaginado(final Integer inicio, final Integer cantidad, final String[]... enlace) throws ScaExcepcion {
		StringBuilder consulta = new StringBuilder(construirSelect(enlace));
		consulta.append(" ORDER BY obj.id");
		Query query = getEntityManager().createQuery(consulta.toString());
		query.setFirstResult(inicio);
		query.setMaxResults(cantidad);
		return query.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public Entidad buscarPorId(final Long id, final String[]... enlace) throws ScaExcepcion {
		StringBuilder consulta = new StringBuilder(construirSelect(enlace));
		consulta.append(" WHERE obj.id = :id");
		Query query = getEntityManager().createQuery(consulta.toString());
		query.setParameter("id", id);
		return (Entidad) query.getSingleResult();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Entidad> buscarTodosPorEstadoActivo(final String[]... enlace) throws ScaExcepcion {
		StringBuilder consulta = new StringBuilder(construirSelect(enlace));
		consulta.append(" WHERE obj.estado = TRUE ORDER BY obj.id");
		Query query = getEntityManager().createQuery(consulta.toString());
		return query.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Entidad> buscarTodosPorEstadoActivoPaginado(final Integer inicio, final Integer cantidad, final String[]... enlace) throws ScaExcepcion {
		StringBuilder consulta = new StringBuilder(construirSelect(enlace));
		consulta.append(" WHERE obj.estado = TRUE ORDER BY obj.id");
		Query query = getEntityManager().createQuery(consulta.toString());
		query.setFirstResult(inicio);
		query.setMaxResults(cantidad);
		return query.getResultList();
	}

	public EntityManager getEntityManager() {
		return this.em;
	}

}
