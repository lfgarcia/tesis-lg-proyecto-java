/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			EntidadBasica.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.generico;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

import javax.faces.context.FacesContext;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Mapeo genérico a la base de datos.
 * 
 * @author Luis García Castro
 */
@MappedSuperclass
public abstract class EntidadBasica implements Serializable {

	private static final long serialVersionUID = 7516049274615342024L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id",
			nullable = false)
	private Long id;

	@Column(name = "estado",
			nullable = false)
	private Boolean estado;

	@Column(name = "usuario_creacion",
			nullable = false)
	private String usuarioCreacion;

	@Column(name = "fecha_creacion",
			nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCreacion;

	@Column(name = "usuario_modificacion")
	private String usuarioModificacion;

	@Column(name = "fecha_modificacion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaModificacion;

	public EntidadBasica() {
		this.id = null;
	}

	public EntidadBasica(final Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntidadBasica other = (EntidadBasica) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@PrePersist
	public void onCreate() {
		try {
			FacesContext contexto = FacesContext.getCurrentInstance();
			Object sessionBB = contexto.getExternalContext().getSessionMap().get("sesionBB");
			Method getUsername = sessionBB.getClass().getMethod("getUsername");
			String username = (String) getUsername.invoke(sessionBB);
			this.setUsuarioCreacion(username);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
			this.setUsuarioCreacion("administrador");
		}
		this.setEstado(Boolean.TRUE);
		this.setFechaCreacion(new Date());
	}

	@PreUpdate
	public void onUpdate() {
		FacesContext contexto = FacesContext.getCurrentInstance();
		Object sessionBB = contexto.getExternalContext().getSessionMap().get("sesionBB");
		try {
			Method getUsername = sessionBB.getClass().getMethod("getUsername");
			String username = (String) getUsername.invoke(sessionBB);
			this.setUsuarioModificacion(username);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
			this.setUsuarioModificacion("administrador");
		}
		this.setFechaModificacion(new Date());
	}

}
