/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			HorarioPersonaDao.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao;

import java.util.List;

import ec.edu.puce.ltic.sca.entidad.HorarioPersona;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenerico;

/**
 * Consultas para el horario por persona.
 * 
 * @author Luis García Castro
 */
public interface HorarioPersonaDao extends DaoGenerico<HorarioPersona, Long> {

	/**
	 * Permite realizar la búsqueda del registro del horario persona por el
	 * código del día, hora de entrada y hora de salida.
	 * 
	 * @param codigoDia
	 * @param horaEntrada
	 * @param horaSalida
	 * @param personaId
	 * @return
	 * @throws ScaExcepcion
	 */
	HorarioPersona buscarPorHorarioPersona(final String codigoDia, final String horaEntrada, final String horaSalida, final Long idPersona) throws ScaExcepcion;

	/**
	 * Permite realizar la búsqueda de todo el horario completo.
	 * 
	 * @return Lista de horarios
	 * @throws ScaExcepcion
	 */
	List<HorarioPersona> buscarTodos() throws ScaExcepcion;

}
