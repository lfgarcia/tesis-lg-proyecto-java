/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			CatalogoDao.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao;

import java.util.List;

import ec.edu.puce.ltic.sca.entidad.Catalogo;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenerico;

/**
 * Consultas para los catálogos.
 * 
 * @author Luis García Castro
 */
public interface CatalogoDao extends DaoGenerico<Catalogo, Long> {

	/**
	 * Permite realizar la búsqueda de todos los catálogos por el código del
	 * tipo de catálogo.
	 * 
	 * @param codigo
	 * @return Lista de catálogos.
	 * @throws ScaExcepcion
	 */
	List<Catalogo> buscarTodosPorTipoCatalogoCodigo(final String codigo) throws ScaExcepcion;

	/**
	 * Permite realizar la búsqueda del catálogo por su código.
	 * 
	 * @param codigo
	 * @return Catálogo encontrado.
	 * @throws ScaExcepcion
	 */
	Catalogo buscarPorCodigo(final String codigo) throws ScaExcepcion;

	/**
	 * Permite realizar la búsqueda de todos los catálogos por coincidencias del
	 * nombre.
	 * 
	 * @param nombre
	 * @return Lista de catálogos.
	 * @throws ScaExcepcion
	 */
	List<Catalogo> buscarTodosPorNombre(final String nombre) throws ScaExcepcion;

	/**
	 * Permite realizar la búsqueda de todos los catálogos por coincidencias de
	 * la descripción.
	 * 
	 * @param descripcion
	 * @return lista de catálogos.
	 * @throws ScaExcepcion
	 */
	List<Catalogo> buscarTodosPorDescripcion(final String descripcion) throws ScaExcepcion;

}
