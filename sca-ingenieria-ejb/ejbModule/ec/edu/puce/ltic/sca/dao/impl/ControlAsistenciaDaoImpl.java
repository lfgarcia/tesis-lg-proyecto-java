/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ControlAsistenciaDaoImpl.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.edu.puce.ltic.sca.dao.ControlAsistenciaDao;
import ec.edu.puce.ltic.sca.entidad.ControlAsistencia;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenericoImpl;

/**
 * Implementación de las consultas para el control de asistencia.
 * 
 * @author Luis García Castro
 */
@Stateless
public class ControlAsistenciaDaoImpl extends DaoGenericoImpl<ControlAsistencia, Long> implements ControlAsistenciaDao {

	private static final long serialVersionUID = -5900829659755478597L;

	public ControlAsistenciaDaoImpl() {
		super(ControlAsistencia.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public ControlAsistencia buscarPorPersona(final String identificacionPuce) throws ScaExcepcion {
		Query query = getEntityManager().createNamedQuery(ControlAsistencia.buscarPorPersona);
		query.setParameter("identificacionPuce", identificacionPuce);
		List<ControlAsistencia> resultado = query.getResultList();
		if (resultado.isEmpty()) {
			return null;
		}
		return resultado.get(0);
	}

}
