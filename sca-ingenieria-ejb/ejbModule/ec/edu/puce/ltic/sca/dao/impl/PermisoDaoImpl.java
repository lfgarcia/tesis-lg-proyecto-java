/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			PermisoDaoImpl.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao.impl;

import javax.ejb.Stateless;

import ec.edu.puce.ltic.sca.dao.PermisoDao;
import ec.edu.puce.ltic.sca.entidad.Permiso;
import ec.edu.puce.ltic.sca.generico.DaoGenericoImpl;

/**
 * Implementación de las consultas para los permisos
 * 
 * @author Luis García Castro
 */
@Stateless
public class PermisoDaoImpl extends DaoGenericoImpl<Permiso, Long> implements PermisoDao {

	private static final long serialVersionUID = -8433818894077899131L;

	public PermisoDaoImpl() {
		super(Permiso.class);
	}

}
