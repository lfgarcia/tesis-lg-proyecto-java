/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ParametroDao.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao;

import java.util.List;

import ec.edu.puce.ltic.sca.entidad.Parametro;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenerico;

/**
 * Consultas para los parámetros del sistema.
 * 
 * @author Luis García Castro
 */
public interface ParametroDao extends DaoGenerico<Parametro, Long> {

	/**
	 * Permite realizar la búsqueda de todos los parámetros por el código del
	 * tipo de parámetro.
	 * 
	 * @param codigo
	 * @return Lista de parámetros.
	 * @throws ScaExcepcion
	 */
	List<Parametro> buscarTodosPorTipoParametroCodigo(final String codigo) throws ScaExcepcion;

	/**
	 * Permite realizar la búsqueda de un parámetro por su código.
	 * 
	 * @param codigo
	 * @return Parámetro encontrado.
	 * @throws ScaExcepcion
	 */
	Parametro buscarPorCodigo(final String codigo) throws ScaExcepcion;

	/**
	 * Permite realizar la búsqueda de todos los parámetros por coincidencias
	 * del nombre.
	 * 
	 * @param nombre
	 * @return Lista de parámetros.
	 * @throws ScaExcepcion
	 */
	List<Parametro> buscarTodosPorNombre(final String nombre) throws ScaExcepcion;

	/**
	 * Permite realizar la búsqueda de todos los parámetros por coincidencias de
	 * la descripción.
	 * 
	 * @param descripcion
	 * @return Lista de parámetros.
	 * @throws ScaExcepcion
	 */
	List<Parametro> buscarTodosPorDescripcion(final String descripcion) throws ScaExcepcion;

}
