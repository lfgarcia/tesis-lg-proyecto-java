/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			OpcionMenuDaoImpl.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.edu.puce.ltic.sca.dao.OpcionMenuDao;
import ec.edu.puce.ltic.sca.entidad.OpcionMenu;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenericoImpl;

/**
 * Implementación de las consultas para las opciones de menú.
 * 
 * @author Luis García Castro
 */
@Stateless
public class OpcionMenuDaoImpl extends DaoGenericoImpl<OpcionMenu, Long> implements OpcionMenuDao {

	private static final long serialVersionUID = -5767403588306190848L;

	public OpcionMenuDaoImpl() {
		super(OpcionMenu.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<OpcionMenu> buscarTodosPadres() throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(OpcionMenu.buscarTodosPadres);
		return consulta.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<OpcionMenu> buscarTodosPorPadre(final Long idPadre) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(OpcionMenu.buscarTodosPadres);
		consulta.setParameter("idPadre", idPadre);
		return consulta.getResultList();
	}

}
