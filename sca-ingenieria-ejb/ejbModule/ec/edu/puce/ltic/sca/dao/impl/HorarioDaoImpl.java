/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			HorarioDaoImpl.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.edu.puce.ltic.sca.dao.HorarioDao;
import ec.edu.puce.ltic.sca.entidad.Horario;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenericoImpl;

/**
 * Implementación de las consultas para el horario.
 * 
 * @author Luis García Castro
 */
@Stateless
public class HorarioDaoImpl extends DaoGenericoImpl<Horario, Long> implements HorarioDao {

	private static final long serialVersionUID = 48882834048760284L;

	public HorarioDaoImpl() {
		super(Horario.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Horario> buscarTodos() throws ScaExcepcion {
		Query query = getEntityManager().createNamedQuery(Horario.buscarTodos);
		return query.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public Horario buscarPorHoraEntradaCodigoDia(final String horaEntrada, final String codigoDia) throws ScaExcepcion {
		Query query = getEntityManager().createNamedQuery(Horario.buscarPorHoraEntradaCodigoDia);
		query.setParameter("horaEntrada", horaEntrada);
		query.setParameter("codigoDia", codigoDia);
		List<Horario> horarios = query.getResultList();
		if (horarios.isEmpty()) {
			return null;
		}
		return horarios.get(0);
	}

}
