/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			PersonaDao.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao;

import java.util.List;

import ec.edu.puce.ltic.sca.entidad.Persona;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenerico;

/**
 * Consultas para las personas registradas.
 * 
 * @author Luis García Castro
 */
public interface PersonaDao extends DaoGenerico<Persona, Long> {

	/**
	 * Permite realizar la búsqueda de todas las personas que posean un perfil
	 * inferior al de la persona conectada al sistema.
	 * 
	 * @param idPerfil
	 * @return Lista de personas.
	 * @throws ScaExcepcion
	 */
	List<Persona> buscarTodos(final Long idPerfil) throws ScaExcepcion;

	/**
	 * Permite realizar la búsqueda por la identificación de la persona.
	 * 
	 * @param idTipoIdentificacion
	 *            Tipo de identificación.
	 * @param numeroIdentificacion
	 *            Número de identifiación.
	 * @return Persona encontrada.
	 * @throws ScaExcepcion
	 */
	Persona buscarPorIdentificacion(final Long idTipoIdentificacion, final String numeroIdentificacion) throws ScaExcepcion;

	/**
	 * Permite realizar la búsqueda por la identifiación proporcionada por la
	 * PUCE.
	 * 
	 * @param identificacionPuce
	 * @return Persona encontrada.
	 * @throws ScaExcepcion
	 */
	Persona buscarPorIdentificacionPuce(final String identificacionPuce) throws ScaExcepcion;

}
