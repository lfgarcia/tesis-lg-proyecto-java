/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			OpcionMenuDao.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao;

import java.util.List;

import ec.edu.puce.ltic.sca.entidad.OpcionMenu;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenerico;

/**
 * Consultas para las opciones de menú.
 * 
 * @author Luis García Castro
 */
public interface OpcionMenuDao extends DaoGenerico<OpcionMenu, Long> {

	/**
	 * Permite realizar la búsqueda de todas las opciones de menú padres.
	 * 
	 * @return Lista de opciones de menú padres.
	 * @throws ScaExcepcion
	 */
	List<OpcionMenu> buscarTodosPadres() throws ScaExcepcion;

	/**
	 * Permite realizar la búsqueda de todas las opciones de menú por su padre.
	 * 
	 * @param idPadre
	 * @return Lista de opciones de menú hijos.
	 * @throws ScaExcepcion
	 */
	List<OpcionMenu> buscarTodosPorPadre(final Long idPadre) throws ScaExcepcion;

}
