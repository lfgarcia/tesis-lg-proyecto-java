/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			TipoCatalogoDao.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao;

import java.util.List;

import ec.edu.puce.ltic.sca.entidad.TipoCatalogo;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenerico;

/**
 * Consultas para los tipos de catálogos que maneja el sistema.
 * 
 * @author Luis García Castro
 */
public interface TipoCatalogoDao extends DaoGenerico<TipoCatalogo, Long> {

	/**
	 * Permite realizar la búsqueda por el código del tipo de catálogo.
	 * 
	 * @param codigo
	 * @return Tipo de catálogo encontrado.
	 * @throws ScaExcepcion
	 */
	TipoCatalogo buscarPorCodigo(final String codigo) throws ScaExcepcion;

	/**
	 * Permite realizar la búsqueda de todos los tipos de catálogos por
	 * coincidencias del nombre.
	 * 
	 * @param nombre
	 * @return Lista de tipos de catálogos.
	 * @throws ScaExcepcion
	 */
	List<TipoCatalogo> buscarTodosPorNombre(final String nombre) throws ScaExcepcion;

	/**
	 * Permite realizar la búqueda de todos los tipos de catálogos por
	 * coincidencias de la descripción.
	 * 
	 * @param descripcion
	 * @return Lista de tipos de catálogos.
	 * @throws ScaExcepcion
	 */
	List<TipoCatalogo> buscarTodosPorDescripcion(final String descripcion) throws ScaExcepcion;

}
