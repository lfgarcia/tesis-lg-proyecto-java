/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			PersonaDaoImpl.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.edu.puce.ltic.sca.dao.PersonaDao;
import ec.edu.puce.ltic.sca.entidad.Persona;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenericoImpl;

/**
 * Implementación de las consultas para las personas registradas.
 * 
 * @author Luis García Castro
 */
@Stateless
public class PersonaDaoImpl extends DaoGenericoImpl<Persona, Long> implements PersonaDao {

	private static final long serialVersionUID = 2134006599257424433L;

	public PersonaDaoImpl() {
		super(Persona.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Persona> buscarTodos(final Long idPerfil) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(Persona.buscarTodos);
		consulta.setParameter("idPerfil", idPerfil);
		List<Persona> resultado = consulta.getResultList();
		return resultado;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Persona buscarPorIdentificacion(final Long idTipoIdentificacion, final String numeroIdentificacion) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(Persona.buscarPorTipoIdentificacionAndNumeroIdentificacion);
		consulta.setParameter("idTipoIdentificacion", idTipoIdentificacion);
		consulta.setParameter("numeroIdentificacion", numeroIdentificacion);
		List<Persona> resultado = consulta.getResultList();
		if (resultado.isEmpty()) {
			return null;
		}
		return resultado.get(0);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Persona buscarPorIdentificacionPuce(final String identificacionPuce) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(Persona.buscarPorIdentificacionPuce);
		consulta.setParameter("identificacionPuce", identificacionPuce);
		List<Persona> resultado = consulta.getResultList();
		if (resultado.isEmpty()) {
			return null;
		}
		return resultado.get(0);
	}

}
