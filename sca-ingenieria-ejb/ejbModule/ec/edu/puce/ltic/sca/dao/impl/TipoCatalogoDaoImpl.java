/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			TipoCatalogoDao.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.edu.puce.ltic.sca.dao.TipoCatalogoDao;
import ec.edu.puce.ltic.sca.entidad.TipoCatalogo;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenericoImpl;

/**
 * Implementación de las consultas para los tipos de catálogos que maneja el
 * sistema.
 * 
 * @author Luis García Castro
 */
@Stateless
public class TipoCatalogoDaoImpl extends DaoGenericoImpl<TipoCatalogo, Long> implements TipoCatalogoDao {

	private static final long serialVersionUID = 6857185098120688424L;

	public TipoCatalogoDaoImpl() {
		super(TipoCatalogo.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public TipoCatalogo buscarPorCodigo(final String codigo) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(TipoCatalogo.buscarPorCodigo);
		consulta.setParameter("codigo", codigo);
		List<TipoCatalogo> resultado = consulta.getResultList();
		if (resultado.isEmpty()) {
			return null;
		}
		return resultado.get(0);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<TipoCatalogo> buscarTodosPorNombre(final String nombre) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(TipoCatalogo.buscarTodosPorNombre);
		consulta.setParameter("nombre", nombre);
		return consulta.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<TipoCatalogo> buscarTodosPorDescripcion(final String descripcion) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(TipoCatalogo.buscarTodosPorDescripcion);
		consulta.setParameter("descripcion", descripcion);
		return consulta.getResultList();
	}

}
