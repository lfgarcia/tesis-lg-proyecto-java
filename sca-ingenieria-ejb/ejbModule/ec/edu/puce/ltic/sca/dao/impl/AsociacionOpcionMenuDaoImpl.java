/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			AsociacionOpcionMenuDaoImpl.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.edu.puce.ltic.sca.dao.AsociacionOpcionMenuDao;
import ec.edu.puce.ltic.sca.entidad.AsociacionOpcionMenu;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenericoImpl;

/**
 * Implementación de las consultas para la asociación de las opciones de menú.
 * 
 * @author Luis García Castro
 */
@Stateless
public class AsociacionOpcionMenuDaoImpl extends DaoGenericoImpl<AsociacionOpcionMenu, Long> implements AsociacionOpcionMenuDao {

	private static final long serialVersionUID = 4629806554888884093L;

	public AsociacionOpcionMenuDaoImpl() {
		super(AsociacionOpcionMenu.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<AsociacionOpcionMenu> buscarPorPerfilId(final Long idPerfil) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(AsociacionOpcionMenu.buscarPorPerfilId);
		consulta.setParameter("idPerfil", idPerfil);
		List<AsociacionOpcionMenu> resultado = consulta.getResultList();
		return resultado;
	}

}
