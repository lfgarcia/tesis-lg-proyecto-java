/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			PerfilDaoImpl.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.edu.puce.ltic.sca.dao.PerfilDao;
import ec.edu.puce.ltic.sca.entidad.Perfil;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenericoImpl;

/**
 * Implementación de las consultas para los perfiles del sistema.
 * 
 * @author Luis García Castro
 */
@Stateless
public class PerfilDaoImpl extends DaoGenericoImpl<Perfil, Long> implements PerfilDao {

	private static final long serialVersionUID = 7367690763402903799L;

	public PerfilDaoImpl() {
		super(Perfil.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Perfil> buscarPorTipoRol(final List<String> perfiles) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(Perfil.buscarPorTipoRol);
		consulta.setParameter("perfiles", perfiles);
		List<Perfil> resultado = consulta.getResultList();
		return resultado;
	}

}
