/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			PerfilDao.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao;

import java.util.List;

import ec.edu.puce.ltic.sca.entidad.Perfil;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenerico;

/**
 * Consultas para los perfiles del sistema.
 * 
 * @author Luis García Castro
 */
public interface PerfilDao extends DaoGenerico<Perfil, Long> {

	/**
	 * Permite realizar la búsqueda de los perfiles por el tipo de rol que
	 * desempeña.
	 * 
	 * @param perfiles
	 *            Lista de los códigos de los perfiles del sistema.
	 * @return Lista de Perfiles.
	 * @throws ScaExcepcion
	 */
	List<Perfil> buscarPorTipoRol(final List<String> perfiles) throws ScaExcepcion;

}
