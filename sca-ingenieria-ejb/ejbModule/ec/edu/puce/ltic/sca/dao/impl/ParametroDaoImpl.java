/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ParametroDaoImpl.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.edu.puce.ltic.sca.dao.ParametroDao;
import ec.edu.puce.ltic.sca.entidad.Parametro;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenericoImpl;

/**
 * Implementación de las consultas para los parámetros del sistema.
 * 
 * @author Luis García Castro
 */
@Stateless
public class ParametroDaoImpl extends DaoGenericoImpl<Parametro, Long> implements ParametroDao {

	private static final long serialVersionUID = -6730550200123710560L;

	public ParametroDaoImpl() {
		super(Parametro.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Parametro> buscarTodosPorTipoParametroCodigo(final String codigo) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(Parametro.buscarTodosPorTipoParametroCodigo);
		consulta.setParameter("codigo", codigo);
		return consulta.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public Parametro buscarPorCodigo(final String codigo) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(Parametro.buscarPorCodigo);
		consulta.setParameter("codigo", codigo);
		List<Parametro> resultado = consulta.getResultList();
		if (resultado.isEmpty()) {
			return null;
		}
		return resultado.get(0);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Parametro> buscarTodosPorNombre(final String nombre) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(Parametro.buscarTodosPorNombre);
		consulta.setParameter("nombre", nombre);
		return consulta.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Parametro> buscarTodosPorDescripcion(final String descripcion) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(Parametro.buscarTodosPorDescripcion);
		consulta.setParameter("descripcion", descripcion);
		return consulta.getResultList();
	}

}
