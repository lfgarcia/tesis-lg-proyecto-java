/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			CatalogoDaoImpl.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.edu.puce.ltic.sca.dao.CatalogoDao;
import ec.edu.puce.ltic.sca.entidad.Catalogo;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenericoImpl;

/**
 * Implementación de las consultas para los catálogos.
 * 
 * @author Luis García Castro
 */
@Stateless
public class CatalogoDaoImpl extends DaoGenericoImpl<Catalogo, Long> implements CatalogoDao {

	private static final long serialVersionUID = 7287029840479348429L;

	public CatalogoDaoImpl() {
		super(Catalogo.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Catalogo> buscarTodosPorTipoCatalogoCodigo(final String codigo) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(Catalogo.buscarTodosPorTipoCatalogoCodigo);
		consulta.setParameter("codigo", codigo);
		return consulta.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public Catalogo buscarPorCodigo(final String codigo) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(Catalogo.buscarPorCodigo);
		consulta.setParameter("codigo", codigo);
		List<Catalogo> resultado = consulta.getResultList();
		if (resultado.isEmpty()) {
			return null;
		}
		return resultado.get(0);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Catalogo> buscarTodosPorNombre(final String nombre) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(Catalogo.buscarTodosPorNombre);
		consulta.setParameter("nombre", nombre);
		return consulta.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Catalogo> buscarTodosPorDescripcion(final String descripcion) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(Catalogo.buscarTodosPorDescripcion);
		consulta.setParameter("descripcion", descripcion);
		return consulta.getResultList();
	}

}
