/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			HorarioReasignacionDao.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao;

import ec.edu.puce.ltic.sca.entidad.HorarioReasignacion;
import ec.edu.puce.ltic.sca.generico.DaoGenerico;

/**
 * Consultas para la reasignación de horario.
 * 
 * @author Luis García Castro
 */
public interface HorarioReasignacionDao extends DaoGenerico<HorarioReasignacion, Long> {

}
