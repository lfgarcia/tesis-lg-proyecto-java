/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			UsuarioDaoImpl.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.edu.puce.ltic.sca.dao.UsuarioDao;
import ec.edu.puce.ltic.sca.entidad.Usuario;
import ec.edu.puce.ltic.sca.excepcion.EncodeExcepcion;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenericoImpl;
import ec.edu.puce.ltic.sca.utils.EncriptarSha;

/**
 * Implementación de las consultas para los usuarios.
 * 
 * @author Luis García Castro
 */
@Stateless
public class UsuarioDaoImpl extends DaoGenericoImpl<Usuario, Long> implements UsuarioDao {

	private static final long serialVersionUID = -834473432065837715L;

	public UsuarioDaoImpl() {
		super(Usuario.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Usuario> buscarTodos() throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(Usuario.buscarTodos);
		List<Usuario> resultado = consulta.getResultList();
		return resultado;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Usuario doLogin(final String usuario, final String clave) throws ScaExcepcion {
		try {
			Query consulta = getEntityManager().createNamedQuery(Usuario.doLogin);
			consulta.setParameter("usuario", usuario);
			consulta.setParameter("clave", EncriptarSha.encriptar(clave));
			List<Usuario> resultado = consulta.getResultList();
			if (resultado.isEmpty()) {
				this.comprobarUsuario(usuario);
				throw new ScaExcepcion("ERR-LOGIN-02", "Usuario o Contraseña incorrecto.");
			}
			return resultado.get(0);
		} catch (EncodeExcepcion e) {
			throw new ScaExcepcion("ENCODE", "No se pudo codificar la contraseña.");
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public Usuario comprobarUsuario(final String usuario) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(Usuario.comprobarUsuario);
		consulta.setParameter("usuario", usuario);
		List<Usuario> resultado = consulta.getResultList();
		if (resultado.isEmpty()) {
			throw new ScaExcepcion("ERR-LOGIN-01", "No existe el usuario ingresado.");
		}
		return resultado.get(0);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Usuario buscarPorPersonaId(final Long idPersona) throws ScaExcepcion {
		Query consulta = getEntityManager().createNamedQuery(Usuario.buscarPorPersonaId);
		consulta.setParameter("idPersona", idPersona);
		List<Usuario> resultado = consulta.getResultList();
		if (resultado.isEmpty()) {
			return null;
		}
		return resultado.get(0);
	}

}
