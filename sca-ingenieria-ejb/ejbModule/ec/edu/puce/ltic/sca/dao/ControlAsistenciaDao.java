/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ControlAsistenciaDao.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao;

import ec.edu.puce.ltic.sca.entidad.ControlAsistencia;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenerico;

/**
 * Consultas para el control de asistencia.
 * 
 * @author Luis García Castro
 */
public interface ControlAsistenciaDao extends DaoGenerico<ControlAsistencia, Long> {

	/**
	 * Permite identificar si la persona ya registró el ingreso de su
	 * asistencia.
	 * 
	 * @param identificacionPuce
	 * @return Control de asistencia.
	 * @throws ScaExcepcion
	 */
	ControlAsistencia buscarPorPersona(final String identificacionPuce) throws ScaExcepcion;

}
