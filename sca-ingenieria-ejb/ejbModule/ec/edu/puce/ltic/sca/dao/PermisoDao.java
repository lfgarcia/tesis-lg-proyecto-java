/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			PermisoDao.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao;

import ec.edu.puce.ltic.sca.entidad.Permiso;
import ec.edu.puce.ltic.sca.generico.DaoGenerico;

/**
 * Consultas para los permisos.
 * 
 * @author Luis García Castro
 */
public interface PermisoDao extends DaoGenerico<Permiso, Long> {

}
