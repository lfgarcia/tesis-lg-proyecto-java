/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			HorarioReasignacionDaoImpl.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao.impl;

import javax.ejb.Stateless;

import ec.edu.puce.ltic.sca.dao.HorarioReasignacionDao;
import ec.edu.puce.ltic.sca.entidad.HorarioReasignacion;
import ec.edu.puce.ltic.sca.generico.DaoGenericoImpl;

/**
 * Implementación de las consultas para la reasignación de horario.
 * 
 * @author Luis García Castro
 */
@Stateless
public class HorarioReasignacionDaoImpl extends DaoGenericoImpl<HorarioReasignacion, Long> implements HorarioReasignacionDao {

	private static final long serialVersionUID = 1277320251343048959L;

	public HorarioReasignacionDaoImpl() {
		super(HorarioReasignacion.class);
	}

}
