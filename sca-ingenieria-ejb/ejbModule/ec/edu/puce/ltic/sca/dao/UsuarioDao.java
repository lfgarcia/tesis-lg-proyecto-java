/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			UsuarioDao.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao;

import java.util.List;

import ec.edu.puce.ltic.sca.entidad.Usuario;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenerico;

/**
 * Consultas para los usuarios.
 * 
 * @author Luis García Castro
 */
public interface UsuarioDao extends DaoGenerico<Usuario, Long> {

	/**
	 * Permite realizar la búsqueda de todos los usuarios registrados en el
	 * sistema.
	 * 
	 * @return Lista de usuarios.
	 * @throws ScaExcepcion
	 */
	List<Usuario> buscarTodos() throws ScaExcepcion;

	/**
	 * Permite realizar el login en el sistema.
	 * 
	 * @param usuario
	 * @param clave
	 * @return Acceso al sistema.
	 * @throws ScaExcepcion
	 */
	Usuario doLogin(final String usuario, final String clave) throws ScaExcepcion;

	/**
	 * Permite realizar la comprobación del usuario en el sistema.
	 * 
	 * @param usuario
	 * @return Usuario encontrado.
	 * @throws ScaExcepcion
	 */
	Usuario comprobarUsuario(final String usuario) throws ScaExcepcion;

	/**
	 * Permite realizar la busqueda del usuario por el id de la persona.
	 * 
	 * @param idPersona
	 * @return Usuario encontrado.
	 * @throws ScaExcepcion
	 */
	Usuario buscarPorPersonaId(final Long idPersona) throws ScaExcepcion;

}
