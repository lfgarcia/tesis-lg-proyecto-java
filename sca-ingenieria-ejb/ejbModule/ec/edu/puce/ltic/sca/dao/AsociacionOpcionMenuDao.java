/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			AsociacionOpcionMenuDao.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao;

import java.util.List;

import ec.edu.puce.ltic.sca.entidad.AsociacionOpcionMenu;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenerico;

/**
 * Consultas para la asociación de las opciones de menú.
 * 
 * @author Luis García Castro
 */
public interface AsociacionOpcionMenuDao extends DaoGenerico<AsociacionOpcionMenu, Long> {

	/**
	 * Permite realiza la búsqueda por el id de perfil.
	 * 
	 * @param idPerfil
	 * @return Lista de asociaciones de opciones por menú.
	 * @throws ScaExcepcion
	 */
	List<AsociacionOpcionMenu> buscarPorPerfilId(final Long idPerfil) throws ScaExcepcion;
}
