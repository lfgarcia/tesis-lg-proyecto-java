/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			HorarioPersonaDaoImpl.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.edu.puce.ltic.sca.dao.HorarioPersonaDao;
import ec.edu.puce.ltic.sca.entidad.HorarioPersona;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenericoImpl;

/**
 * Implementación de las consultas para el horario por persona.
 * 
 * @author Luis García Castro
 */
@Stateless
public class HorarioPersonaDaoImpl extends DaoGenericoImpl<HorarioPersona, Long> implements HorarioPersonaDao {

	private static final long serialVersionUID = -4765450817199514767L;

	public HorarioPersonaDaoImpl() {
		super(HorarioPersona.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public HorarioPersona buscarPorHorarioPersona(final String codigoDia, final String horaEntrada, final String horaSalida, final Long idPersona) throws ScaExcepcion {
		Query query = getEntityManager().createNamedQuery(HorarioPersona.buscarPorHorarioPersona);
		query.setParameter("codigoDia", codigoDia);
		query.setParameter("horaEntrada", horaEntrada);
		query.setParameter("horaSalida", horaSalida);
		query.setParameter("idPersona", idPersona);
		List<HorarioPersona> horarioPersonas = query.getResultList();
		if (horarioPersonas.isEmpty()) {
			return null;
		}
		return horarioPersonas.get(0);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<HorarioPersona> buscarTodos() throws ScaExcepcion {
		Query query = getEntityManager().createNamedQuery(HorarioPersona.buscarTodos);
		return query.getResultList();
	}

}
