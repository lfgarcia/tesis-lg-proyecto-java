/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			HorarioDao.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.dao;

import java.util.List;

import ec.edu.puce.ltic.sca.entidad.Horario;
import ec.edu.puce.ltic.sca.excepcion.ScaExcepcion;
import ec.edu.puce.ltic.sca.generico.DaoGenerico;

/**
 * Consultas para el horario.
 * 
 * @author Luis García Castro
 */
public interface HorarioDao extends DaoGenerico<Horario, Long> {

	/**
	 * Permite realizar la búsqueda de todo el horario completo.
	 * 
	 * @return Lista de horarios
	 * @throws ScaExcepcion
	 */
	List<Horario> buscarTodos() throws ScaExcepcion;

	/**
	 * Permite realizar la búsqueda del horario por la hora de entrada y el
	 * código de día.
	 * 
	 * @param horaEntrada
	 * @param codigoDia
	 * @return Horario encontrado.
	 * @throws ScaExcepcion
	 */
	Horario buscarPorHoraEntradaCodigoDia(final String horaEntrada, final String codigoDia) throws ScaExcepcion;

}
