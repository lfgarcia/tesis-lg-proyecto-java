/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			EncodeExcepcion.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.excepcion;

/**
 * Excepción creada al no poder encriptar.
 * 
 * @author Luis García Castro
 */
public class EncodeExcepcion extends Exception {

	private static final long serialVersionUID = 1555419098304597421L;

	private String codigo;

	private String mensaje;

	public EncodeExcepcion() {
		super();
	}

	public EncodeExcepcion(final Throwable cause) {
		super(cause);
	}

	public EncodeExcepcion(final String mensaje, final Throwable cause) {
		super(mensaje, cause);
		this.mensaje = mensaje;
	}

	public EncodeExcepcion(final String codigo, final String mensaje, final Throwable cause) {
		super(mensaje, cause);
		this.codigo = codigo;
		this.mensaje = mensaje;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
