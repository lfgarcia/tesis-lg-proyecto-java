/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ScaExcepcion.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.excepcion;

/**
 * Excepción predefinida para errores en el sistema con mensajes personalizados.
 * 
 * @author Luis García Castro
 */
public class ScaExcepcion extends Exception {

	private static final long serialVersionUID = 2464632981249135024L;

	private String codigo;

	private String mensaje;

	public ScaExcepcion() {
		super();
	}

	public ScaExcepcion(final Throwable cause) {
		super(cause);
	}

	public ScaExcepcion(final String mensaje, final Throwable cause) {
		super(mensaje, cause);
		this.mensaje = mensaje;
	}

	public ScaExcepcion(final String codigo, final String mensaje, final Throwable cause) {
		super(mensaje, cause);
		this.codigo = codigo;
		this.mensaje = mensaje;
	}

	public ScaExcepcion(final String codigo, final String mensaje) {
		super(mensaje);
		this.codigo = codigo;
		this.mensaje = mensaje;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
