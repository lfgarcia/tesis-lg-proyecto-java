/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ReporteCalculoFaltas.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.reporte.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import ec.edu.puce.ltic.sca.generico.ReporteBasica;

/**
 * Mapeo de clase a la base de datos.
 * 
 * @author Luis García Castro
 */
@Entity
@Table(	name = "reporte_calculo_faltas",
		schema = "reporte")
@Immutable
public class ReporteCalculoFaltas extends ReporteBasica {

	private static final long serialVersionUID = 7921384791374166721L;

	@Column(name = "horas_total")
	private Double horasTotal;

	@Column(name = "horas_asistencia")
	private Double horasAsistencia;

	@Column(name = "total_faltas")
	private Double totalFaltas;

	public Double getHorasTotal() {
		return horasTotal;
	}

	public void setHorasTotal(Double horasTotal) {
		this.horasTotal = horasTotal;
	}

	public Double getHorasAsistencia() {
		return horasAsistencia;
	}

	public void setHorasAsistencia(Double horasAsistencia) {
		this.horasAsistencia = horasAsistencia;
	}

	public Double getTotalFaltas() {
		return totalFaltas;
	}

	public void setTotalFaltas(Double totalFaltas) {
		this.totalFaltas = totalFaltas;
	}

}
