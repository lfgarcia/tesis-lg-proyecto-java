/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ReporteCalculoFaltasDao.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.reporte.dao;

import ec.edu.puce.ltic.sca.generico.DaoGenerico;
import ec.edu.puce.ltic.sca.reporte.entidad.ReporteCalculoFaltas;

/**
 * Consultas para el reporte específico.
 * 
 * @author Luis García Castro
 */
public interface ReporteCalculoFaltasDao extends DaoGenerico<ReporteCalculoFaltas, Long> {

}
