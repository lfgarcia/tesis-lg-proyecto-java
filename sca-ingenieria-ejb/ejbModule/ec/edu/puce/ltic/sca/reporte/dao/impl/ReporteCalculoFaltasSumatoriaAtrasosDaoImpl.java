/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ReporteCalculoFaltasSumatoriaAtrasosDaoImpl.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.reporte.dao.impl;

import javax.ejb.Stateless;

import ec.edu.puce.ltic.sca.generico.DaoGenericoImpl;
import ec.edu.puce.ltic.sca.reporte.dao.ReporteCalculoFaltasSumatoriaAtrasosDao;
import ec.edu.puce.ltic.sca.reporte.entidad.ReporteCalculoFaltasSumatoriaAtrasos;

/**
 * Implementación de las consultas para el reporte específico.
 * 
 * @author Luis García Castro
 */
@Stateless
public class ReporteCalculoFaltasSumatoriaAtrasosDaoImpl extends DaoGenericoImpl<ReporteCalculoFaltasSumatoriaAtrasos, Long> implements ReporteCalculoFaltasSumatoriaAtrasosDao {

	private static final long serialVersionUID = 2999497081340243816L;

	public ReporteCalculoFaltasSumatoriaAtrasosDaoImpl() {
		super(ReporteCalculoFaltasSumatoriaAtrasos.class);
	}

}
