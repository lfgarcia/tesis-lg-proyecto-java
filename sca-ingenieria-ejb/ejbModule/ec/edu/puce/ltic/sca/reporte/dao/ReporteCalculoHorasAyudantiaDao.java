/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ReporteCalculoHorasAyudantiaDao.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.reporte.dao;

import ec.edu.puce.ltic.sca.generico.DaoGenerico;
import ec.edu.puce.ltic.sca.reporte.entidad.ReporteCalculoHorasAyudantia;

/**
 * Consultas para el reporte específico.
 * 
 * @author Luis García Castro
 */
public interface ReporteCalculoHorasAyudantiaDao extends DaoGenerico<ReporteCalculoHorasAyudantia, Long> {

}
