/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ReporteCalculoHorasAyudantia.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.reporte.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import ec.edu.puce.ltic.sca.generico.ReporteBasica;

/**
 * Mapeo de clase a la base de datos.
 * 
 * @author Luis García Castro
 */
@Entity
@Table(	name = "reporte_calculo_horas_ayudantia",
		schema = "reporte")
@Immutable
public class ReporteCalculoHorasAyudantia extends ReporteBasica {

	private static final long serialVersionUID = 4645650286386279100L;

	@Column(name = "horas_asistencia")
	private Double horasAsistencia;

	@Column(name = "horas_certificado")
	private Double horasCertificado;

	public Double getHorasAsistencia() {
		return horasAsistencia;
	}

	public void setHorasAsistencia(Double horasAsistencia) {
		this.horasAsistencia = horasAsistencia;
	}

	public Double getHorasCertificado() {
		return horasCertificado;
	}

	public void setHorasCertificado(Double horasCertificado) {
		this.horasCertificado = horasCertificado;
	}

}
