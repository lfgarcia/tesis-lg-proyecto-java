/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ReporteCalculoFaltasSumatoriaAtrasosDao.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.reporte.dao;

import ec.edu.puce.ltic.sca.generico.DaoGenerico;
import ec.edu.puce.ltic.sca.reporte.entidad.ReporteCalculoFaltasSumatoriaAtrasos;

/**
 * Consultas para el reporte específico.
 * 
 * @author Luis García Castro
 */
public interface ReporteCalculoFaltasSumatoriaAtrasosDao extends DaoGenerico<ReporteCalculoFaltasSumatoriaAtrasos, Long> {

}
