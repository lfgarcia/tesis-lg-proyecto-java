/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ReporteCalculoAtrasosDaoImpl.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.reporte.dao.impl;

import javax.ejb.Stateless;

import ec.edu.puce.ltic.sca.generico.DaoGenericoImpl;
import ec.edu.puce.ltic.sca.reporte.dao.ReporteCalculoAtrasosDao;
import ec.edu.puce.ltic.sca.reporte.entidad.ReporteCalculoAtrasos;

/**
 * Implementación de las consultas para el reporte específico.
 * 
 * @author Luis García Castro
 */
@Stateless
public class ReporteCalculoAtrasosDaoImpl extends DaoGenericoImpl<ReporteCalculoAtrasos, Long> implements ReporteCalculoAtrasosDao {

	private static final long serialVersionUID = 7057288130816866554L;

	public ReporteCalculoAtrasosDaoImpl() {
		super(ReporteCalculoAtrasos.class);
	}

}
