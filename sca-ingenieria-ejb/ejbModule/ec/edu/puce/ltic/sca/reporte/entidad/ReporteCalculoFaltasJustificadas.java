/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ReporteCalculoFaltasJustificadas.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.reporte.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import ec.edu.puce.ltic.sca.generico.ReporteBasica;

/**
 * Mapeo de clase a la base de datos.
 * 
 * @author Luis García Castro
 */
@Entity
@Table(	name = "reporte_calculo_faltas_justificadas",
		schema = "reporte")
@Immutable
public class ReporteCalculoFaltasJustificadas extends ReporteBasica {

	private static final long serialVersionUID = 7564867143947475950L;

	@Column(name = "cantidad_permisos")
	private Long cantidadPermisos;

	@Column(name = "cantidad_justificados")
	private Long cantidadJustificados;

	@Column(name = "cantidad_injustificados")
	private Long cantidadInjustificados;

	public Long getCantidadPermisos() {
		return cantidadPermisos;
	}

	public void setCantidadPermisos(Long cantidadPermisos) {
		this.cantidadPermisos = cantidadPermisos;
	}

	public Long getCantidadJustificados() {
		return cantidadJustificados;
	}

	public void setCantidadJustificados(Long cantidadJustificados) {
		this.cantidadJustificados = cantidadJustificados;
	}

	public Long getCantidadInjustificados() {
		return cantidadInjustificados;
	}

	public void setCantidadInjustificados(Long cantidadInjustificados) {
		this.cantidadInjustificados = cantidadInjustificados;
	}

}
