/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ReporteCalculoFaltasSumatoriaAtrasos.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.reporte.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import ec.edu.puce.ltic.sca.generico.ReporteBasica;

/**
 * Mapeo de clase a la base de datos.
 * 
 * @author Luis García Castro
 */
@Entity
@Table(	name = "reporte_calculo_faltas_sumatoria_atrasos",
		schema = "reporte")
@Immutable
public class ReporteCalculoFaltasSumatoriaAtrasos extends ReporteBasica {

	private static final long serialVersionUID = -4497615883583534445L;

	@Column(name = "atrasos_total")
	private Double atrasosTotal;

	@Column(name = "faltas_total")
	private Double faltasTotal;

	public Double getAtrasosTotal() {
		return atrasosTotal;
	}

	public void setAtrasosTotal(Double atrasosTotal) {
		this.atrasosTotal = atrasosTotal;
	}

	public Double getFaltasTotal() {
		return faltasTotal;
	}

	public void setFaltasTotal(Double faltasTotal) {
		this.faltasTotal = faltasTotal;
	}

}
