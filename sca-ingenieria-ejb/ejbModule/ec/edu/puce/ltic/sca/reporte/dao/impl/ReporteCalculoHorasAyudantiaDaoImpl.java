/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ReporteCalculoHorasAyudantiaDaoImpl.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.reporte.dao.impl;

import javax.ejb.Stateless;

import ec.edu.puce.ltic.sca.generico.DaoGenericoImpl;
import ec.edu.puce.ltic.sca.reporte.dao.ReporteCalculoHorasAyudantiaDao;
import ec.edu.puce.ltic.sca.reporte.entidad.ReporteCalculoHorasAyudantia;

/**
 * Implementación de las consultas para el reporte específico.
 * 
 * @author Luis García Castro
 */
@Stateless
public class ReporteCalculoHorasAyudantiaDaoImpl extends DaoGenericoImpl<ReporteCalculoHorasAyudantia, Long> implements ReporteCalculoHorasAyudantiaDao {

	private static final long serialVersionUID = 7891551816978736283L;

	public ReporteCalculoHorasAyudantiaDaoImpl() {
		super(ReporteCalculoHorasAyudantia.class);
	}

}
