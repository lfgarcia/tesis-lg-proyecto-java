/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ReporteCalculoFaltasJustificadasDao.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.reporte.dao;

import ec.edu.puce.ltic.sca.generico.DaoGenerico;
import ec.edu.puce.ltic.sca.reporte.entidad.ReporteCalculoFaltasJustificadas;

/**
 * Consultas para el reporte específico.
 * 
 * @author Luis García Castro
 */
public interface ReporteCalculoFaltasJustificadasDao extends DaoGenerico<ReporteCalculoFaltasJustificadas, Long> {

}
