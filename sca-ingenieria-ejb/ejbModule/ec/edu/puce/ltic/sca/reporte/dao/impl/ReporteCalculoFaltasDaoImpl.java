/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ReporteCalculoFaltasDaoImpl.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.reporte.dao.impl;

import javax.ejb.Stateless;

import ec.edu.puce.ltic.sca.generico.DaoGenericoImpl;
import ec.edu.puce.ltic.sca.reporte.dao.ReporteCalculoFaltasDao;
import ec.edu.puce.ltic.sca.reporte.entidad.ReporteCalculoFaltas;

/**
 * Implementación de las consultas para el reporte específico.
 * 
 * @author Luis García Castro
 */
@Stateless
public class ReporteCalculoFaltasDaoImpl extends DaoGenericoImpl<ReporteCalculoFaltas, Long> implements ReporteCalculoFaltasDao {

	private static final long serialVersionUID = 1849337837390676717L;

	public ReporteCalculoFaltasDaoImpl() {
		super(ReporteCalculoFaltas.class);
	}

}
