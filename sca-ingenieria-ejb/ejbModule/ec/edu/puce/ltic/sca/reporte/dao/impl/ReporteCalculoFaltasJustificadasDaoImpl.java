/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			ReporteCalculoFaltasJustificadasDaoImpl.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.reporte.dao.impl;

import javax.ejb.Stateless;

import ec.edu.puce.ltic.sca.generico.DaoGenericoImpl;
import ec.edu.puce.ltic.sca.reporte.dao.ReporteCalculoFaltasJustificadasDao;
import ec.edu.puce.ltic.sca.reporte.entidad.ReporteCalculoFaltasJustificadas;

/**
 * Implementación de las consultas para el reporte específico.
 * 
 * @author Luis García Castro
 */
@Stateless
public class ReporteCalculoFaltasJustificadasDaoImpl extends DaoGenericoImpl<ReporteCalculoFaltasJustificadas, Long> implements ReporteCalculoFaltasJustificadasDao {

	private static final long serialVersionUID = 5054161169189318700L;

	public ReporteCalculoFaltasJustificadasDaoImpl() {
		super(ReporteCalculoFaltasJustificadas.class);
	}

}
