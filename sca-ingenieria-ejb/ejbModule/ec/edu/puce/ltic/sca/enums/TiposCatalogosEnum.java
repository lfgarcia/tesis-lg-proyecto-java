/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			TiposCatalogosEnum.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.enums;

/**
 * Enumerador para los tipos de catálogos por defecto del sistema.
 * 
 * @author Luis García Castro
 */
public enum TiposCatalogosEnum {

	TIPO_PARAMETRO("TIP_PAR", "Tipo de Parámetro"),
	TIPO_IDENTIFICACION("TIP_ID", "Tipo de Identificacion"),
	GENERO("GEN", "Genero");

	private String codigo;

	private String nombre;

	private TiposCatalogosEnum(String codigo, String nombre) {
		this.codigo = codigo;
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
