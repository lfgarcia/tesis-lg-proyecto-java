/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			PerfilesEnum.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.enums;

/**
 * Enumerador para los perfiles tipos administrativos.
 * 
 * @author Luis García Castro
 */
public enum PerfilesEnum {

	DOCENTE("TIP_PER_DOC", "Docente"),
	BECARIO("TIP_PER_BEC", "Becario");

	private String codigo;

	private String nombre;

	private PerfilesEnum(String codigo, String nombre) {
		this.codigo = codigo;
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
