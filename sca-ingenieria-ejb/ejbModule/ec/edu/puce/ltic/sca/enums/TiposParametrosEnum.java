/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			TiposParametrosEnum.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.enums;

/**
 * Enumerador para los tipos de parámetros por defecto del sistema.
 * 
 * @author Luis García Castro
 */
public enum TiposParametrosEnum {

	VALOR_FECHA("TIP_PAR_FEC", "Valor fecha"),
	VALOR_HORA("TIP_PAR_HOR", "Valor hora"),
	VALOR_ENTERO("TIP_PAR_NUM", "Valor entero"),
	VALOR_DECIMAL("TIP_PAR_DEC", "Valor decimal"),
	VALOR_TEXTO("TIP_PAR_TXT", "Valor texto");

	private String codigo;

	private String nombre;

	private TiposParametrosEnum(String codigo, String nombre) {
		this.codigo = codigo;
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
