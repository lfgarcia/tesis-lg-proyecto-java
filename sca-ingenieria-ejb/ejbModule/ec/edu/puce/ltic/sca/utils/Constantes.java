/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			Constantes.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.utils;

/**
 * Archivo de constantes que utilizará el sistema SCA.
 * 
 * @author Luis García Castro
 */
public interface Constantes {

	/* FETCH EN CONSULTAS. */
	public static final String[] fetchTipoCatalogo = { "tipoCatalogo", "tc" };

	public static final String[] fetchTipoParametro = { "tipoParametro", "tp" };

	public static final String[] fetchTipoIdentificacion = { "tipoIdentificacion", "ti" };

	public static final String[] fetchGenero = { "genero", "ge" };

	public static final String[] fetchPersona = { "persona", "pe" };
	/* ***** ***** ***** */

	/* CORREOS ELECTRÓNICOS PREDEFINIDOS */
	public static final String ASUNTO_GENERACION_USUARIO = "Generación de usuario.";

	public static final String GENERACION_USUARIO_CABECERA = "<p>Estimado/a <b>{{nombres}} {{apellidos}}</b>.</p>";

	public static final String GENERACION_USUARIO_CUERPO = "<p>Se gener&oacute; su usuario y contrase&ntilde;a con la siguiente informaci&oacute;n:</p><p><b>Usuario:</b> {{nombreUsuario}}</p><p><b>Clave:</b> {{clave}}</p>";

	public static final String ASUNTO_RECUPERAR_USUARIO = "Recuperación de contraseña.";

	public static final String RECUPERAR_USUARIO_CABECERA = "<p>Estimado/a <b>{{nombres}} {{apellidos}}</b>.</p>";

	public static final String RECUPERAR_USUARIO_CUERPO = "<p>Tu nueva contrase&ntilde;a es la siguiente: <b>{{clave}}</b></p>";

	public static final String ASUNTO_GENERACION_CODIGO = "Generación de código de validación.";

	public static final String GENERACION_CODIGO_CUERPO = "<p>Se gener&oacute; su c&oacute;digo de verificaci&oacute;n de dos pasos:</p><p><b>{{codigo}}</b></p>";

	public static final String PIE_CORREO = "Este correo electr&oacute;nico fue generado autom&aacute;ticamente por el Sistema de Control de Asistencia LTIC-PUCE";
	/* ***** ***** ***** */

	public static final String URL_LOGIN = "/ScaIngenieria/login.xhtml";

	public static final String[] PERFILES = { "TIP_PER_BEC", "TIP_PER_AYU", "TIP_PER_VOL" };

	public static final String CODIGO_LUNES = "DIA_SEM_LUN";

	public static final String CODIGO_MARTES = "DIA_SEM_MAR";

	public static final String CODIGO_MIERCOLES = "DIA_SEM_MIE";

	public static final String CODIGO_JUEVES = "DIA_SEM_JUE";

	public static final String CODIGO_VIERNES = "DIA_SEM_VIE";

	public static final String TAMANO_ALUMNOS_POR_HORA = "QNT_ALUMNO";

	public static final String PERIODO_ACTUAL = "PER_ACTUAL";

	public static final String PERIODO_ACADEMICO = "PER_ACA";

	public static final String USUARIO_ADMINISTRADOR = "administrador";

	public static final String EMAIL_DOCENTE_ACTUAL = "EMAIL_DOC_ACTUAL";

}
