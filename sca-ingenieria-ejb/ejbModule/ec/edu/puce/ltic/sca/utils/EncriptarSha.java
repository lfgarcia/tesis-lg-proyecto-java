/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			EncriptarSha.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import ec.edu.puce.ltic.sca.excepcion.EncodeExcepcion;

/**
 * Clase de encripción para el sistema.
 * 
 * @author Luis García Castro
 */
public class EncriptarSha {

	/**
	 * Permite realizar una encripción con tipo SHA-256.
	 * 
	 * @param texto
	 *            Texto a ser encriptado.
	 * @return Texto encriptado.
	 * @throws EncodeExcepcion
	 */
	public static String encriptar(final String texto) throws EncodeExcepcion {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(texto.getBytes());
			byte byteData[] = md.digest();
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				String hex = Integer.toHexString(0xff & byteData[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);
			}
			return hexString.toString();
		} catch (NoSuchAlgorithmException e) {
			throw new EncodeExcepcion("NA", "No se encontró ningún algoritmo.", e.getCause());
		}
	}

}
