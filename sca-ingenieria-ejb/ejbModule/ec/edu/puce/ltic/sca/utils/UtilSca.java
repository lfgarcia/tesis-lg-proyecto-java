/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			UtilSca.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.utils;

import java.util.Random;

/**
 * Implementación de varios métodos utilizados como utilitarios en el proyecto
 * SCA.
 * 
 * @author Luis García Castro
 */
public class UtilSca {

	/**
	 * Permite generar una clave aleatoria entre números y letras mayúsculas.
	 * 
	 * @param longitud
	 *            Tamaño para la clave generada.
	 * @return Clave generada aleatoriamente.
	 */
	public static String generarClaveAleatoria(int longitud) {
		String cadenaAleatoria = "";
		long milis = new java.util.GregorianCalendar().getTimeInMillis();
		Random r = new Random(milis);
		int i = 0;
		while (i < longitud) {
			char c = (char) r.nextInt(255);
			if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z')) {
				cadenaAleatoria += c;
				i++;
			}
		}
		return cadenaAleatoria;
	}

	/**
	 * Permite generar un código de validación de 2 pasos.
	 * 
	 * @param longitud
	 *            Tamaño para el código generado.
	 * @return Código generado aleatoriamente.
	 */
	public static String generarCodigoAleatorio(int longitud) {
		String cadenaAleatoria = "";
		long milis = new java.util.GregorianCalendar().getTimeInMillis();
		Random r = new Random(milis);
		int i = 0;
		while (i < longitud) {
			char c = (char) r.nextInt(255);
			if (c >= '0' && c <= '9') {
				cadenaAleatoria += c;
				i++;
			}
		}
		return cadenaAleatoria;
	}

	/**
	 * Permite codificar el texto para enviar al URL.
	 *
	 * @param texto
	 *            a codificar.
	 * @return Texto codificado para URL.
	 */
	public static String codificarTextoUrl(String texto) {
		String retorno = "";
		char[] tex = texto.toCharArray();
		for (int i = 0; i < tex.length; i++) {
			retorno = retorno + codificarT(tex[i]);
		}
		return retorno.trim();
	}

	/**
	 * Códigos para cada caracter.
	 *
	 * @param c
	 * @return
	 */
	private static String codificarT(char c) {
		String r = "";
		switch (c) {
		case ' ':
			r = "%20";
			break;
		case '#':
			r = "%23";
			break;
		case ':':
			r = "%3a";
			break;
		case '?':
			r = "%3f";
			break;
		case '/':
			r = "%2f";
			break;
		case '&':
			r = "%26";
			break;
		case '<':
			r = "%3c";
			break;
		case '>':
			r = "%3e";
			break;
		case '%':
			r = "%25";
			break;
		case '|':
			r = "%7c";
			break;
		case ';':
			r = "%3b";
			break;
		case 'á':
			r = "%e1";
			break;
		case 'é':
			r = "%e9";
			break;
		case 'í':
			r = "%ed";
			break;
		case 'ó':
			r = "%f3";
			break;
		case 'ú':
			r = "%fa";
			break;
		case 'ñ':
			r = "%f1";
			break;
		case 'Á':
			r = "%c1";
			break;
		case 'É':
			r = "%c9";
			break;
		case 'Í':
			r = "%cd";
			break;
		case 'Ó':
			r = "%d3";
			break;
		case 'Ú':
			r = "%da";
			break;
		case 'Ñ':
			r = "%d1";
			break;
		default:
			r = "" + c + "";
			break;
		}
		return r;
	}

}
