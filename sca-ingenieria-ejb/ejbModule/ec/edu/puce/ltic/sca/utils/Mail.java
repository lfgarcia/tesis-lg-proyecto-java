/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			Mail.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.utils;

import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Implementación del envío de correos electrónicos.
 * 
 * @author Luis García Castro
 */
@Stateless
public class Mail {

	@Resource(name = "java:jboss/mail/ScaIngenieria")
	private Session session;

	/**
	 * Permite realizar el envío de correos electrónicos.
	 * 
	 * @param parametros
	 *            Parametrización del correo electrónico.
	 * @throws MessagingException
	 */
	public void enviarCorreo(MailParametros parametros) throws MessagingException {
		Message message = new MimeMessage(session);
		for (String correo : parametros.getCorreosTO()) {
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(correo));
		}
		for (String correo : parametros.getCorreosCC()) {
			message.addRecipient(Message.RecipientType.CC, new InternetAddress(correo));
		}
		for (String correo : parametros.getCorreosCCO()) {
			message.addRecipient(Message.RecipientType.BCC, new InternetAddress(correo));
		}
		message.setSubject(parametros.getAsunto());
		message.setContent(parametros.getContenido(), "text/html");
		Transport.send(message);
	}

	/**
	 * Permite remplazar parámetros en el contenido del correo.
	 * 
	 * @param parametros
	 *            Lista de parámetros para remplazar en el correo electrónico.
	 * @param contenido
	 *            Contenido del correo electrónico.
	 * @return Texto final para el envío del correo electrónico.
	 */
	public String remplazarParametros(Map<String, String> parametros, String contenido) {
		for (Map.Entry<String, String> entry : parametros.entrySet()) {
			contenido = contenido.replace("{{" + entry.getKey() + "}}", entry.getValue());
		}
		return contenido;
	}

}
