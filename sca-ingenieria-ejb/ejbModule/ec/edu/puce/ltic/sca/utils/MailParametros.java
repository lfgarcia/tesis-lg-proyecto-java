/**
 * Sistema de Control de Asistencia - SCA
 * @Clase:			MailParameters.java
 * @Creado:			28-08-2016
 * @Modificado:		
 * @CopyRight:		PUCE
 */
package ec.edu.puce.ltic.sca.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Clase que permite tener los parametros para configurar el envío de correos.
 * 
 * @author Luis García Castro
 */
public class MailParametros {

	private String asunto;

	private String contenido;

	private List<String> correosTO = new ArrayList<>();

	private List<String> correosCC = new ArrayList<>();

	private List<String> correosCCO = new ArrayList<>();

	private Map<String, String> parametros = new HashMap<>();

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public List<String> getCorreosTO() {
		return correosTO;
	}

	public void setCorreosTO(List<String> correosTO) {
		this.correosTO = correosTO;
	}

	public List<String> getCorreosCC() {
		return correosCC;
	}

	public void setCorreosCC(List<String> correosCC) {
		this.correosCC = correosCC;
	}

	public List<String> getCorreosCCO() {
		return correosCCO;
	}

	public void setCorreosCCO(List<String> correosCCO) {
		this.correosCCO = correosCCO;
	}

	public Map<String, String> getParametros() {
		return parametros;
	}

	public void setParametros(Map<String, String> parametros) {
		this.parametros = parametros;
	}

}
